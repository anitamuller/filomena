<?php
App::uses('AppModel', 'Model');
/**
 * Student Model
 *
 * @property User $User
 * @property ActualState $ActualState
 * @property Correction $Correction
 */
class Student extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
    public $primaryKey = 'student';

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'pseudonim';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
            'User' => array(
                    'className' => 'User',
                    'foreignKey' => 'user_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
            )
    );

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
            'ActualState' => array(
                    'className' => 'ActualState',
                    'foreignKey' => 'student_id',
                    'dependent' => false,
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
            ),
            'Correction' => array(
                    'className' => 'Correction',
                    'foreignKey' => 'student_id',
                    'dependent' => false,
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
            )
    );

        
//Validaciones del modelo
    public $validate = array(
        'user_id' => array(
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'There is already one student created for this user',
            ),
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A user is required',
                'allowEmpty' => false
            ),
        ),
        'surnames' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A surname is required',
                'allowEmpty' => false
            ),
            'between' => array(
                'rule'    => array('between', 1, 100),
                'required' => true,
                'message' => 'Surnames must be between 1 to 100 characters'
            )
        ),
        'names' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A name is required',
                'allowEmpty' => false
            ),
            'between' => array(
                'rule'    => array('between', 1, 100),
                'required' => true,
                'message' => 'Names must be between 1 to 100 characters'
            )
        ),
        'dni' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A dni is required',
                'allowEmpty' => false
            ),
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Numbers only'
            ),
            'between' => array(
                'rule'    => array('between', 1, 20),
                'required' => true,
                'message' => 'Dni must be between 1 to 20 characters'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'DNI must be unique',
            ),
        ),
        /* No impongo restricciones sobre el pseudónimo porque siempre va a ser generado
         * automáticamente por el mismo sistema, no se le permite al usuario ingresar nada
         * relativo a esto.
         */
    );

}
