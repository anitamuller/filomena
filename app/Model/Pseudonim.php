<?php
App::uses('AppModel', 'Model');
/**
 * Pseudonim Model
 *
 */
class Pseudonim extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'pseudonim';

}
