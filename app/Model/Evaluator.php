<?php
App::uses('AppModel', 'Model');
/**
 * Evaluator Model
 *
 * @property Problem $Problem
 * @property User $User
 */
class Evaluator extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
    public $primaryKey = 'evaluator';

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'names';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
    public $hasMany = array(
            'Problem' => array(
                    'className' => 'Problem',
                    'foreignKey' => 'evaluator_id',
                    'dependent' => false,
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'exclusive' => '',
                    'finderQuery' => '',
                    'counterQuery' => ''
            )
    );

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
            'User' => array(
                    'className' => 'User',
                    'foreignKey' => 'user_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
            )
    );
    
    
//Validaciones del modelo
    public $validate = array(
        'user_id' => array(
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'There is already one evaluator created for this user',
            ),
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A user is required',
                'allowEmpty' => false
            ),
        ),
        'surnames' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A surname is required',
                'allowEmpty' => false
            ),
            'between' => array(
                'rule'    => array('between', 1, 100),
                'required' => true,
                'message' => 'Surnames must be between 1 to 100 characters'
            )
        ),
        'names' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A name is required',
                'allowEmpty' => false
            ),
            'between' => array(
                'rule'    => array('between', 1, 100),
                'required' => true,
                'message' => 'Names must be between 1 to 100 characters'
            )
        ),
        'dni' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A dni is required',
                'allowEmpty' => false
            ),
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Numbers only'
            ),
            'between' => array(
                'rule'    => array('between', 1, 20),
                'required' => true,
                'message' => 'Dni must be between 1 to 20 characters'
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'DNI must be unique',
            ),
        ),
        'email' => array(
            'Valid email'=>array(
                'rule'=>array('email'),
                'message'=>'Please enter a valid email address'
            ),
            'between' => array(
                'rule'    => array('between', 1, 100),
                'required' => true,
                'message' => 'Emails must be between 1 to 100 characters'
            )
        )
    );           
}
