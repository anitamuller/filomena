<?php
App::uses('AppModel', 'Model');

/*Empleo de módulo necesario para el proceso de 'hash'.
 *Como mecanismo de cifrado se utiliza 'sha256'.
 */
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
Security::setHash('sha256');


/**
 * User Model
 *
 * @property Evaluator $Evaluator
 * @property Student $Student
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
    public $displayField = 'username';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
    public $hasOne = array(
            'Evaluator' => array(
                    'className' => 'Evaluator',
                    'foreignKey' => 'user_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'dependent' => true
            ),
            'Student' => array(
                    'className' => 'Student',
                    'foreignKey' => 'user_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'dependent' => true 
            )
    );
        

//Validaciones del modelo
    public $validate = array(
        'username' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required',
                'allowEmpty' => false
            ),
            'unique' => array(
                'rule' => 'isUnique',
                'message' => 'This username is already in use'
            ),
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'Letters and numbers only'
            ),
            'between' => array(
                'rule'    => array('between', 1, 100),
                'required' => true,
                'message' => 'Usernames must be between 1 to 100 characters'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            ),
            'between' => array(
                'rule' => array('between', 5, 100),
                'required' => true,
                'message' => 'Passwords must be between 5 to 100 characters'
            ),
        ),
        'password_confirm' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Please confirm your password'
            ),
            'equaltofield' => array(
                'rule' => array('equaltofield', 'password'),
                'message' => 'Both passwords must match.'
            )
        ),
        'role' => array(
            'nonEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'A role is required',
                'allowEmpty' => false
            ),
            'valid' => array(
                'rule' => array('inList', array('Administrador', 'Alumno', 'Evaluador')),
                'message' => 'Please enter a valid role',
                'allowEmpty' => false
            )
        )
    );
    
    
//Métodos auxiliares empleados
    /*
     * Analiza si dos contraseñas coinciden o no
     */    
    public function equaltofield($check, $otherfield) {
        //get name of field 
        $fname = '';
        foreach ($check as $key => $value) {
            $fname = $key;
            break;
        }
        return $this->data[$this->name][$otherfield] === $this->data[$this->name][$fname];
    }
    
    
    /*
     * Antes de realizar el guardado de los datos del formulario,
     * se realiza el hash del password escrito por el usuario y se lo
     * almacena en la base de datos ya cifrado
     */
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                    $this->data[$this->alias]['password']
            );
        }
        return true;
    }
        
       

}