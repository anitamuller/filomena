<?php
App::uses('AppModel', 'Model');
/**
 * Item Model
 *
 * @property SectionCollation $SectionCollation
 * @property Problem $Problem
 * @property Question $Question
 * @property Correction $Correction
 */
class Item extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'SectionCollation' => array(
			'className' => 'SectionCollation',
			'foreignKey' => 'section_collation_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Problem' => array(
			'className' => 'Problem',
			'foreignKey' => 'problem_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Question' => array(
			'className' => 'Question',
			'foreignKey' => 'question_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Correction' => array(
			'className' => 'Correction',
			'foreignKey' => 'item_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
