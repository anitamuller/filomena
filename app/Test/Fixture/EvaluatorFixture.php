<?php
/**
 * EvaluatorFixture
 *
 */
class EvaluatorFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'evaluator' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'unique'),
		'surnames' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'names' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'dni' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'key' => 'unique', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'email' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'evaluator', 'unique' => 1),
			'user_id' => array('column' => 'user_id', 'unique' => 1),
			'dni_UNIQUE' => array('column' => 'dni', 'unique' => 1),
			'user_id_2' => array('column' => 'user_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'evaluator' => 1,
			'user_id' => 1,
			'surnames' => 'Lorem ipsum dolor sit amet',
			'names' => 'Lorem ipsum dolor sit amet',
			'dni' => 'Lorem ipsum dolor ',
			'email' => 'Lorem ipsum dolor sit amet'
		),
	);

}
