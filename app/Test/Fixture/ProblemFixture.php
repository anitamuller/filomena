<?php
/**
 * ProblemFixture
 *
 */
class ProblemFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'primary'),
		'exam_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'evaluator_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => true, 'key' => 'index'),
		'title' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'relative_order' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'relative_weight' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'problems_ibfk_1_idx' => array('column' => 'exam_id', 'unique' => 0),
			'problems_ibfk_2_idx' => array('column' => 'evaluator_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'exam_id' => 1,
			'evaluator_id' => 1,
			'title' => 'Lorem ipsum dolor sit amet',
			'relative_order' => 1,
			'relative_weight' => 1
		),
	);

}
