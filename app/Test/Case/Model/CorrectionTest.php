<?php
App::uses('Correction', 'Model');

/**
 * Correction Test Case
 *
 */
class CorrectionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.correction',
		'app.item',
		'app.student',
		'app.user',
		'app.evaluator',
		'app.problem',
		'app.exam',
		'app.collation_table',
		'app.actual_state',
		'app.question',
		'app.image',
		'app.students_answer',
		'app.tool'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Correction = ClassRegistry::init('Correction');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Correction);

		parent::tearDown();
	}

}
