<?php
App::uses('Item', 'Model');

/**
 * Item Test Case
 *
 */
class ItemTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.item',
		'app.section',
		'app.collation_table',
		'app.problem',
		'app.exam',
		'app.evaluator',
		'app.user',
		'app.student',
		'app.actual_state',
		'app.question',
		'app.image',
		'app.students_answer',
		'app.tool',
		'app.correction'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Item = ClassRegistry::init('Item');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Item);

		parent::tearDown();
	}

}
