<?php
App::uses('Pseudonim', 'Model');

/**
 * Pseudonim Test Case
 *
 */
class PseudonimTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.pseudonim'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Pseudonim = ClassRegistry::init('Pseudonim');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Pseudonim);

		parent::tearDown();
	}

}
