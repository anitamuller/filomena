<?php
App::uses('StudentsAnswer', 'Model');

/**
 * StudentsAnswer Test Case
 *
 */
class StudentsAnswerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.students_answer',
		'app.student',
		'app.user',
		'app.evaluator',
		'app.problem',
		'app.exam',
		'app.collation_table',
		'app.actual_state',
		'app.question',
		'app.image',
		'app.item',
		'app.tool',
		'app.correction'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->StudentsAnswer = ClassRegistry::init('StudentsAnswer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->StudentsAnswer);

		parent::tearDown();
	}

}
