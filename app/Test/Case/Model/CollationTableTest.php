<?php
App::uses('CollationTable', 'Model');

/**
 * CollationTable Test Case
 *
 */
class CollationTableTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.collation_table',
		'app.problem',
		'app.exam',
		'app.evaluator',
		'app.user',
		'app.student',
		'app.actual_state',
		'app.question',
		'app.image',
		'app.item',
		'app.students_answer',
		'app.tool',
		'app.correction',
		'app.section'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CollationTable = ClassRegistry::init('CollationTable');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CollationTable);

		parent::tearDown();
	}

}
