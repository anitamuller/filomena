<?php
App::uses('Tool', 'Model');

/**
 * Tool Test Case
 *
 */
class ToolTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.tool',
		'app.question',
		'app.problem',
		'app.exam',
		'app.evaluator',
		'app.user',
		'app.student',
		'app.actual_state',
		'app.correction',
		'app.collation_table',
		'app.image',
		'app.item',
		'app.students_answer'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Tool = ClassRegistry::init('Tool');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Tool);

		parent::tearDown();
	}

}
