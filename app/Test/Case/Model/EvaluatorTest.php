<?php
App::uses('Evaluator', 'Model');

/**
 * Evaluator Test Case
 *
 */
class EvaluatorTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.evaluator',
		'app.user',
		'app.student',
		'app.actual_state',
		'app.correction',
		'app.problem'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Evaluator = ClassRegistry::init('Evaluator');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Evaluator);

		parent::tearDown();
	}

}
