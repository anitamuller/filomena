<?php
App::uses('ActualState', 'Model');

/**
 * ActualState Test Case
 *
 */
class ActualStateTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.actual_state',
		'app.student',
		'app.user',
		'app.evaluator',
		'app.problem',
		'app.exam',
		'app.collation_table',
		'app.question',
		'app.correction'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ActualState = ClassRegistry::init('ActualState');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ActualState);

		parent::tearDown();
	}

}
