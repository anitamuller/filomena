<?php
App::uses('StudentsAnswersController', 'Controller');

/**
 * StudentsAnswersController Test Case
 *
 */
class StudentsAnswersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.students_answer',
		'app.student',
		'app.user',
		'app.evaluator',
		'app.problem',
		'app.exam',
		'app.collation_table',
		'app.section',
		'app.item',
		'app.question',
		'app.actual_state',
		'app.image',
		'app.tool',
		'app.correction'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
