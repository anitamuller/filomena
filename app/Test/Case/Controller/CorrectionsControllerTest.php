<?php
App::uses('CorrectionsController', 'Controller');

/**
 * CorrectionsController Test Case
 *
 */
class CorrectionsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.correction',
		'app.item',
		'app.section',
		'app.collation_table',
		'app.problem',
		'app.exam',
		'app.evaluator',
		'app.user',
		'app.student',
		'app.actual_state',
		'app.question',
		'app.image',
		'app.students_answer',
		'app.tool'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
