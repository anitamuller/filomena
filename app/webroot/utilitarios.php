<?php

/*
 * Funcion auxiliar que me permite ordenar un arreglo de acuerdo a una columna y a un criterio, en nuestro caso
 * sera la fecha para mostrarlos ordenados.
 */

function ordenar($rs, $columnas, $tipo = SORT_ASC) {
    if (empty($rs)) {
        return $rs;
    }
    $sentido_default = SORT_ASC;   //-- Lo necesito para pasar por referencia al call_user_func_array
    // Armo los arrays utilizados para ORDENAR
    $orden = array();
    for ($a = 0; $a < count($rs); $a++) {
        foreach ($columnas as $id => $col) {
            $orden[$id][$a] = $rs[$a][$col];
        }
    }
    // Armo los parametros del mutisort
    foreach ($columnas as $id => $col) {
        $parametros[] = & $orden[$id];
        if (!is_array($tipo)) {  //Valor comun 
            $parametros[] = &$tipo;
        } elseif (isset($tipo[$col])) {  //Es arreglo asociativo por columna
            $parametros[] = &$tipo[$col];
        } else {
            $parametros[] = &$sentido_default;
        }
    }
    $parametros[] = & $rs;
    // Como la funcion trabaja por referencia, tomo la posicion del array que me interesa ordenar
    $indice_resultado = count($parametros) - 1;
    call_user_func_array('array_multisort', $parametros);
    return $parametros[$indice_resultado];
}