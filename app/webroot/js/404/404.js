
		$(document).ready(function() { 
		
		$('body').mousemove(function(e) {
			mouseX = e.clientX;
			$('#layer_one').css('background-position', '-'+Math.floor(mouseX / 9) + 'px 0');
			$('#layer_two').css('background-position', Math.floor(mouseX / 6) + 'px 0');
			$('#layer_three').css('background-position', Math.floor(mouseX / 1.3) + 'px 0');
			$('#clouds_one').css('background-position', '-'+Math.floor(mouseX / 4) + 'px 0');
			$('#clouds_two').css('background-position', Math.floor(mouseX / 4) + 'px 0');
			$('#clouds_three').css('background-position', Math.floor(mouseX / 1.5) + 'px 0');
		});
		
		function whall(){
			$('#whall').animate({'top':'254px', 'background-position':'80.2% 0%'}, 600, function(){
				$('#whall').animate({'top':'260px', 'background-position':'80% 0%'}, 600, function(){
					$('#whall').animate({'top':'254px', 'background-position':'79.8% 0%'}, 600, function(){
						$('#whall').animate({'top':'260px', 'background-position':'80% 0%'}, 600, function(){
							return whall();
						});
					});
				});
			});
		}
		
		function zeplin(){
			$('#zeplin').animate({'top':'163px', 'background-position':'15.2% 0%'}, 600, function(){
				$('#zeplin').animate({'top':'160px', 'background-position':'15% 0%'}, 600, function(){
					$('#zeplin').animate({'top':'163px', 'background-position':'14.8% 0%'}, 600, function(){
						$('#zeplin').animate({'top':'160px', 'background-position':'15% 0%'}, 600, function(){
							return zeplin();
						});
					});
				});
			});
		}
		
		whall();
		zeplin();
		
		});