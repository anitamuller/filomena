<?php
/*
 * Función empleada para obtener el path necesario para algunas redirecciones y evitar hardcodear
 * y que funcione sólo si el proyecto tiene un cierto nombre y está ubicado bajo un usuario específico.
 * Este acercamiento es general y me permite obtener el path que necesito.
 */
function obtenerPath() {
    
    //Obtengo el nombre de la carpeta central de cake
    //En nuestro caso esto devuelve filomena
    $nombreBase = basename(dirname(APP));
    
    //Obtengo la url completa incluido controller y acción
    $urlCompleta = Router::url(null, true);
    
    //Esto tiene la siguiente forma
    //http://cs.uns.edu.ar/~amuller/filomena/StudentsAnswers/answer/1/4
    //http://localhost/filomena/StudentsAnswers/answer/1/2
    //Parseo la url completa
    $urlParseada = parse_url($urlCompleta);
    
    //Pido la componente 'path' que es la que me interesa
    $path = $urlParseada['path'];
    
    //Hago un explode y leo del path hasta encontrar el nombre de la carpeta
    //la url que tengo que devolver es ir acumulando todo lo que haya hasta la carpeta incluida
    //Esto es lo que debe devolver
    $componentesPath = explode("/", $path);
    
    //Ojo porque el primer componente es un espacio en blanco, NO hay que tenerlo en cuenta
    $esPrimero = true;
    
    //Variable para ir acumulando lo que necesito   
    $stringResultado = '';
    
    foreach ($componentesPath as $componente) {
        
        if ($esPrimero) {
            //Si es el primero lo omito porque es un espacio en blanco
            $esPrimero = false;
        }
        
        else {
            //Si no es el primero, proceso    
            //Me fijo si llegué a encontrar la dirección base o no
            if ((strcmp($nombreBase,$componente))===0) {
                //Lo encontré, freno
                break;
            }

            else {
                //Tengo que ir acumulando en el arreglo
                $stringResultado = $stringResultado . '/' . $componente;
            }
        }  
    }
    
    //Al final agrego el nombre base al resultado y termino
    $stringResultado = $stringResultado . '/' . $nombreBase;
    return $stringResultado;  
}