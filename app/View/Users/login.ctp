<?php

echo $this->Session->flash('auth'); ?>

<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="form-vertical login-form" action="/filomena/" id="UserLoginForm" method="post" accept-charset="utf-8" role="form">
        <h2 class="form-title"><i class="fa fa-user-md fa-2x"></i>    Filomena</h2>
        <div class="alert alert-error hide">
            <button class="close" data-dismiss="alert"></button>
            <span>Ingresá un usuario y un password</span>
        </div>
        <div class="control-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="fa fa-user"></i>
                    <input class="m-wrap placeholder-no-fix" type="text" autocomplete="off" name="data[User][username]" placeholder="Usuario" maxlength="100"  required="required"/>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="fa fa-lock"></i>
                    <input class="m-wrap placeholder-no-fix" type="password" autocomplete="off" placeholder="Contraseña" name="data[User][password]"/>
                </div>
            </div>
        </div>
        <?php echo $this->Session->flash();?>
        <div class="form-actions">
            
            <button type="submit" class="btn blue">
                Login <i class="m-icon-swapright m-icon-white"></i>
            </button>            
        </div>
    </form>
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    2014 &copy; Filomena
</div>

<?php 
    echo $this->Html->script('login/jquery-migrate-1.2.1.min');
    echo $this->Html->script('login/jquery.slimscroll.min');
    echo $this->Html->script('login/jquery.blockui.min');
    echo $this->Html->script('login/jquery.cookie.min');
    echo $this->Html->script('login/jquery.validate.min');
    echo $this->Html->script('login/jquery.backstretch.min');
    echo $this->Html->script('login/select2.min');
    echo $this->Html->script('login/app.js');
    echo $this->Html->script('login/login-soft');

?>

<script>
    jQuery(document).ready(function () {
        App.init();
        Login.init();
    });
</script>