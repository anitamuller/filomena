<div class="container main-container">


<div class="panel panel-primary user-actions">
        <div class="panel-heading"><?php echo __('Actions'); ?></div>
        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">           
                <li><?php echo $this->Html->link(__('Users list'), array('action' => 'index')); ?></li>
            </ul>
        </div>
    </div>



<?php echo $this->Form->create('User'); ?>             
                        <fieldset>
                            <legend><?php echo __('Add Evaluator'); ?></legend>
                            <div class="form-group"><?php echo $this->Form->input(__('username'), array('class' => 'form-control', 'placeholder' => 'Nombre de usuario'));?></div>
                            <div class="form-group"><?php echo $this->Form->input(__('password'), array('class' => 'form-control', 'placeholder' => 'Password'));?></div>
                            <div class="form-group"><?php echo $this->Form->input(__('password_confirm'), array('label' => 'Confirmación de password', 'maxLength' => 20, 'title' => 'Confirm password', 'type'=>'password','class' => 'form-control', 'placeholder' => 'Confirmación de password'));?></div>
                                                                                    
                            <div class="form-group"><?php echo $this->Form->input(__('Evaluator.names'), array('class' => 'form-control', 'placeholder' => 'Nombre'));?></div>
                            <div class="form-group"><?php echo $this->Form->input(__('Evaluator.surnames'), array('class' => 'form-control', 'placeholder' => 'Apellido'));?></div>
                            
                            <div class="form-group"><?php echo $this->Form->input(__('Evaluator.dni'), array('class' => 'form-control', 'placeholder' => 'DNI'));?></div>
                            <div class="form-group"><?php echo $this->Form->input(__('Evaluator.email'), array('class' => 'form-control', 'placeholder' => 'E-mail'));?></div>
                        
                            <?php echo ($this->Form->submit('Guardar',array('class' => 'btn btn-primary','style' => 'font-size: 14px; padding: 10px; border-radius: 2px;','name'=>'guardar')));?>
                            <?php echo ($this->Form->submit('Guardar y agregar nuevo evaluador',array('class' => 'btn btn-primary','style' => 'font-size: 14px; padding: 10px; border-radius: 2px;','name'=>'guardarNuevo')));?>
                        </fieldset>
   
                        <div class="form-group"><?php echo $this->Form->end(); ?></div>
           
 <?php $this->end();