<div class="container main-container">


<div class="panel panel-primary user-actions">
        <div class="panel-heading"><?php echo __('Actions'); ?></div>
        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">           
                <li><?php echo $this->Html->link(__('Users list'), array('action' => 'index')); ?></li>
            </ul>
        </div>
    </div>

<?php echo $this->Form->create('User'); ?>
<div class="form">
	<fieldset>
		<legend><?php echo __('Edit Evaluator'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('role',array('type' => 'hidden'));
                //Mando el id que tiene el evaluador asociado a este user para luego poder insertarlo bien
                echo $this->Form->input('Evaluator.evaluator',array('type' => 'hidden'));
        ?>        
                <div class="form-group"><?php echo $this->Form->input(__('username'), array('class' => 'form-control', 'placeholder' => 'Username'));?></div>
                <div class="form-group"><?php echo $this->Form->input(__('password'), array('label' => 'Nueva password','class' => 'form-control', 'placeholder' => 'Nueva password','value'=>''));?></div>
                <div class="form-group"><?php echo $this->Form->input(__('password_confirm'), array('label' => 'Confirmación de nueva password', 'maxLength' => 100, 'title' => 'Confirm password', 'type'=>'password','class' => 'form-control', 'placeholder' => 'Confirmación de nueva password'));?></div>
                            
                                                        
                <div class="form-group"><?php echo $this->Form->input(__('Evaluator.names'), array('class' => 'form-control', 'placeholder' => 'Name'));?></div>
                <div class="form-group"><?php echo $this->Form->input(__('Evaluator.surnames'), array('class' => 'form-control', 'placeholder' => 'Surname'));?></div>
                
                <div class="form-group"><?php echo $this->Form->input(__('Evaluator.dni'), array('class' => 'form-control', 'placeholder' => 'DNI'));?></div>
                <div class="form-group"><?php echo $this->Form->input(__('Evaluator.email'), array('class' => 'form-control', 'placeholder' => 'E-mail'));?></div>
                
                <?php echo $this->Form->submit(__('Submit'),array('class' => 'btn btn-primary','style' => 'font-size: 14px; padding: 10px; border-radius: 2px;')); ?>
	</fieldset>

</div>

 <?php $this->end(); ?>