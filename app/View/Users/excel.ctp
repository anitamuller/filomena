<div>
    <legend style="width: 70%;">Bienvenido <?php echo CakeSession::read("Auth.User.username"); ?>!</legend>

<?php
App::import('Vendor', 'PHPExcel', array('file' => 'excel/Classes/PHPExcel.php'));

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("agustina")
->setLastModifiedBy("agustina")
->setTitle("Reporte")
->setSubject("Reporte")
->setDescription("Reporte")
->setKeywords("office 2007 openxml php")
->setCategory("Resultados");


$objPHPExcel->getActiveSheet()->setTitle('Usuarios');
$objPHPExcel->setActiveSheetIndex(0);


$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Nombre de Usuario')
                                ->setCellValue('B1', 'Rol')
                                ->setCellValue('C1', 'Id');



$linea = 1;
foreach ($nombres as $nombre){
	++$linea;
	$objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$linea, $nombre['User']['username'], PHPExcel_Cell_DataType::TYPE_STRING);
	$objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$linea, $nombre['User']['role'], PHPExcel_Cell_DataType::TYPE_STRING);
	$objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$linea, $nombre['User']['id'], PHPExcel_Cell_DataType::TYPE_STRING);
}



ob_end_clean();
//header('Content-type: application/vnd.ms-excel');//si funciona!!!
// It will be called file.xls
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');//recomendado en la web pero no funciona
header('Content-Disposition: attachment; filename="payroll.xlsx"');
header('Cache-Control: max-age=0 ');
 
//Modificar la ruta en este metodo $objWriter->save()
 ob_end_clean();
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
?>
</div>