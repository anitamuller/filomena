<div class="container main-container">


<div class="panel panel-primary user-actions">
        <div class="panel-heading"><?php echo __('Actions'); ?></div>
        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">           
                <li><?php echo $this->Html->link(__('Users list'), array('action' => 'index')); ?></li>
            </ul>
        </div>
    </div>

<?php echo $this->Form->create('User'); ?>
<div class="form">
	<fieldset>
		<legend><?php echo __('Edit Administrator'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('role',array('type' => 'hidden'));
	?>
                <div class="form-group"><?php echo $this->Form->input(__('username'), array('class' => 'form-control', 'placeholder' => 'Username'));?></div>
                <div class="form-group"><?php echo $this->Form->input(__('password'), array('label' => 'Nueva password','class' => 'form-control', 'placeholder' => 'Nueva password', 'value'=>''));?></div>
                <div class="form-group"><?php echo $this->Form->input(__('password_confirm'), array('label' => 'Confirmación de nueva password', 'maxLength' => 100, 'title' => 'Confirmación de nueva password', 'type'=>'password','class' => 'form-control', 'placeholder' => 'Confirmación de password'));?></div>

                <?php echo $this->Form->submit(__('Submit'),array('class' => 'btn btn-primary','style' => 'font-size: 14px; padding: 10px; border-radius: 2px;')); ?>
	</fieldset>

</div>
 <?php $this->end(); ?>