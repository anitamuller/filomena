<div class="users index">
    <div class="container main-container">
        
        <div class="panel panel-primary user-actions">
        <div class="panel-heading"><?php echo __('Actions'); ?></div>
        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">           
                <li><?php echo $this->Html->link(__('New Admin'), array('action' => 'addadmin')); ?></li>
                <li><?php echo $this->Html->link(__('New Evaluator'), array('action' => 'addevaluator')); ?></li>
                <li><?php echo $this->Html->link(__('New Student'), array('action' => 'addstudent')); ?></li>
                <li><?php echo $this->Html->link(__('Go back'), array('controller'=>'ManagerAdmin','action' => 'abm')); ?></li>
		
            </ul>
        </div>
    </div>
        
        
        
        <legend style="width: 70%;"><?php echo __('Users'); ?></legend>
        
<div class="panel panel-default user-content">
        <div class="panel-body">
            <table class="table table-striped table-hover table-info">
        <tr>
            <th><?php echo $this->Paginator->sort('username'); ?></th>
            <th><?php echo $this->Paginator->sort('role'); ?></th>
            <th><?php echo $this->Paginator->sort('created'); ?></th>
            <th><?php echo $this->Paginator->sort('modified'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        
	<?php foreach ($users as $user): ?>
        <tr>
            <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
            <td><?php echo h($user['User']['role']); ?>&nbsp;</td>
            <td><?php echo h($user['User']['created']); ?>&nbsp;</td>
            <td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
            <td class="actions">
                
                <?php
                $nombreRol = $user['User']['role'];
                if (strcmp("Evaluador", $nombreRol) === 0) {
                    //El usuario es evaluador
                    echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id']));
                    echo $this->Html->link(__('Edit'), array('action' => 'editevaluator', $user['User']['id']));
                    echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['username']));     
                }
                
                else if (strcmp("Alumno", $nombreRol) === 0) {
                    //El usuario es alumno
                    echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id']));
                    echo $this->Html->link(__('Edit'), array('action' => 'editstudent', $user['User']['id']));
                    echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['username']));   
                }
                
                else if (strcmp("Administrador", $nombreRol) === 0) {
                    //El usuario es administrador
                    echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id']));
                    echo $this->Html->link(__('Edit'), array('action' => 'editadministrator', $user['User']['id']));
                    echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['username']));   
                } 
            ?>	
            </td>
        </tr>
<?php endforeach; ?>
    </table>
    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
    <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
    </div>
<?php $this->end();
