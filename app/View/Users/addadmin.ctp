    <div class="container main-container">


<div class="panel panel-primary user-actions">
        <div class="panel-heading"><?php echo __('Actions'); ?></div>
        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">           
                <li><?php echo $this->Html->link(__('Users list'), array('action' => 'index')); ?></li>
            </ul>
        </div>
    </div>



<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Add Administrator'); ?></legend>
	<?php
		echo $this->Form->input(__('username'),array('class' => 'form-control', 'placeholder' => 'Nombre de usuario'));
		echo $this->Form->input(__('password'),array('class' => 'form-control', 'placeholder' => 'Password'));
                echo $this->Form->input(__('password_confirm'), array('label' => 'Confirmación de password', 'maxLength' => 20, 'title' => 'Confirm password', 'type'=>'password','class' => 'form-control', 'placeholder' => 'Confirmación de password'));
	?>
                
       <?php echo ($this->Form->submit('Guardar',array('class' => 'btn btn-primary','style' => 'font-size: 14px; padding: 10px; border-radius: 2px;','name'=>'guardar')));?>        
       <?php echo ($this->Form->submit('Guardar y agregar nuevo administrador',array('class' => 'btn btn-primary','style' => 'font-size: 14px; padding: 10px; border-radius: 2px;','name'=>'guardarNuevo')));?>        
                
	</fieldset>
      
<?php echo $this->Form->end(); ?>
 <?php $this->end(); ?>