<div class="container main-container">


<div class="panel panel-primary user-actions">
        <div class="panel-heading"><?php echo __('Actions'); ?></div>
        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">           
                <li><?php echo $this->Html->link(__('Users list'), array('action' => 'index')); ?></li>
            </ul>
        </div>
    </div>

<?php echo $this->Form->create('User'); ?>
<div class="form">
	<fieldset>
		<legend><?php echo __('Edit Student'); ?></legend>
	<?php
		echo $this->Form->input('User.id');
		echo $this->Form->input('User.role',array('type' => 'hidden'));
                //Mando el id que tiene el student asociado a este user para luego poder insertarlo bien
                echo $this->Form->input('Student.student',array('type' => 'hidden'));
                
                //También el pseudónimo para que al editarse vuelva a enivarse el mismo
                echo $this->Form->input('Student.pseudonim',array('type' => 'hidden'));
                
        ?>        
                <div class="form-group"><?php echo $this->Form->input(__('User.username'), array('class' => 'form-control', 'placeholder' => 'Username'));?></div>
                <div class="form-group"><?php echo $this->Form->input(__('User.password'), array('label' => 'Nueva password','class' => 'form-control', 'placeholder' => 'Nueva password','value'=>''));?></div>
                <div class="form-group"><?php echo $this->Form->input(__('password_confirm'), array('label' => 'Confirmación de nueva password', 'maxLength' => 100, 'title' => 'Confirm password', 'type'=>'password','class' => 'form-control', 'placeholder' => 'Confirmación de nueva password'));?></div>
                            
                                                        
                <div class="form-group"><?php echo $this->Form->input(__('Student.names'), array('class' => 'form-control', 'placeholder' => 'Nombre'));?></div>
                <div class="form-group"><?php echo $this->Form->input(__('Student.surnames'), array('class' => 'form-control', 'placeholder' => 'Apellido'));?></div>
                <div class="form-group"><?php echo $this->Form->input('Student.dni', array('class' => 'form-control', 'placeholder' => 'Dni'));?></div>
                
                <?php echo $this->Form->submit(__('Submit'),array('class' => 'btn btn-primary','style' => 'font-size: 14px; padding: 10px; border-radius: 2px;')); ?>
	</fieldset>

</div>
 <?php $this->end(); ?>
