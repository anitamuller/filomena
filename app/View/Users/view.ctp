<div class="users view">
    
<div class="container main-container">


<div class="panel panel-primary user-actions">
        <div class="panel-heading"><?php echo __('Actions'); ?></div>
        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">           
                <li><?php echo $this->Html->link(__('Users list'), array('action' => 'index')); ?></li>
            </ul>
        </div>
    </div>    

<fieldset>
<h2><?php echo __('User'); ?></h2>
	<dl>
		
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd>
			<?php echo h($user['User']['role']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
	</dl>

	<div class="related">
        
        <?php
            $nombreRol = $user['User']['role'];
                if (strcmp("Evaluador", $nombreRol) === 0) {
                    //El usuario es evaluador
                    //Genero la salida en una variable de la cual hago echo luego
                    $salida = '<h3>'.__('Related Evaluator').'</h3> 
                        <dl><dt>'.__('Surnames').'</dt> <dd>'
                        .$user['Evaluator']['surnames'].'</dd>
                        <dt>'.__('Names').'</dt><dd>'
                        .$user['Evaluator']['names'].'</dd>
                        <dt>'.__('Dni').'</dt><dd>'
                        .$user['Evaluator']['dni'].'</dd>
                        <dt>'.__('E-mail').'</dt><dd>'
                        .$user['Evaluator']['email'].'</dd></dl>';
                    echo $salida;
                }
                
                else if (strcmp("Alumno", $nombreRol) === 0) {
                    //El usuario es alumno
                    //Genero la salida en una variable de la cual hago echo luego
                    $salida = '<h3>'.__('Related Student').'</h3> 
                        <dl><dt>'.__('Surnames').'</dt> <dd>'
                        .$user['Student']['surnames'].'</dd>
                        <dt>'.__('Names').'</dt><dd>'
                        .$user['Student']['names'].'</dd>
                        <dt>'.__('Dni').'</dt><dd>'
                        .$user['Student']['dni'].'</dd>
                        <dt>'.__('Pseudonim').'</dt><dd>'
                        .$user['Student']['pseudonim'].'</dd></dl>';
                    echo $salida;
                }
            
        ?>    
</fieldset>
    </div>
</div>

	