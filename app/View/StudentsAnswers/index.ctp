
<div>
    <!--<legend style="width: 70%; margin-top:10px;">Bienvenido <?php echo CakeSession::read("Auth.User.username"); ?>!</legend>-->

   
    <div style="text-align: center; padding-top: 100px;">
    <?php
   
    if ($published){
        //Si el examen está publicado por el administrador
        if (!$init && !$finish){
                //El alumno ni empezó ni terminó, le pongo el comenzar examen
                echo $this->Html->link(__('Comenzar Examen'), array('controller'=>'StudentsAnswers','action' => 'answer', 1, 1),
                array('class' => 'btn btn-danger btn-lg', 'style' => 'background-color: rgb(44, 195, 44); border-color:rgb(44, 195, 44); font-size: 14px;')); 
        }

        else if ($init && !$finish){
                //El alumno empezó el examen pero no lo terminó todavía, le pongo el retomar
                echo $this->Html->link(__('Retomar Examen'), array('controller'=>'StudentsAnswers','action' => 'actualstate'),
                array('class' => 'btn btn-danger btn-lg', 'style' => 'font-size: 14px;')); 
        }
        
        else {
            //No hay otra combinación posible más que haya empezado y terminado el examen
            //Notar que nunca podrá terminar sin antes empezar. Solo si es un alumno que no fue a rendir, por lo que
            //lo redijo siempre a esta página sin que pueda hacer más nada.
            ?>
            <div>
                <h3 style="text-align: center">El examen ha finalizado.</h3>
            </div>
            <?php
            }
    }
    else{
        ?>
        <div>
            <h3 style="text-align: center">El examen no ha sido publicado.</h3>
        </div>
    <?php } ?>
    </div>

</div>

