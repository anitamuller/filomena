<script>

function confirmfrmSubmit(){

var agree=confirm("¿Está seguro que desea pasar a la siguiente pregunta?. Una vez avanzada una pregunta no hay vuelta atrás. Este cartel no volverá a mostrarse.");

if (agree)
    return true ;
else
    return false ;
}


</script>


<div class="students form" style="margin-top:-2%;">
    
    
    <?php

        //Incluyo utilitarios que tiene el método para obtener la url
        include '../webroot/pathGetter.php';

        //Me fijo si se trata de la primera pregunta del primer problema o no para poner un pop up de confirmación
        if (($problema==1) && ($pregunta==1)) {
            //Si es así le pongo confirmación
            echo $this->Form->create('StudentsAnswer',array('onsubmit'=>'return confirmfrmSubmit();'));
        }
        
        else {
            //Si no no le pongo confirmación
            echo $this->Form->create('StudentsAnswer');
        }
    ?>

    <div class="row">
        <div class="col-md-6">
           <?php    //Titulo del caso y de la pregunta asociada al caso
                    $nombreProblema = 'problema' . $problema;
                    $cantPreguntas = $this->Session->read($nombreProblema);
                    $cantProblemas = $this->Session->read('cantProblemas');
                    echo '<h2>Caso ' . $problema. ' de ' .$cantProblemas .'</h2>'; 
                    echo '<h4>Pregunta ' . $pregunta. ' de '. $cantPreguntas.'</h4>';
                ?> 
        </div>
        <div class="col-md-6">
            <?php
                //Si es una pregunta contestable pongo el enunciado
                if ($pregunta < $cantPreguntas) {
                    echo '<strong><div class="descPregunta">' .$preguntaEvaluada[0]['questions']. ' </div></strong>';
                }else{
                    //Si no es una pregunta contestable pongo el botón para el siguiente problema
                    echo '<div class="col-md-1 col-md-offset-5">';
                            
                    
                    //Me fijo si se trata de la última pregunta del último problema o no
                    if (($pregunta==$cantPreguntas) && ($problema==$cantProblemas)) {
                        //Es la última
                        echo $this->Form->submit(__('Finalizar examen'),
                            array('class' => 'btn btn-danger btn-lg',
                            'style' => 'font-size: 13px; padding: 10px; border-radius: 4px; margin-top:40px;'));
                    }
                    
                    else {
                        //No es la última pregunta del último problema, paso al siguiente
                        echo $this->Form->submit(__('Siguiente caso'),
                                array('class' => 'btn btn-primary btnSiguiente',
                                'style' => 'font-size: 13px; padding: 10px; border-radius: 4px; margin-top:40px;'));
                        }
                    echo'</div>';
                }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
                <?php
                echo '<div class="papel">';
                //Recupero las descripciones de las preguntas anteriores
                $cantPreg = $pregunta - 1; //Acá no puedo usar directamente $pregunta, pq mi indice dentro del arreglo empieza en cero
                if ($pregunta != 1) {//Si estoy en la primer pregunta $preguntasPrevias estara vacio
                    for ($i = 0; $i < $cantPreg; $i++) {
                       echo '<div class="descCaso">' .$preguntasPrevias[$i]['description']. ' </div>';
                    }
                }

                //Recupero la descripcion y enunciado de la pregunta actual
                //echo $this->Text->autoParagraph($preguntaEvaluada[0]['description']);
                echo '<div class="descCaso casoActual">' .$preguntaEvaluada[0]['description']. ' </div>';
                echo '</div>'; 
                ?>

            
            
                <?php
                $cantImg = count($imagenes);

                $urlBase = obtenerPath();

                for ($i=0; $i<$cantImg ; $i++){
                    $url_img = $urlBase . '/img/'. $imagenes[$i];
                
                    echo '<a class="fancybox-thumb" rel="fancybox-thumb"  href="' .$url_img. '">
                    <img src="' .$url_img. '" alt="" class="imagen">
                    </a>';
                }
                ?>
                
      

            </script>


        </div>
        <div class="col-md-6">
                <?php
                //Si es una pregunta contestable pongo la respuesta del alumno
                if ($pregunta < $cantPreguntas) {
                    echo $this->Form->textarea('rtaAlumno', array('class' => 'txtArea','rows' => '24', 'cols' => '65','escape' => false));
                }    
                 ?>   
        </div>
    </div>

    <div class="row">
        <div class="col-md-1 col-md-offset-11">
                <?php
                //Si es una pregunta contestable pongo el boton a la siguiente pregunta
                if ($pregunta < $cantPreguntas) {
                        echo $this->Form->submit(__('Siguiente'), 
                                array('class' => 'btn btn-primary btnSiguiente',
                                'style' => 'font-size: 15px; padding: 11px; border-radius: 4px;'));
                }
                ?>

        </div>
    </div>
    
    <?php echo $this->Html->script('jquery.mousewheel-3.0.6.pack');
          echo $this->Html->script('jquery.fancybox.pack');
          echo $this->Html->script('jquery.fancybox-buttons');
          echo $this->Html->script('jquery.fancybox-media');
          echo $this->Html->script('jquery.fancybox-thumbs');?>

    <script>
$(document).ready(function() {
	$(".fancybox-thumb").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});
});
    </script>