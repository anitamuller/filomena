<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

	<?php echo $this->Html->charset(); ?>
        <title>
	    <?php echo 'Filomena'; 
                  //Obtengo la url base que la empleo para redirecciones absolutas
                  $urlBase = getPath();
            
                  $direccionFavi = $urlBase . '/img/favicon.ico';  
            
            ?>
        </title>
        
        <?php echo '<link rel="shortcut icon" href='.$direccionFavi.'>'; ?>
        
        <style type="text/css">#backtotop,#blogsidebar li.tweets span,#contact h2,#contact ul li span,#content #portfolionav li a,#content .fservice h4,#dsq-reply h3,#facebookbutton,#footer #contactusbutton,#footer #dribbblebutton,#footer #followusbutton,#footer .services li a,#footer h2,#info h1,#info p,#searchresults,#team .thegrid li a span p,#testimonialsgrid div h1,.abouttext h1,.abouttext h2,.abouttext h3,.archive .year div,.bar.fixed ul li,.clients li h2,.dsq-commenter-name,.member h2,.post .content h1,.post .content h2,.post .content h3,.post .content h4,.post .content h5,.post .date .day,.post .date p,.post .date p span.year,.post .header h2,.projectlist h1,.projectlist h2,.service .largebuttons a .large,.service .largebuttons a .small,.service h1,.service h2,.service h3,.service h4{font-family:"ff-unit-web-1","ff-unit-web-2",verdana;}</style><link rel="stylesheet" href="http://use.typekit.com/c/5c5afc/1w;ff-unit-web-1,2,TzW:P:n4,TzT:P:n5,TzP:P:n7,TzM:P:n8/d?3bb2a6e53c9684ffdc9a99f61b5b2a627dc13eb1b909f2c88003c852c364c3dc85e0c4f893bad152c823394b7ff58b8c8a36dd67085ca7b75f69a54906f46e00c6f5ac0bcd18a184afd5f537d513ad5b38e5af70be6661d19f43d9a9c74eb52f3d58896e96e4458a3889003d989812ae0cd4d7d7bdfee2df6c7c3e792f3ecbd837a0d59075e7fe244e55f62d92f6305e364f3eec455d89bd6077ba6457d8581ce10fe26dd04b8d7236fe87a240627582301850b6c9becd6edf78cf9d30e969357df4aaf8cbc318dd100588a6b42b13f23a13882e0813eaff857dc58e222ba2ef991bc075e45ad6ec364c0c973fcb2fab5547e82cc2ef60ec41e0de5014386c3da6d6977736c8f49a530b3dec26c64eb43c7385d08c2ba87dd86ef349068a5e3a2286fca71b3ec897c58f39f3c0"><script type="text/javascript">try{Typekit.load();}catch(e){}</script>

	<?php
               
                
            echo $this->Html->css('font-awesome');

            echo $this->Html->script('jquery-1.11.1');
            echo $this->Html->script('jquery-ui.min');
            echo $this->Html->script('bootstrap');
            echo $this->Html->css('404/404.css');
            
            echo $this->fetch('meta');
	    echo $this->fetch('css');
            echo $this->fetch('script');
           
            
        ?>

    </head>
    <body>
        <?php
            echo $this->fetch('content');
        ?>

    </body>

</html>



<?php
/*
 * Función empleada para obtener el path necesario para algunas redirecciones y evitar hardcodear
 * y que funcione sólo si el proyecto tiene un cierto nombre y está ubicado bajo un usuario específico.
 * Este acercamiento es general y me permite obtener el path que necesito.
 */
function getPath() {
    
    //Obtengo el nombre de la carpeta central de cake
    //En nuestro caso esto devuelve filomena
    $nombreBase = basename(dirname(APP));
    
    //Obtengo la url completa incluido controller y acción
    $urlCompleta = Router::url(null, true);
    
    //Esto tiene la siguiente forma
    //http://cs.uns.edu.ar/~amuller/filomena/StudentsAnswers/answer/1/4
    //http://localhost/filomena/StudentsAnswers/answer/1/2
    //Parseo la url completa
    $urlParseada = parse_url($urlCompleta);
    
    //Pido la componente 'path' que es la que me interesa
    $path = $urlParseada['path'];
    
    //Hago un explode y leo del path hasta encontrar el nombre de la carpeta
    //la url que tengo que devolver es ir acumulando todo lo que haya hasta la carpeta incluida
    //Esto es lo que debe devolver
    $componentesPath = explode("/", $path);
    
    //Ojo porque el primer componente es un espacio en blanco, NO hay que tenerlo en cuenta
    $esPrimero = true;
    
    //Variable para ir acumulando lo que necesito   
    $stringResultado = '';
    
    foreach ($componentesPath as $componente) {
        
        if ($esPrimero) {
            //Si es el primero lo omito porque es un espacio en blanco
            $esPrimero = false;
        }
        
        else {
            //Si no es el primero, proceso    
            //Me fijo si llegué a encontrar la dirección base o no
            if ((strcmp($nombreBase,$componente))===0) {
                //Lo encontré, freno
                break;
            }

            else {
                //Tengo que ir acumulando en el arreglo
                $stringResultado = $stringResultado . '/' . $componente;
            }
        }  
    }
    
    //Al final agrego el nombre base al resultado y termino
    $stringResultado = $stringResultado . '/' . $nombreBase;
    return $stringResultado;  
}
?>
