<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

	<?php echo $this->Html->charset(); ?>
        <title>
	    <?php 
            echo 'Filomena'; 
            
            //Obtengo la url base que la empleo para redirecciones absolutas
            $urlBase = getPath();
            
            $direccionFavi = $urlBase . '/img/favicon.ico';
                        
            ?>
        </title>
        
        <?php
            echo '<link rel="shortcut icon" href='.$direccionFavi.'>';
	                        
            echo $this->Html->css('font-awesome');

            echo $this->Html->script('jquery-1.11.1');
            echo $this->Html->script('jquery-ui.min');
            echo $this->Html->script('jQueryRotate.2.1');
            echo $this->Html->script('bootstrap');
            
            echo $this->fetch('meta');
	    echo $this->fetch('css');
            echo $this->fetch('script');
           
            
        ?>
    
    <?php if (AuthComponent::user()) {//el usuario esta logueado 
                echo $this->Html->css('bootstrap');
                echo $this->Html->css('jquery.fancybox');
                echo $this->Html->css('simplePagination');
                echo $this->Html->css('calculator');
                echo $this->Html->css('gestograma');
                echo $this->Html->css('jquery.fancybox-button');
                echo $this->Html->css('jquery.fancybox-thumbs');
                echo $this->Html->css('filomena');

 
    echo '
    </head>       
    <body>
        <div>
            <div id="header">                
                <div class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <div class="navbar-brand"><i class="fa fa-user-md"></i>&nbsp;Filomena</div>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                            <li> <div style="margin-top: 14px; margin-right: 10px;">';
                              
                                if (AuthComponent::user()) {
                                    echo '<i class="fa fa-user"></i>&nbsp'. CakeSession::read("Auth.User.username");
                                    $nombreRol= $this->Session->read('Auth.User.role');
                                    if (strcmp("Alumno", $nombreRol) === 0) {
                                        echo '<i class="fa fa-stethoscope" style="margin-left: 20px;"></i>&nbsp'.'Pseudónimo: '. CakeSession::read("Auth.User.Student.pseudonim");
                                    }else if (strcmp("Evaluador", $nombreRol) === 0) {
                                        echo '<i class="fa fa-stethoscope" style="margin-left: 20px;"></i>&nbsp'.'Nombre: '.CakeSession::read("Auth.User.Evaluator.names") .' '. CakeSession::read("Auth.User.Evaluator.surnames");
                                    }
                                    
                                }
echo'
                               </div>
                            </li>
                            
                            <li>';     

                                 $esAdmin = false;
                                 $esStudent = false;
                                 $esEvaluator = false; 
                                if (AuthComponent::user()) {
                                   
                                   
                                   $nombreRol= $this->Session->read('Auth.User.role');
                                   
                                    if (strcmp("Administrador", $nombreRol) === 0) {
                                    //Es administrador
                                    $esAdmin = true;
                                    } else if (strcmp("Evaluador", $nombreRol) === 0) {
                                    //Es evaluador
                                    $esEvaluator = true;
                                    } else if (strcmp("Alumno", $nombreRol) === 0) {
                                    //Es alumno
                                    $esStudent = true;
                                    }
            
                                                                        
                                    if ($esAdmin && (!$esEvaluator) && (!$esStudent)) {
                                        $direccion = $urlBase . '/users/administrador';
                                        echo'<li><a href='.$direccion.'><i class="fa fa-home"> </i>&nbsp;Inicio</a></li>';
                                       
                
                                    } else if ((!$esAdmin) && $esEvaluator && (!$esStudent)) {
                                        $direccion = $urlBase . '/managerEvaluator/assignedproblems';
                                        echo '<li><a href='.$direccion.'><i class="fa fa-home"> </i>&nbsp;Inicio</a></li>';
                                       
                                    
                                    } else if ((!$esAdmin) && (!$esEvaluator) && $esStudent) {
                                        $direccion = $urlBase . '/StudentsAnswers/index';
                                        echo'<li><a href='.$direccion.'><i class="fa fa-home"> </i>&nbsp;Inicio</a></li>';

                                        
                                    } else {
                                       $direccion = $urlBase . '/users/login';
                                       echo'<li><a href='.$direccion.'><i class="fa fa-home"> </i>&nbsp;Inicio</a></li>';

                                    }
            
                                }
echo'                
                            </li>
                            <li>';
                            if ((!$esAdmin) && (!$esEvaluator) && $esStudent){
                            echo '<div >
                            <button  data-toggle="dropdown" style="padding: 13.5px; background-color: #008cba; border: none; border: 0;">
                                <i class="fa fa-wrench"></i>&nbspHerramientas<span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>'.$this->Html->link(__('Calculadora'), array('controller' => 'students', 'action' => 'calculator'),array('target' => '_blank')).'</li>
                                <li>'.$this->Html->link(__('Calendario'), array('controller' => 'students', 'action' => 'datepicker'),array('target' => '_blank')).'</li>
                                <li>'.$this->Html->link(__('Gestograma'), array('controller' => 'students', 'action' => 'gestograma'),array('target' => '_blank')).'</li>
                                <li>'.$this->Html->link('Tablas', '/files/Tablas.pdf', array('target' => '_blank')).'</li> 
                            </ul>     
                            </div>';
                                
                            }
                            echo'</li>
                             
                            <li>';
                            
                                if (AuthComponent::user()) {
                                    //el usuario esta logueado
                                    $user = AuthComponent::user('username');
                                    $direccion = $urlBase . '/users/logout';                    
                                    echo'<a href='.$direccion.'><i class="fa fa-sign-out"> </i>&nbsp;Salir</a>';
                                    
                                } 
echo '
                            </li>
                        </ul>
                    </div>          
                </div>

                <div id="content" class="container main-container">';
                    echo $this->Session->flash(); 
                    echo $this->fetch('content'); 
                echo'</div>
                <div id="footer">
                </div>
            </div>';
            echo $this->element('sql_dump'); 
    echo'</body>';
    
    
    ?>
    
    
    
    <?php } else { //No se ha logueado nadie
                echo $this->Html->css('bootstrap.login');
                echo $this->Html->css('bootstrap-responsive');
                echo $this->Html->css('login/style-metro.css');
                echo $this->Html->css('login/style.css');
                echo $this->Html->css('login/style-responsive.css');
                echo $this->Html->css('login/themes/default.css');
                echo $this->Html->css('login/select2_metro.css');
                echo $this->Html->css('login/pages/login-soft.css');
                
                echo '</head><body class=login>';
                echo $this->Session->flash();
                echo $this->fetch('content');
                echo $this->element('sql_dump'); 
                echo'</body>';
                
    } ?>
    
    
    
    
    
    
 
</html>

<?php
/*
 * Función empleada para obtener el path necesario para algunas redirecciones y evitar hardcodear
 * y que funcione sólo si el proyecto tiene un cierto nombre y está ubicado bajo un usuario específico.
 * Este acercamiento es general y me permite obtener el path que necesito.
 */
function getPath() {
    
    //Obtengo el nombre de la carpeta central de cake
    //En nuestro caso esto devuelve filomena
    $nombreBase = basename(dirname(APP));
    
    //Obtengo la url completa incluido controller y acción
    $urlCompleta = Router::url(null, true);
    
    //Esto tiene la siguiente forma
    //http://cs.uns.edu.ar/~amuller/filomena/StudentsAnswers/answer/1/4
    //http://localhost/filomena/StudentsAnswers/answer/1/2
    //Parseo la url completa
    $urlParseada = parse_url($urlCompleta);
    
    //Pido la componente 'path' que es la que me interesa
    $path = $urlParseada['path'];
    
    //Hago un explode y leo del path hasta encontrar el nombre de la carpeta
    //la url que tengo que devolver es ir acumulando todo lo que haya hasta la carpeta incluida
    //Esto es lo que debe devolver
    $componentesPath = explode("/", $path);
    
    //Ojo porque el primer componente es un espacio en blanco, NO hay que tenerlo en cuenta
    $esPrimero = true;
    
    //Variable para ir acumulando lo que necesito   
    $stringResultado = '';
    
    foreach ($componentesPath as $componente) {
        
        if ($esPrimero) {
            //Si es el primero lo omito porque es un espacio en blanco
            $esPrimero = false;
        }
        
        else {
            //Si no es el primero, proceso    
            //Me fijo si llegué a encontrar la dirección base o no
            if ((strcmp($nombreBase,$componente))===0) {
                //Lo encontré, freno
                break;
            }

            else {
                //Tengo que ir acumulando en el arreglo
                $stringResultado = $stringResultado . '/' . $componente;
            }
        }  
    }
    
    //Al final agrego el nombre base al resultado y termino
    $stringResultado = $stringResultado . '/' . $nombreBase;
    return $stringResultado;  
}
?>