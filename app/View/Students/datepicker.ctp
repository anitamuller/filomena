<head>
    <?php  echo $this->Html->css('datepicker');
           //echo $this->Html->css('normalize'); 
           echo $this->Html->script('jquery-1.7.1.min');
           echo $this->Html->script('jquery-ui-1.8.18.custom.min');
    ?>
    <script type="text/javascript">
		$(function(){
			$('#datepicker').datepicker({
				inline: true,
				//nextText: '&rarr;',
				//prevText: '&larr;',
				showOtherMonths: true,
				//dateFormat: 'dd MM yy',
				dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
                                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                                //showOn: "button",
				//buttonImage: "img/calendar-blue.png",
				//buttonImageOnly: true,
			});
		});
                </script>
</head>

<div id="datepicker" style="margin-top: 70px;"></div>