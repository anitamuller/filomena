

<div class="row">

    <h3 class="tituloEvaluador"><?php echo __($tituloProblemaAsociado.'  /  '.$tituloPreguntaAsociada); ?></h3>

</div>

<?php 

//Me fijo si hay alumnos para corregir
if ($hayAlumnos==false) {
    echo('<h4>Hasta el momento, no hay alumnos para corregir.</h4>');
}

else {
include '../webroot/pagination.class.php';

// If we have an array with items
        if (count($alumnos)) {
          // Create the pagination object
          $pagination = new pagination($alumnos, (isset($_GET['page']) ? $_GET['page'] : 1), 12);

          // Decide if the first and last links should show
          $pagination->setShowFirstAndLast(true);
          // You can overwrite the default seperator
          //$pagination->setMainSeperator(' | ');
          // Parse through the pagination class
          $studentPages = $pagination->getResults();
          
          // If we have items 
          if (count($studentPages) != 0) {
            // Create the page numbers
            $pageNumbers= '<div class="light-theme simple-pagination" style="margin:0 auto 0 auto; height:auto; width:auto;">'.$pagination->getLinks($_GET).'</div> ';
            
            echo 
            '<div class="row">
                <div class="col-md-10  col-md-offset-1">
                   <table class="table table-hover" style="font-size: 15px">
                       <tr class="active">
                           <th>Alumnos</th>
                           <th style="text-align: center;">Estado del examen</th>
                           <th style="text-align: center;">Corregido</th>
                       </tr>'; 
            
            // Loop through all the items in the array
            foreach ($studentPages  as $studentArray) {
                
                echo '<tr>';

                  //Obtiene los datos necesarios para enviarle a la otra vista
                  $pseudonimo = $studentArray['pseudonimo'];
                  $idAlumno = $studentArray['id'];
                  $preguntaCorregida = $studentArray['corregida'];
                  $estado = $studentArray['estado']; 
                  
          
                if($estado==='Finalizado'){//Si finalizó el examen muestro el link para que lo corrija
                   echo '<td>'.$this->Html->link(__($pseudonimo), array('action' => 'assignedcorrection',$idAlumno,$problemID,$questionID)).'</td>';
                }else{ //Si está rindiendo solamente muestro el pseudónimo del alumno, no puede corregirlo todavía
                   echo '<td><div class="nombreColor">'.$pseudonimo.'</div></td>';
                }
                
               echo '<td style="text-align: center;">'.$estado .'</td>';
                
                  if ($preguntaCorregida===true) {
                      $salida = 'Sí';
                      $clase= 'fa fa-check  corregido';
                  }
                  
                  else {
                      $salida = 'No';
                      $clase='fa fa-times noCorregido';
                  }
                  
                            

                echo '<td style="text-align: center;">'. $this->Html->div($clase.' fa-2x').'</td>';
                echo '</tr>';
                
            }
            
            echo '</table></div></div>';
            // print out the page numbers beneath the results
            echo '<div class="row"><div class="col-md-10  col-md-offset-1">';
                   
            echo $pageNumbers;

            echo '</div></div>';
          }
          else {

               echo '<tr>';
               echo '<th>No hay alumnos para corregir</th>';
               echo '<tr>'; 
            }
           
          
        }
}

?>


<div class="row">
    <div class="col-md-2 col-md-offset-5">
    <?php
	echo $this->Html->link(__('Volver'), array('controller'=>'ManagerEvaluator','action' => 'assignedquestions',$problemID),
            array('class' => 'btn btn-primary btn-lg', 'style' => 'font-size: 13px; padding: 10px; border-radius: 4px;')); 
    ?>
    </div>
</div>



