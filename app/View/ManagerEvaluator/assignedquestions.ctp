<div class="row">

    <h3 class="tituloEvaluador"><?php echo __($tituloProblemaAsociado); ?></h3>

</div>



<div class="row">
    <div class="col-md-10  col-md-offset-1">
        <table class="table table-hover" style="font-size: 15px">
            <tr class="active">
                <th>Preguntas asociadas</th>
                <th style="text-align: center;">Corregida para todos los alumnos</th>
            </tr> 

        <?php 
            $contador=1;
            
            if(!(empty($preguntasOrdenadas[0]))) {
            
            foreach ($preguntasOrdenadas as $pregunta):
            ?>

            <tr>

                <?php
                  $nombrePregunta = 'Pregunta N°' . $contador;     
                  $contador++;
                  
                  if($pregunta['corregida']===true) {
                      $salida = 'Sí';
                      $clase= 'fa fa-check corregido';
                  }
                  else {
                      $salida = 'No';
                      $clase='fa fa-times noCorregido';
                  }
                  
                ?>

                <td><?php echo $this->Html->link(__($nombrePregunta), array('action' => 'assignedstudents', $pregunta['id'])); ?>&nbsp;</td>
                <td style="text-align: center;"><?php echo $this->Html->div($clase.' fa-2x');?></td>
            </tr>
            <?php endforeach; }
            else {?>

            <tr>
                <th>No hay preguntas para corregir</th>
            </tr> 

            <?php
            
            }
            ?>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-md-2 col-md-offset-5">
    <?php
	echo $this->Html->link(__('Volver'), array('controller'=>'ManagerEvaluator','action' => 'assignedproblems'),
            array('class' => 'btn btn-sm btn-primary btn-lg', 'style' => 'font-size: 13px; padding: 10px; border-radius: 4px;')); 
    ?>
    </div>
</div>

