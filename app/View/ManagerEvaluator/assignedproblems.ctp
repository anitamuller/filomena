<div class="row">

        <h3 class="tituloAdministrador"><?php echo __('Problemas Asignados'); ?></h3>

</div>

<div class="row">
    <div class="col-md-10  col-md-offset-1">

        <table class="table table-hover" style="font-size: 15px">
            <tr class="active">
                <th>Casos</th>
                <th style="text-align: center;">Corregido</th>
            </tr> 

        <?php 
                       
            if(!(empty($problemasOrdenados[0]))) {
            
            foreach ($problemasOrdenados as $problem):
            ?>

            <tr>

                <?php
                                   
                  if($problem['corregido']===true) {
                      $salida = 'Sí';
                      $clase= 'fa fa-check corregido';
                  }
                  else {
                      $salida = 'No';
                      $clase='fa fa-times noCorregido';
                  }
                  
                ?>



                <td><?php echo $this->Html->link(__($problem['nombreProblema']), array('action' => 'assignedquestions', $problem['id'])); ?>&nbsp;</td>
                <td style="text-align: center;"><?php echo $this->Html->div($clase.' fa-2x ');?></td>
            </tr>
            <?php endforeach;
                  }
            else {?>

            <tr>
                <th>No hay problemas para corregir</th>
            </tr> 

            <?php
            
            }
            
            
            
            ?>
        </table>
    </div>   
</div>


<?php if(!(empty($problemasOrdenados[0]))){
            //Debe analizar si están todos corregidos o no para habilitar el botón
            if (($todosCorregidos) && (!$finishedEvaluator)) {
                //Habilita el botón para que complete la corrección
                ?>
                <div class="row">
                <div class="col-md-2 col-md-offset-5">   
                <?php
                echo $this->Html->link(__('Finalizar corrección'), array('controller'=>'ManagerEvaluator','action' => 'finishedcorrection'),
                array('class' => 'btn btn-danger btn-lg', 'style' => 'font-size: 13px; padding: 10px; border-radius: 4px;'), "¿Está seguro que desea finalizar la corrección? Una vez finalizada no se podrán hacer cambios."); 
                ?> 
                </div>
                </div>
            <?php }
            
            else if ((!$todosCorregidos) || ($finishedEvaluator)) {
                //Faltan corregir algunos o ya corregí todo, por lo que el botón estará deshabilitado
                ?>
                <div class="row">
                <div class="col-md-2 col-md-offset-5">
                <?php
                echo $this->Html->link(__('Finalizar corrección'),array(),array('disabled'=>'true', 'class' => 'btn btn-sm btn-danger btn-lg', 'style' => 'font-size: 13px; padding: 10px; border-radius: 4px;'));
                ?>
                </div>
                </div>
            <?php
            }
            }