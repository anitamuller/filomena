<script>
    function controlarVecino(id) {

        var idVecino = obtenerIDVecino(id);

        var palabraBase = 'Question';
        var idPropio = palabraBase.concat(id);

        if (document.getElementById(idPropio).checked) { // Si estoy chequeado, mi vecino no 
            document.getElementById(idVecino).checked = false;
        } else { //No estoy chequeado, mi vecino si
            document.getElementById(idVecino).checked = true;
        }
    }

    function obtenerIDVecino(id) {

        var palabraBase = 'Question';

        if (id % 2 == 0) { //Si es par, entonces el vecino es id+1
            var aux = id + 1;
            return palabraBase.concat(aux);
        } else { //Si es impar, entonces el vecino es id-1
            var aux = id - 1;
            return palabraBase.concat(aux);

        }
    }
</script>


<?php
    //Verifica si el alumno respondió algo o no
    if ($respuestaVacia==true) {
       $salida = "Sin respuesta.";
    } else {
       $salida = $respuesta;
    };
?>

<div class="row">
    <div class="col-md-2 col-md-offset-11 sinMargen">

    </div>
</div>

<?php
if (!$finishedCorrection) {
     //Si aún no finalizó la corrección, entonces se debe crear el formulario y cuando se haga submit llamar a pubcorrections
     echo $this->Form->create(null,array(
    'url'=>array('controller'=>'managerEvaluator','action'=>'pubcorrections')));
}    

else if ($finishedCorrection) {
    //Terminó de corregir, redirijo a otra función que no chequea nada ni graba nada, solo redirije a donde corresponde en función
    //de lo recibido
    echo $this->Form->create(null,array(
    'url'=>array('controller'=>'managerEvaluator','action'=>'redirectcorrection')));
}
?>





<div class="container-fluid">
<div class="row">
    <div class=".col-xs-9 col-md-6">
        <div class="tituloCorreccion">Respuesta de <?php echo '<div class="nombreColor">'.$pseudonimo.'</div>'; ?></div>
        <textarea rows="20" cols="55" disabled><?php echo ''.$salida; ?></textarea>
    </div>
    <div class=".col-xs-9 col-md-6">
        <?php
        $tituloCaso = $titulos['caso'];
        $tituloPregunta = $titulos['pregunta']
        ?>
        <div class="tituloCorreccion"><?php echo $tituloCaso.'  /  '.'<div class="nombreColor">'.$tituloPregunta.'</div>'; ?></div>

            <table class="table table-hover" style="font-size: 13.5px">
                <tr class ="active">
                    <th>Sección</th>
                    <th class="centered">Item</th>
                    <th style="text-align: center;">Sí</th>
                    <th style="text-align: center;">No</th>

                </tr>


        <?php
            //Variable empleada para contar la cantidad de items
            $indice=0;
            $auxiliarID=0;
            
            foreach($seccionesItems as $seccion) {
               //Recupera el titulo de la sección y los items de esa sección y la pregunta actual 
               $tituloSeccion = $seccion['tituloSeccion'];
               $itemsSeccion = $seccion['items'];
               
               $escribiTituloSeccion = false;
               
                foreach ($itemsSeccion as $item) {

                    //Recupera la información de cada item necesaria para mostrar
                    $descripcionItem = $item['descripcion'];
                    $itemID = $item['id'];
                    $seleccionar = $item['seleccionar'];

                    echo '<tr>';

                    //Hay dos casos
                    if (!$escribiTituloSeccion) {
                        //Tengo que generar una salida
                        $escribiTituloSeccion = true;
                        echo '<td>'.$tituloSeccion.'</td>';
                    } 
                    else {
                        //No es el primer elemento de la sección
                        echo '<td></td>';
                     }

                     //Imprime el resto
                     echo '<td>'.$descripcionItem.'</td>';

                     //Genera un checkbox para Sï y otro para No para optimizar
                     //El $contador se lo envía también para saber la cantidad de checkboxs existentes de cada tipo
                     //Como id en el checkbox se va a guardar el id del item. El id del student se guarda una sola vez como hidden
                     //no tiene sentido guardarlo n veces
                     //Debe fijarse cuál (si es que hay) está seleccionado para así ponerlo
                     //Debe verificar además si el evaluador ya terminó de corregir o no para que sean no editables
                     
                     if (!$finishedCorrection) {
                        //Debe permitir que todo se edite
                     
                     if ($seleccionar===0) {
                         //Debe estar el NO tildado
                         echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '', 'name'=>'data[ItemsSi][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false)).'</td>';
                         $auxiliarID++;
                         echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '','checked' => true, 'name'=>'data[ItemsNo][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false)).'</td>';
                         $auxiliarID++;
                     }
                     
                     else if ($seleccionar===1) {
                         //Debe estar el SÍ tildado
                         echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '','checked' => true, 'name'=>'data[ItemsSi][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false)).'</td>';
                         $auxiliarID++;
                         echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '', 'name'=>'data[ItemsNo][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false)).'</td>';
                         $auxiliarID++;
                     }
                     
                     else if ($seleccionar===-1) {
                        //No hay ninguno seleccionado por default 
                        echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '', 'name'=>'data[ItemsSi][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false)).'</td>';
                        $auxiliarID++;
                        echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '', 'name'=>'data[ItemsNo][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false)).'</td>';
                        $auxiliarID++;
                     }
                     }

                     else if ($finishedCorrection) {
                        //Va a poder ver lo que puso pero sin editar
                        //Crea igual los checks pero le pone disabled=>true a cada uno de ellos
                        
                          if ($seleccionar===0) {
                             //Debe estar el NO tildado
                             echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '', 'name'=>'data[ItemsSi][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false, 'disabled'=>true)).'</td>';
                             $auxiliarID++;
                             echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '','checked' => true, 'name'=>'data[ItemsNo][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false, 'disabled'=>true)).'</td>';
                             $auxiliarID++;
                         }

                         else if ($seleccionar===1) {
                             //Debe estar el SÍ tildado
                             echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '','checked' => true, 'name'=>'data[ItemsSi][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false, 'disabled'=>true)).'</td>';
                             $auxiliarID++;
                             echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '', 'name'=>'data[ItemsNo][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false, 'disabled'=>true)).'</td>';
                             $auxiliarID++;
                         }

                         else if ($seleccionar===-1) {
                            //No hay ninguno seleccionado por default 
                            echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '', 'name'=>'data[ItemsSi][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false, 'disabled'=>true)).'</td>';
                            $auxiliarID++;
                            echo '<td>'.$this->Form->input($auxiliarID,array('type' => 'checkbox','label' => '', 'name'=>'data[ItemsNo][Item'.$indice.']','value'=>$itemID, 'onclick'=> 'controlarVecino('.$auxiliarID.');', 'hiddenField'=>false, 'disabled'=>true)).'</td>';
                            $auxiliarID++;
                         }    
                     }
                     
                     
                     //Incrementa el contador
                     $indice++;
                    }
                echo '</tr>';    
               
                                        }
                                        
                                        
               //Me envio la cantidad de inputs seleccionables
               echo $this->Form->input('cantidadInputs', array('type' => 'hidden', 'name'=>'data[Items][cantidad]','value'=>$indice));
               
               //Lo mismo con el studentID y el problemID para tenerlo luego
               echo $this->Form->input('problemID', array('type' => 'hidden', 'name'=>'data[Problem][id]','value'=>$problemID));
               echo $this->Form->input('studentID', array('type' => 'hidden', 'name'=>'data[Student][id]','value'=>$studentID));
               echo $this->Form->input('questionID', array('type' => 'hidden', 'name'=>'data[Question][id]','value'=>$questionID));
               
            ?>
            </table>
    </div>

</div>
</div>

<div class="row">
    <div class="col-md-2">
        <?php 
            $idAnterior=$vecinos['anterior'];
             if (!($idAnterior==null)){//Si existe un vecino anterior 
                 echo $this->Form->input('anteriorID', array('type' => 'hidden', 'name'=>'data[Anterior][id]','value'=>$idAnterior));
                 echo $this->Form->submit(__('Anterior'), 
                            array('class' => 'btn btn-primary btnSiguiente',
                            'style' => 'font-size: 14px; padding: 10px; border-radius: 2px;',
                            'name'=>'botonAnterior'));
             }else{
                 echo $this->Form->input('anteriorID', array('type' => 'hidden', 'name'=>'data[Anterior][id]','value'=>$idAnterior));  
             }
        ?>
    </div>
    <div class="col-md-2 col-md-offset-3">
    <?php
	echo $this->Html->link(__('Volver'), array('controller'=>'ManagerEvaluator','action' => 'assignedstudents', $questionID),
            array('class' => 'btn btn-sm btn-primary btn-md', 'style' => 'font-size: 13px; padding: 10px; border-radius: 4px;')); 
    ?>
    </div>
    <div class="col-md-2 col-md-offset-3">
        <?php
            $idSiguiente=$vecinos['siguiente'];
             if (!($idSiguiente==null)){//Si existe un vecino siguiente 
                echo $this->Form->input('siguienteID', array('type' => 'hidden', 'name'=>'data[Siguiente][id]','value'=>$idSiguiente));
                echo $this->Form->submit(__('Siguiente'), 
                            array('class' => 'btn btn-primary btnSiguiente',
                            'style' => 'font-size: 14px; padding: 10px; border-radius: 2px;',
                            'name'=>'botonSiguiente'));
               
             }else{ //En caso de no existir un siguiente envía null
               echo $this->Form->input('siguienteID', array('type' => 'hidden', 'name'=>'data[Siguiente][id]','value'=>$idSiguiente));
               echo $this->Form->submit(__('Siguiente'), 
                             array('class' => 'btn btn-sm btn-primary btnSiguiente',
                             'style' => 'font-size: 13px; padding: 10px; border-radius: 4px;',
                             'name'=>'botonSiguiente'));

                 }
                
            
        ?>
    </div>
</div>