<div class="row">

    <h3 class="tituloAdministrador"><?php echo __('Defensa'); ?></h3>

</div>


<div class="row">
    <div class="col-md-10  col-md-offset-1">
       
        <?php 
            include '../webroot/pagination.class.php';
            $contador=1;
            
            if(!(empty($alumnos[0]))) {
            
            // If we have an array with items
            if (count($alumnos)) {
            // Create the pagination object
            $pagination = new pagination($alumnos, (isset($_GET['page']) ? $_GET['page'] : 1), 12);

            // Decide if the first and last links should show
            $pagination->setShowFirstAndLast(true);
            // You can overwrite the default seperator
            //$pagination->setMainSeperator(' | ');
            // Parse through the pagination class
            $studentPages = $pagination->getResults();
            
            // If we have items 
            if (count($studentPages) != 0) {
            // Create the page numbers
            $pageNumbers= '<div class="light-theme simple-pagination" style="margin:0 auto 0 auto; height:auto; width:auto;">'.$pagination->getLinks($_GET).'</div> ';
            
            echo 
            '<div class="row">
                <div class="col-md-10  col-md-offset-1">
                   <table class="table table-hover" style="font-size: 15px">
                       <tr class="active">
                           <th>Alumnos</th>
                           <th style="text-align: center;">Defensa realizada</th>
                           
                       </tr>'; 
            
            // Loop through all the items in the array
         
                
            
            // Loop through all the items in the array
            foreach ($studentPages  as $studentArray) {
            ?>

            <tr>

                <?php
                  //Obtiene los datos necesarios para enviarle a la otra vista
                  $pseudonimo = $studentArray['pseudonimo'];
                  $idAlumno = $studentArray['id'];
                  $defendido = $studentArray['defendido'];
                  $contador++;
                ?>
                <?php
                echo '<td>'.$this->Html->link(__($pseudonimo), array('action' => 'studentdefense',$idExam, $idAlumno)).'</td>';
                ?>
                <td style="text-align: center;"><?php 
                    if ($defendido){
                        $clase= 'fa fa-check  corregido';
                    }
                    else{
                        $clase='fa fa-times noCorregido';
                    }
                        
                echo $this->Html->div($clase.' fa-2x'); ?></td>

            </tr>
            <?php }
            // print out the page numbers beneath the results
            echo '</table></div></div>';
            echo '<div class="row"><div class="col-md-10  col-md-offset-1">';
                   
            echo $pageNumbers;

            echo '</div></div>';
            }}}
            
            else {?>

            <tr>
                <th>No hay alumnos para corregir</th>
            </tr> 

            <?php
            
            }
        
            ?>
        
         
    </div>
</div>
<div class="row">
    <div class="col-md-6">
    <?php

	echo $this->Html->link(__('Volver'), array('controller'=>'ManagerAdmin','action' => 'examstate', $idExam),
            array('class' => 'btn btn-sm btn-primary btn-lg btnSiguiente', 'style' => 'float: right; font-size: 13px; border-radius: 4px; margin-top:40px;')); 
    ?>
    </div>

    <div class="col-md-6">
 <?php    
    if ($examenDefendido===true) {
        echo $this->Html->link(__('Finalizar defensa'), array(),array('disabled'=>'true','class' => 'btn btn-sm btn-danger btn-lg', 'style' => 'font-size: 12px; border-radius: 4px; margin-top:40px;'));     
    }
    
    else {
        echo $this->Html->link(__('Finalizar defensa'), array('action' => 'enddefense', $idExam  ),array('class' => 'btn btn-sm btn-danger btn-lg', 'style' => 'font-size: 12px; border-radius: 4px; margin-top:40px;'), 
                "¿Está seguro que desea finalizar la defensa? Una vez finalizada no se podrán hacer cambios."); 
    }
 
 ?>
    </div>
</div>


