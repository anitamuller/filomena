<div class="row">
    <h3 class="tituloAdministrador"><?php echo __('Proceso de corrección'); ?></h3>
</div>
<div class="row">
    <div class="col-md-10  col-md-offset-1">
        <table class="table table-hover" style="font-size: 12px; margin-top: 30px;">
            <tr class="active">
                <th style="text-align: center;">Evaluador</th>
                <th style="text-align: center;">Email</th>
                <th style="text-align: center;">Casos asignados</th>
                <th style="text-align: center;">Porcentaje</th>
                <th style="text-align: center;">Finalizó corrección</th>
            </tr> 

        <?php 
            $contador=1;
            
            if(!(empty($listaEvaluadores[0]))) {
            
            foreach ($listaEvaluadores as $evaluador):
            ?>

            <tr style="border-bottom: 1px solid #DFDDDD; border-top: 1px solid #DFDDDD;"> 

                <?php
                  //Obtiene los datos necesarios para enviarle a la otra vista
                  $nombre = $evaluador['nombre'];
                  $apellido = $evaluador['apellido'];
                  $nombreCompleto = $apellido . ', ' . $nombre;
                  $email = $evaluador['email'];
                  $casos = $evaluador['casos'];
                  $terminoCorregir = $evaluador['terminoCorreccion'];
                  $contador++;
                ?>
                <?php
                echo '<td style="text-align: center;">'.$nombreCompleto.'</td>';
                echo '<td style="text-align: center;">'.$email.'</td>';
                echo '<td style="text-align: center;">'.$casos.'</td>';
                
                $porcentaje = $evaluador['porcentajeCorreccion'];
                $stringPorcentaje = $porcentaje . '%';
                
                ?>
                <td style="text-align: center;">
                    <div class="demo-wrapper html5-progress-bar">
                        <div class="progress-bar-wrapper">
                            <progress id="progressbar" min="0" max="100" value="<?php echo $porcentaje; ?>" >
                            </progress>
                            <span class="progress-value"> <?php echo $stringPorcentaje; ?></span> 
                        </div>
                    </div>
                </td>


                <td style="text-align: center;"><?php 
                        if ($terminoCorregir){
                            $clase= 'fa fa-check  corregido';
                        }
                        else{
                            $clase='fa fa-times noCorregido';
                        }
                        
                        echo $this->Html->div($clase.' fa-2x'); ?></td>
            </tr>
        <?php endforeach; 
            }
            
            else {?>

            <tr>
                <th>No hay evaluadores asignados</th>
            </tr> 

            <?php
            
            }
            ?>
        </table>  
    </div>
</div>
<div class="row">
    <div class="col-md-2 col-md-offset-5">
    <?php

	echo $this->Html->link(__('Volver'), array('controller'=>'ManagerAdmin','action' => 'examstate', $idExam),
            array('class' => 'btn btn-sm btn-primary btn-sm btnSiguiente btn-lg', 'style' => 'font-size: 13px; border-radius: 4px;')); 
    ?>
    </div>
</div>


<script>
    $(document).ready(function () {
        var progressbar = $('#progressbar'),
                limite= progressbar.attr('value'),
                max = progressbar.attr('max'),
                time = (500 / max) * 5,
                value = 0;
        var loading = function () {
            
            addValue = progressbar.val(value);
            $('.progress-value').html(value + '%');
            if (value >= limite) {
                clearInterval(animate);
            }
            value += 1;
        };
        var animate = setInterval(function () {
            loading();
        }, time);
    });
</script>