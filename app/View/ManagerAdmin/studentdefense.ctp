<?php
echo $this->Form->create(null,array(
    'url'=>array('controller'=>'managerAdmin','action'=>'studentdefense')));    
?>


<div class="row">

    <?php
       $salida = 'Alumno '.'<div class="nombreColor">'.$pseudonimo.'</div>';       
    ?>
        <h3 class="tituloAdministrador"><?php echo __($salida); ?></h3>

</div>



<div class="row">
    <div class="col-md-10  col-md-offset-1">

        <table class="table table-hover" style="font-size: 12px ;margin-top: 30px;">
            <tr class="active" style="font-size: 13px">
                <th>Casos</th>
                <th style="text-align: center;">Defendido</th>
            </tr> 
            
            	<?php foreach ($problemasADefender as $problema): ?>
                <tr>
                    <td><?php echo $this->Html->link(__($problema['titulo']), array('action' => 'collationtableproblem', $idExam, $idAlumno, $problema['id']  )); ?>&nbsp;</td>
                    <td style="text-align: center;"><?php 
                    if ($problema['defendido']){
                        $clase= 'fa fa-check  corregido';
                    }
                    else{
                        $clase='fa fa-times noCorregido';
                    }
                        
                echo $this->Html->div($clase.' fa-2x'); ?></td>
                </tr>
                <?php endforeach; ?>
                
        </table>    
        </div>
        </div>
        
        <div class="row">

    <div class="col-md-6">
        <?php
        
        echo $this->Html->link(__('Volver'), array('controller'=>'ManagerAdmin','action' => 'studentsdefense', $idExam),
            array('class' => 'btn btn btn-primary btn-sm btnSiguiente btn-lg', 'style' => 'float: right; font-size: 13px; border-radius: 4px')); 
            
        
        ?>
    </div>
