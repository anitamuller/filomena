<div>
    <legend style="width: 70%;">Bienvenido <?php echo CakeSession::read("Auth.User.username"); ?>!</legend>

    <?php
    App::import('Vendor', 'PHPExcel', array('file' => 'excel/Classes/PHPExcel.php'));
if ($defensaCompleta){
// Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

// Set document properties
    $objPHPExcel->getProperties()->setCreator("Filomena")
            ->setLastModifiedBy("Filomena")
            ->setTitle("Reporte")
            ->setSubject("Reporte")
            ->setDescription("Reporte")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Resultados");


    $objPHPExcel->getActiveSheet()->setTitle('NotasFilomena');
    $objPHPExcel->setActiveSheetIndex(0);

    $style = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        )
    );

PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

$objPHPExcel->getActiveSheet()
    ->getColumnDimension('A')
    ->setAutoSize(true);

$objPHPExcel->getActiveSheet()
    ->getColumnDimension('B')
    ->setAutoSize(true);

      $cantAlumnos = count($pseudonimos);
      

    
    
    
    $col = 2;
    $row = 2;


    
  

    ////encabezado (nombre del caso y secciones)
    $cantSecciones = count($secciones);
    for ($z = 0; $z < $cantProblemas; $z++) {
        $start = $col;
        $end = $col + $cantSecciones - 1;


        $start = PHPExcel_Cell::stringFromColumnIndex($start);
        $end = PHPExcel_Cell::stringFromColumnIndex($end);
        $merge = "$start{$row}:$end{$row}";





        $objPHPExcel->getActiveSheet()->mergeCells($merge);
        $objPHPExcel->getActiveSheet()->getStyle($merge)->applyFromArray($style);
        $objPHPExcel->getActiveSheet()->getStyle($merge)->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle($merge)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);


        if ($z & 1) {
            //colorear desde
            //desde $merge
            $fin = $row + $cantAlumnos + 1;
            $comienzo = $row;

            for ($desde = $comienzo; $desde <= $fin; $desde++) {

                $celdas = "$start{$desde}:$end{$desde}";
                debug($celdas);
                $objPHPExcel->getActiveSheet()->getStyle($celdas)->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('rgb' => 'C3DED5'),
                        )
                );
            }
        } else {


            $fin = $row + $cantAlumnos + 1;
            $comienzo = $row;

            for ($desde = $comienzo; $desde <= $fin; $desde++) {

                $celdas = "$start{$desde}:$end{$desde}";
                debug($celdas);
                $objPHPExcel->getActiveSheet()->getStyle($celdas)->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('rgb' => 'EDCED2'),
                        )
                );
            }
        }

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $nombresProblemas[$z]['Problem']['title'], PHPExcel_Cell_DataType::TYPE_STRING);

        $col = $col + $cantSecciones;
    }

    $col = 1;
    $row = 4;

    $k = 3;
    $l = 2;

    for ($i = 1; $i <= $cantProblemas; $i++) {

        for ($j = 0; $j < $cantSecciones; $j++) {
            $nombreSeccion = $secciones[$j];


            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($l, $k)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($l, $k)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($l, $k)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($l, $k, $nombreSeccion, PHPExcel_Cell_DataType::TYPE_STRING);
            $l = $l + 1;
        }
    }


    //guardado de datos
    $i = 0;
    $j = 0;
    foreach ($sumaNotas as $notas):

        $indice = $pseudonimos[$i];
        $estudiantes = $notas[$indice];
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $indice, PHPExcel_Cell_DataType::TYPE_STRING);


        foreach ($estudiantes as $estudiante):
            //debug($estudiante);

            foreach ($estudiante as $nota):

                debug($nota);
                $nombreSeccion = $secciones[$j];
                $notaSeccion = $nota[$nombreSeccion];
                $col = $col + 1;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $notaSeccion, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

                $j = $j + 1;
            endforeach;


            $j = 0;

        endforeach;
        $i = $i + 1;
        $row = $row + 1;
        $col = 1;

    endforeach;


      
      $columna=0; $fila=4;
      for ($q = 0; $q <= $cantAlumnos; $q++) {

          $nombreEstudiante = $nombresEstudiantes[$q];
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila, $nombreEstudiante, PHPExcel_Cell_DataType::TYPE_STRING);
        $fila++;
    }
    
    $columna=0; $fila=3;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila, 'Nombre y apellido', PHPExcel_Cell_DataType::TYPE_STRING);
    
    $columna=1; $fila=3;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($columna, $fila, 'Pseudónimo', PHPExcel_Cell_DataType::TYPE_STRING);
   
   
    $objPHPExcel->getActiveSheet()->getStyle('A3:B3')->getFont()->setBold(true);






    ob_end_clean();
//header('Content-type: application/vnd.ms-excel');//si funciona!!!
// It will be called file.xls
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //recomendado en la web pero no funciona
    header('Content-Disposition: attachment; filename="Filomena.xlsx"');
    header('Cache-Control: max-age=0 ');

//Modificar la ruta en este metodo $objWriter->save()
    ob_end_clean();
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
}  ?>

</div>