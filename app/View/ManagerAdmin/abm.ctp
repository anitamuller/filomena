<div class="users index">
    <div class="container main-container">
        
        <div class="panel panel-primary user-actions">
        <div class="panel-heading"><?php echo __('Actions'); ?></div>
        <div class="panel-body">
            <ul class="nav nav-pills nav-stacked">           
                <li><?php echo $this->Html->link(__('ABM Users'), array('controller'=>'users','action' => 'index')); ?></li>
            </ul>
        </div>
    </div>
