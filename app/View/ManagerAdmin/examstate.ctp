<div class="row">
    <tr>
    <div style="text-align: center; margin: 0 auto;">
        
        <div class="btn-group-vertical" style="text-align: center; margin: 0 auto; margin-top: 100px;">
       <!-- <div class="btn-group btn-group-justified" style="width: 110%; padding-top: 100px;" role="group" aria-label="...">
        <div class="btn-group" role="group">-->
            <td> <?php 
                        include '../webroot/pathGetter.php';

                        //Obtengo la url base que la empleo para redirecciones absolutas
                        $urlBase = obtenerPath();
                        
                        $publicado = $exam['Exam']['published'];
                        $idExam = $exam['Exam']['id'];
                        
                        if (!$publicado) {
                            //Examen no publicado, muestro el botón
                            $direccion = $urlBase . '/ManagerAdmin/initexam/'. $idExam;
                            echo'<a style="font-size: 13px;" class="btn btn-sm btn-info btn-lg"  href='.$direccion.'><i class="fa fa-clock-o"> </i>&nbsp; Comenzar Exámen</a>';
                        }
                        
                        else {
                            //Examen ya publicado, deshabilito el botón
                            echo'<a style="font-size: 13px;" class="btn btn-sm btn-info btn-lg"  disabled=true ><i class="fa fa-clock-o"> </i>&nbsp; Comenzar Exámen</a>';
                        }
                    
                  ?>
            </td>
            <!--</div>
            <div class="btn-group" role="group">-->
            <td> <?php 
                                               
                        $finalizado = $exam['Exam']['finish'];
            
                        if (!$finalizado) {
                            //Examen no finalizado, muestro el botón
                            $direccion = $urlBase . '/ManagerAdmin/endexam/'. $idExam;
                            echo'<a class="btn btn-sm btn-finish"  href='.$direccion.'><i class="fa fa-file-archive-o"> </i>&nbsp; Finalizar Exámen</a>'; 
                        }
                        else {
                            //Examen ya finalizado, deshabilito el botón
                            echo'<a class="btn btn-sm btn-finish"  disabled=true ><i class="fa fa-file-archive-o"> </i>&nbsp; Finalizar Exámen</a>';    
                        }  
                    ?>
            </td>
            <!--</div>
            <div class="btn-group" role="group">-->
            <td> 
                <?php
                $direccion = $urlBase . '/ManagerAdmin/corrections/'. $idExam;
                echo'<a class="btn btn-sm btn-correccion"  href='.$direccion.'><i class="fa fa-spinner"> </i>&nbsp; Proceso de Corrección</a>';
                ?>
            </td>
           <!--</div>
            <div class="btn-group" role="group">-->
            <td> <?php 
                        if (!$puedeDefender) {
                            //Le deshabilito el botón
                            echo'<a style="font-size: 13px; " class="btn btn-sm btn-danger btn-lg" disabled=true ><i class="fa fa-check-square-o"> </i>&nbsp; Defensa</a>';
                        }
                        else {
                            //Le habilito el botón
                            $direccion = $urlBase . '/ManagerAdmin/studentsdefense/'. $idExam;
                            echo'<a style="font-size: 13px; " class="btn btn-sm btn-danger btn-lg "  href='.$direccion.'><i class="fa fa-check-square-o"> </i>&nbsp; Defensa</a>';
                        }
            
                        
                    ?>
            </td>
           <!--</div>
            <div class="btn-group" role="group">-->
            <td> <?php 
                        if ($defendido) {
                            //Ya defendió, puede exportar
                            $direccion = $urlBase . '/ManagerAdmin/export/'. $idExam;
                            echo'<a class="btn btn-sm  btn-lg btn-exportar"  href='.$direccion.'><i class="fa fa-download"> </i>&nbsp; Exportar Datos</a>';
                        }
                        
                        else {
                            //No defendió, le deshabilito el botón
                            echo'<a style="font-size: 13px; background-color:#009900; color: white;" class="btn btn-sm  btn-lg" disabled=true ><i class="fa fa-download"> </i>&nbsp; Exportar Datos</a>';
                        } 
                    ?>
            </td>
            <!--</div>
            <div class="btn-group" role="group">-->
            </tr>
        </div>
        </div>
    </div>