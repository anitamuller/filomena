<?php

echo $this->Form->create(null,array(
    'url'=>array('controller'=>'managerAdmin','action'=>'collationtableproblem')));    
?>
<div class="row">
    <h3>Alumno <div class="nombreColor"><?php echo $alumno;?></div></h3>
    <h4><?php echo $titulo; ?></h4>
</div>
<div class="row" style="margin-top:7px;">
    <table class="table-condensed " style="font-size: 12px; margin: auto; border-bottom: 30px;">
        <?php 
            $contador=1;
            $indexSeccion=0;
            
            if(!(empty($itemsADefender[0]))) {
                
            
            foreach ($itemsADefender as $seccion):
        
                echo '<td style="color: #006687; font-size: 16px; padding-bottom: 30px; padding-top: 40px;"><strong>'.'Sección: ' . $secciones[$indexSeccion].'</strong></td>';
                
                //Sirven para calcular cuantos puntos obtuvo del total y darle al administrador el rango de valores para poner en el item de defensa
                $puntajeTotal=0;
                $puntajeAlumno=0;
                
                foreach($seccion as $item):
                    ?>
                    <tr style="border-bottom: 1px solid #DFDDDD; border-top: 1px solid #DFDDDD;"> 

                    <?php
                    $nombre = $item['nombre'];
                    $porcentaje = $item['porcentaje'];
                    $correccion = $item['correcion'];
                    $idSeccion = $item['idSeccion'];
                    $puntajeTotal= $puntajeTotal + $porcentaje;   
                    ?>
            
                    <?php
                    if ($nombre !='Defensa'){
                        echo '<td>'.$nombre.'</td>';
                    ?>

                    <td style="text-align: center;"><?php 
                        if ($correccion){
                            $clase= 'fa fa-check  corregidoAdmin';
                            $puntajeAlumno= $puntajeAlumno + $porcentaje;  
                            
                        }
                        else{
                            $clase='fa fa-times noCorregidoAdmin';
                        }
                        
                    echo $this->Html->div($clase.' fa-2x'); 
                
                    }
                    else{
                    
                    $idEvalItemDefensa = 'defensa'.$indexSeccion;
                    ?>


                    <?php 
                    if ($noNumericos[$indexSeccion]){ ?>
            <td>
                <label for="Item de Defensa" style="color: red; font-weight: bold;">Item de defensa</label></br>
            </td>
            <td class="itemDefensa">
                <input maxlength="4" size="2" value="<?php echo $valoresDefault[$indexSeccion] ;?>" name="<?php echo $idEvalItemDefensa; ?>" class="inputErrorDefensa" style="border-color: tomato ; border: none; margin-left: 25px; background-color: tomato; color: white;" type="text" id="<?php echo $idEvalItemDefensa;?>" required>
            </td>

                    <?php } else {
                    if ($errores[$indexSeccion]){?>
            <td>
                <label for="Item de Defensa" style="color: red; font-weight: bold;">Item de defensa</label></br>
            </td>
            <td class="itemDefensa">
                <input maxlength="4" size="2" value="<?php echo $valoresDefault[$indexSeccion] ;?>" name="<?php echo $idEvalItemDefensa; ?>" class="inputErrorDefensa" style="border-color: tomato ; border: none; margin-left: 25px; background-color: tomato; color: white;" type="text" id="<?php echo $idEvalItemDefensa; ?>" required>
            </td>


                    <?php 
                    } else { ?>
            <td>
                <label for="Item de Defensa" style="font-weight: bold; color: #006687;">Item de defensa</label></br>
            </td>
            <td class="itemDefensa">
                
                <?php
                    //Me fijo si defendió o no para saber cómo poner el item de defensa
                    if ($defendido):
                    //Está defendido, lo deshabilito
                    ?>
                        <input maxlength="4" size="2" value="<?php echo $valoresDefault[$indexSeccion] ;?>" name="<?php echo $idEvalItemDefensa; ?>" class="inputDefensa" type="text" id="<?php echo $idEvalItemDefensa; ?>" required disabled>
                    
                    <?php
                    endif;
                    
                    if (!$defendido):
                        //No fue defendido, lo permito  
                    ?> 
                        <input maxlength="4" size="2" value="<?php echo $valoresDefault[$indexSeccion] ;?>" name="<?php echo $idEvalItemDefensa; ?>" class="inputDefensa" type="text" id="<?php echo $idEvalItemDefensa; ?>" required>
                    
                    <?php
                    endif;
                ?>
            </td>
                    <?php
                    }
                    }}
               
                    ?></td>
                    <?php
                       if ($nombre !='Defensa'){ //Si no es de defensa pongo el valor del item   
                           echo ' <td style="text-align: center; font-weight: bold; color:#424242">'.$porcentaje.'</td>';
                       }else{
                           echo ' <td style="text-align: center; font-weight: bolder; font-size:13px; color:#006687;">[0-'.($puntajeTotal-$puntajeAlumno).']</td>';
                       }
                    ?>
            </td>

        </tr><?php
                endforeach; 
                $indexSeccion++;
            endforeach; 
            }
            
            else {?>
        <tr>
            <th>No hay secciones</th>
        </tr> 

            <?php
            
            }
            ?>
    </table>  

</div>
<div class="row">

    <div class="col-md-6">
        <?php
        echo $this->Form->input('idProblema', array('type' => 'hidden', 'value' => $idProblema));
        echo $this->Form->input('idAlumno', array('type' => 'hidden', 'value' => $idAlumno));
        echo $this->Form->input('idSecciones', array('type' => 'hidden', 'value' => serialize($idSecciones)));
        echo $this->Form->input('idExam', array('type' => 'hidden', 'value' => $idExam));
        
        echo $this->Html->link(__('Volver'), array('controller'=>'ManagerAdmin','action' => 'studentdefense', $idExam, $idAlumno),
            array('class' => 'btn btn btn-primary btn-sm btnSiguiente btn-lg', 'style' => 'float: right; font-size: 13px; border-radius: 4px;margin-top: 40px;')); 
            
        
        ?>
    </div>
    <div class="col-md-6">
        <?php
        
            //Si está defendido, no voy a dejar que haga submit, le deshabilito el guardar defensa
            if ($defendido) {
                //Está defendido, lo deshabilito
                echo $this->Html->link(__('Guardar Defensa'),array(), 
                            array('disabled'=>'true','class' => 'btn btn-sm btn-danger btn-lg',
                            'style' => 'font-size: 13px;  border-radius: 4px;margin-top: 40px;'));   
            }
            
            else {
                //No está defendido, lo dejo habilitado
                echo $this->Form->submit(__('Guardar Defensa'), 
                            array('class' => 'btn btn-sm btn-danger btn-lg',
                            'style' => 'font-size: 13px;  border-radius: 4px; margin-top: 40px;'));
            }
        ?>
    </div>
</div>


