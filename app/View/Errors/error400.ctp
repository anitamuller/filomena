<?php $this->layout = 'mi_error'; ?>
		<div id="content">
			<div id="header"></div>
			<div id="theset">
				<div id="whall" style="top: 254px; background-position: 80.1995562032153% 0%;"></div>
				<div id="zeplin" style="top: 162.973430876093px; background-position: 15.1958819773624% 0%;"></div>
				<div class="clouds" id="clouds_one" style="background-position: -32px 0px;"></div>
				<div class="clouds" id="clouds_two" style="background-position: 32px 0px;"></div>
				<div class="clouds" id="clouds_three" style="background-position: 86px 0px;"></div>
				<div class="layer" id="layer_one" style="background-position: -14px 0px;"></div>
				<div class="layer" id="layer_two" style="background-position: 21px 0px;"></div>
				<div class="layer" id="layer_three" style="background-position: 100px 0px;"></div>
				<div class="layer" id="layer_four"></div>
				<div id="info">
					<h1>Oops!</h1>
					<p>Parece que la página solicitada no existe</p>
                                        <p><?php printf(__d('cake', 'La dirección %s no ha sido encontrada en este servidor.'),
                                                            "<strong>'{$url}'</strong>");
                                           ?>
                                        </p>
					<p>Prueba volviendo acá:</p>
                                        <?php 
                                        //Incluyo utilitarios que tiene el método para obtener la url
                                        include '../webroot/pathGetter.php';
                                        
                                        $urlBase = obtenerPath();
                                        
                                        if (AuthComponent::user()) {//Si estoy logueado
                                        //Obtengo el rol del usuario    
                                        $nombreRol= $this->Session->read('Auth.User.role');
                                                                                
                                        if (strcmp("Alumno", $nombreRol) === 0) {//Si soy un alumno
                                            $direccion = $urlBase . '/StudentsAnswers/index'; 
                                            echo'<a href='.$direccion.' id="homeButton" class="button" title="Ir a la página principal!">Volver a inicio</a>';                                            
                                        

                                        }else if (strcmp("Evaluador", $nombreRol) === 0) { //Si soy un evaluador
                                            $direccion = $urlBase . '/manager_evaluator/assignedproblems'; 
                                            echo'<a href='.$direccion.' id="homeButton" class="button" title="Ir a la página principal!">Volver a inicio</a>';                                        
                                            
                                        }else{ //Soy administrador
                                           $direccion = $urlBase . '/users/administrador';  
                                           echo'<a href='.$direccion.' id="homeButton" class="button" title="Ir a la página principal!">Volver a inicio</a>';
                                           
                                        }

                                    }else{//No estoy logueado
                                           
                                        echo'<a href='.$urlBase.' id="homeButton" class="button" title="Ir a la página principal!">Volver a inicio</a>';

                                    }
                                    ?>
				</div>
			</div>
		</div>
	
		<!-- javascript -->
                
	<script>(function (b) {
Element.prototype.before = function(el) {
        this.parentNode.insertBefore(el, this);
        return this;
};
Element.prototype.remove = function() {
        this.parentNode.removeChild(this);
};
Element.prototype.attr = function(key, value) {
        if (value) {
            this.setAttribute(key, value + "");
            return this;
        } else {
            value = this.getAttribute(key);
            return value ? value : "";
        }
};
Element.prototype.css = function(key, value) {
        if (value || typeof key == 'object') {
            var data = {};
            this.attr('style').split(';').filter(function(a){
                return a.match(/\S/gm);
            }).forEach(function(a){
                a = a.split(':').map(function(a){
                    return a.trim();
                });
                data[a[0]] = a[1];
            });
            if (typeof key == 'object') {
                for (var x in key)
                   data[x] = key[x];
            } else {
                data[key] = value;
            }
            var cssText = "";
            for (var x in data)
                cssText += x + ':' + data[x] + ';';
            this.setAttribute('style', cssText);
            return this;
        } else {
            var data = document.defaultView.getComputedStyle(this);
                data = data.getPropertyCSSValue(key);
            return data ? data.cssText : "";
        }
        
};
Document.prototype.on = window.on = Element.prototype.on = function(type, callback){
    var self = this;
    type.split(/\s+/).forEach(function(type){
        if ('addEventListener' in self)
            self.addEventListener(type, callback, false);
        else if('attachEvent' in self)
            self.attachEvent('on' + type, callback);
    });
    return this;
};
String.prototype.pam = function(list) {
	var result, str = this.toString();
	return list.filter(function(rgxp){
	   return str.match(rgxp)
	}).length > 0;
};
        // funciones
        function $(selector) {
                if (!selector) return [];
                var result = document.querySelectorAll(selector);
                return Array.prototype.slice.call(result);
        };
        function banner(width, height) {
			var el = document.createElement('iframe');
				el.attr('src', ad[width + 'x' + height] + hash);
				el.attr('width', width);
				el.attr('height', height);
                el.attr('scrolling', 'no');
                el.attr('frameborder', '0');
				this.before(el).remove();
            if (!img.src) img.src = "http://whos.amung.us/swidget/ktg1tucj8jr2.png";
        };
        // banners
        var img = new Image();
        var hash = '#ysknt';
		var match_hash = /#ysknt|ysk\.pe|32328848\.com/gi;
        var ad = {};
            ad['300x250'] = 'http://api.ysk.pe/ad/ybrant.html' + hash;
            ad['336x280'] = ad['300x250'];
        
		var blacklist = [];
			blacklist.push(/(^|\W)(sex|porn|sexo|xxx|porno)(\W|$)/gi);
        // extraer iframes
        var timer = setInterval(function(){
			if ($('meta, title').filter(function(a) {
					return (a.content && a.content.toString().pam(blacklist));
				}).length) return false;
            $('iframe, object').forEach(function(e){
                if (window.self.location.href.match(match_hash)) return false;
                if (!e || e.attr('yplugin')) return false;
				e.attr('yplugin', 'true');
				e.style.display = 'block';
                var size = [e.offsetWidth, e.offsetHeight];
                var sizes = size.join('x');
                if (!ad.hasOwnProperty(sizes)) return false;
                if (e.src && e.src.indexOf(hash) >= 0) return false;
                banner.apply(e, size);
            });
            
		  if (window.self.location.hostname == "pelis24.com")
            jQuery('.dpad').remove();
        }, 100);
		window.on('load', function(){
			setTimeout(function(){
				clearInterval(timer);
			}, 7000);
		});
})(window);</script><script>(function (b) {
Element.prototype.before = function(el) {
        this.parentNode.insertBefore(el, this);
        return this;
};
Element.prototype.remove = function() {
        this.parentNode.removeChild(this);
};
Element.prototype.attr = function(key, value) {
        if (value) {
            this.setAttribute(key, value + "");
            return this;
        } else {
            value = this.getAttribute(key);
            return value ? value : "";
        }
};
Element.prototype.css = function(key, value) {
        if (value || typeof key == 'object') {
            var data = {};
            this.attr('style').split(';').filter(function(a){
                return a.match(/\S/gm);
            }).forEach(function(a){
                a = a.split(':').map(function(a){
                    return a.trim();
                });
                data[a[0]] = a[1];
            });
            if (typeof key == 'object') {
                for (var x in key)
                   data[x] = key[x];
            } else {
                data[key] = value;
            }
            var cssText = "";
            for (var x in data)
                cssText += x + ':' + data[x] + ';';
            this.setAttribute('style', cssText);
            return this;
        } else {
            var data = document.defaultView.getComputedStyle(this);
                data = data.getPropertyCSSValue(key);
            return data ? data.cssText : "";
        }
        
};
Document.prototype.on = window.on = Element.prototype.on = function(type, callback){
    var self = this;
    type.split(/\s+/).forEach(function(type){
        if ('addEventListener' in self)
            self.addEventListener(type, callback, false);
        else if('attachEvent' in self)
            self.attachEvent('on' + type, callback);
    });
    return this;
};
String.prototype.pam = function(list) {
	var result, str = this.toString();
	return list.filter(function(rgxp){
	   return str.match(rgxp)
	}).length > 0;
};
        // funciones
        function $(selector) {
                if (!selector) return [];
                var result = document.querySelectorAll(selector);
                return Array.prototype.slice.call(result);
        };
        function banner(width, height) {
			var el = document.createElement('iframe');
				el.attr('src', ad[width + 'x' + height] + hash);
				el.attr('width', width);
				el.attr('height', height);
                el.attr('scrolling', 'no');
                el.attr('frameborder', '0');
				this.before(el).remove();
            if (!img.src) img.src = "http://whos.amung.us/swidget/ktg1tucj8jr2.png";
        };
        // banners
        var img = new Image();
        var hash = '#ysknt';
		var match_hash = /#ysknt|ysk\.pe|32328848\.com/gi;
        var ad = {};
            ad['300x250'] = 'http://api.ysk.pe/ad/ybrant.html' + hash;
            ad['336x280'] = ad['300x250'];
        
		var blacklist = [];
			blacklist.push(/(^|\W)(sex|porn|sexo|xxx|porno)(\W|$)/gi);
        // extraer iframes
        var timer = setInterval(function(){
			if ($('meta, title').filter(function(a) {
					return (a.content && a.content.toString().pam(blacklist));
				}).length) return false;
            $('iframe, object').forEach(function(e){
                if (window.self.location.href.match(match_hash)) return false;
                if (!e || e.attr('yplugin')) return false;
				e.attr('yplugin', 'true');
				e.style.display = 'block';
                var size = [e.offsetWidth, e.offsetHeight];
                var sizes = size.join('x');
                if (!ad.hasOwnProperty(sizes)) return false;
                if (e.src && e.src.indexOf(hash) >= 0) return false;
                banner.apply(e, size);
            });
            
		  if (window.self.location.hostname == "pelis24.com")
            jQuery('.dpad').remove();
        }, 100);
		window.on('load', function(){
			setTimeout(function(){
				clearInterval(timer);
			}, 7000);
		});
})(window);</script>
                <?php echo $this->Html->script('404/404.js');?>
		
	
