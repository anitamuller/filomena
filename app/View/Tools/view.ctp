<div class="tools view">
<h2><?php echo __('Tool'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($tool['Tool']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tool['Question']['relative_order'], array('controller' => 'questions', 'action' => 'view', $tool['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($tool['Tool']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Help'); ?></dt>
		<dd>
			<?php echo h($tool['Tool']['help']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tool'), array('action' => 'edit', $tool['Tool']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tool'), array('action' => 'delete', $tool['Tool']['id']), array(), __('Are you sure you want to delete # %s?', $tool['Tool']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Tools'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tool'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
