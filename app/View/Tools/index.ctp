<div class="tools index">
    <div class="container main-container">

        <div class="panel panel-primary user-actions">
            <div class="panel-heading"><?php echo __('Actions'); ?></div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked">           
              <li><?php echo $this->Html->link(__('New Tool'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
                </ul>
            </div>
        </div>

        <legend style="width: 70%;"><?php echo __('Tools'); ?></legend>

        <div class="panel panel-default user-content">
            <div class="panel-body">
                <table class="table table-striped table-hover table-info">
                    <tr>
  
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('question_id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('help'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tools as $tool): ?>
	<tr>
		<td><?php echo h($tool['Tool']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($tool['Question']['relative_order'], array('controller' => 'questions', 'action' => 'view', $tool['Question']['id'])); ?>
		</td>
		<td><?php echo h($tool['Tool']['name']); ?>&nbsp;</td>
		<td><?php echo h($tool['Tool']['help']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $tool['Tool']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $tool['Tool']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $tool['Tool']['id']), array(), __('Are you sure you want to delete # %s?', $tool['Tool']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<small><?php
                echo $this->Paginator->counter(array(
                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                ));
                ?>          
                    </small>
                <div class="pager">
                <?php
                echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                ?>
                </div>
            </div>
        </div>