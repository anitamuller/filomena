<div class="tools form">
<?php echo $this->Form->create('Tool'); ?>
	<fieldset>
		<legend><?php echo __('Add Tool'); ?></legend>
	<?php
		echo $this->Form->input('question_id');
		echo $this->Form->input('name');
		echo $this->Form->input('help');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Tools'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
