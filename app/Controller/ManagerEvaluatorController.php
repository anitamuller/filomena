<?php

include '../webroot/utilitarios.php';
App::uses('AppController', 'Controller');

class ManagerEvaluatorController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    //No buscan ningun modelo asociado con la linea que viene abajo
    var $uses = array();

    
    /*
     * Carga los modelos necesarios para mostrar en una vista los problemas
     * asociados a un evaluador.
     */

    public function assignedproblems() {

        //Cargado de modelos necesarios
        $this->loadModel('Evaluator');
        $this->loadModel('Problem');

        //Obtiene el evaluador
        $evaluador = $this->_obtenerEvaluador();

        //Obtiene los problemas asociados al evaluador encontrado
        $problemasAsociados = $evaluador['Problem'];


        //Lista de problemas asociados a un alumno
        $problemas = array(array());
        //Emplea un contador
        $index = 0;
        
        //Variable para saber si todos los problemas fueron corregidos en la vista
        $todosCorregidos = true;

        foreach ($problemasAsociados as $problema) {

            //Obtengo el id del problema
            $idProblema = $problema['id'];
            $ordenRelativo = $problema['relative_order'];
            $tituloProblema = $problema['title'];


            //Determina si el problema ha sido corregido en su totalidad o no y lo envia
            //a la vista asociada
            $corregido = $this->_problemaCorregido($idProblema);

            if ($corregido==false) {
                //No están todos corregidos
                $todosCorregidos=false;
            }
            
            
            //Guarda en problemas los datos relevantes de cada problema que
            //el evaluador tiene asociado
            $problemas[$index]['id'] = $idProblema;
            $problemas[$index]['nombreProblema'] = 'Caso N°' . $ordenRelativo . ': ' . $tituloProblema;
            $problemas[$index]['corregido'] = $corregido;
            $problemas[$index]['orden'] = $ordenRelativo;

            //Incrementa el contador de control
            $index++;
        }

        //Ordena los problemas para mostrarlos en la vista de acuerdo al orden
        //establecido para los mismos
        if (!(empty($problemas[0]))) {
            $problemasOrdenados = ordenar($problemas, array('orden'), SORT_ASC);
        }
      
        //Obtiene el valor asociado a la finalización del evaluador
        $finishedEvaluator = $evaluador['Evaluator']['finish'];

        //Envia la información a la vista encargada de mostrar los datos
        $this->set(compact('problemasOrdenados', 'todosCorregidos', 'finishedEvaluator'));
    }

    /*
     * Dado un id de un problema, obtiene las preguntas asociadas
     * a éste y las muestra por pantalla para que puedan seleccionarse
     */

    public function assignedquestions($problemID = null) {

        //Cargado de modelos necesarios
        $this->loadModel('Question');
        $this->loadModel('Problem');


        if (!$this->Problem->exists($problemID)) {
            $this->Session->setFlash(__('El problema especificado es inválido.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }
        
        //Ojo, el problema podría existir pero haber forzado la URL y ser uno que NO le corresponde a ese evaluador
        $esProblemaAsignado = $this->_esProblemaAsignado($problemID);
        
        if (!$esProblemaAsignado) {
            $this->Session->setFlash(__('El problema especificado es inválido.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }
        

        //El ID del problema existe, obtiene las preguntas asociadas al mismo
        //Verifica que no sea null porque aquellas que tienen null son las últimas del
        //problema y no deben ser corregidas
        $preguntasAsociadasaProblema = $this->Question->find('all', array(
            'conditions' => array('problem_id' => $problemID, "not" => array("questions" => null)),
            'recursive' => -1
        ));


        //Invoca a un método privado para conseguir el título bien formado del problema
        $tituloProblemaAsociado = $this->_armarTituloProblema($problemID);



        //Guarda los datos necesarios para enviarle a la vista
        $preguntas = array(array());
        //Emplea un contador
        $index = 0;


        foreach ($preguntasAsociadasaProblema as $pregunta) {

            //Obtengo el id y el orden de la pregunta
            $idPregunta = $pregunta['Question']['id'];
            $ordenRelativo = $pregunta['Question']['relative_order'];


            //Consulta si la pregunta del problema en cuestión (mediante sus IDs) ha sido corregida para
            //todos los alumnos que rindieron el examen
            $preguntaCorregida = $this->_preguntaCorregida($idPregunta, $problemID);


            //Guarda en preguntas los datos relevantes de cada una de ellas que
            $preguntas[$index]['id'] = $idPregunta;
            $preguntas[$index]['orden'] = $ordenRelativo;
            $preguntas[$index]['corregida'] = $preguntaCorregida;

            $index++;
        }


        //Ordena las preguntas para mostrarlas en la vista
        if (!(empty($preguntas[0]))) {
            $preguntasOrdenadas = ordenar($preguntas, array('orden'), SORT_ASC);
        }


        //Envia la información a la vista encargada de mostrar los datos
        $this->set(compact('preguntasOrdenadas', 'tituloProblemaAsociado'));
    }
    
    
    /*
     * Dado un id de un problema y un evaluador que lo obtiene de la session
     * se fija si el problema que le pasan ha sido realmente asignado a este o no
     */
    protected function _esProblemaAsignado($idProblema) {
        
        $this->loadModel('Evaluator');
        $this->loadModel('Problem');
        
        //Obtiene el evaluador
        $evaluador = $this->_obtenerEvaluador();

        //Obtiene los problemas asociados al evaluador encontrado
        $problemasAsociados = $evaluador['Problem'];
        
        //Para cada uno de ellos obtengo el id
        foreach($problemasAsociados as $problema) {
            $idProblemaAsociado = $problema['id'];
            
            if ($idProblemaAsociado==$idProblema) return true;
        }
        
        //Llegado este punto ninguno coincide
        return false;
        
    }
    

    /*
     * Dado el id de una pregunta, lista los alumnos que rindieron el examen
     */

    public function assignedstudents($questionID = null) {

        //Cargado de modelos necesarios
        $this->loadModel('Question');
        $this->loadModel('Student');

        //Verifica que el id enviado sea válido
        if (!$this->Question->exists($questionID)) {
            $this->Session->setFlash(__('La pregunta especificada es inválida.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }
        
        //Obtiene la pregunta para obtener el id del problema
        $pregunta = $this->Question->find('first', array(
            'conditions' => array('id' => $questionID),
            'recursive' => -1
        ));
        
        //Podría pasar que exista pero que sea null y en este caso también le muestro lo mismo
        $esNull = $pregunta['Question']['questions'] == null; 
        if ($esNull) {
            $this->Session->setFlash(__('La pregunta especificada es inválida.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }


        //Obtiene el id del problema asociado a esa pregunta
        $problemID = $pregunta['Question']['problem_id'];
        
        
        //Me fijo que el problema asociado a la pregunta sea realmente uno que tengo asignado
        //Ojo, el problema podría existir pero haber forzado la URL y ser uno que NO le corresponde a ese evaluador
        $esProblemaAsignado = $this->_esProblemaAsignado($problemID);
        
        if (!$esProblemaAsignado) {
            $this->Session->setFlash(__('El problema especificado es inválido.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }


        //Obtiene el titulo del problema para enviarselo a la vista
        $tituloProblemaAsociado = $this->_armarTituloProblema($problemID);
        $tituloPreguntaAsociada = 'Pregunta N°' . $pregunta['Question']['relative_order'];


        //El id enviado existe
        /* Obtiene todos los estudiantes que tengan en 1 la variable init, es decir que hayan
         * comenzando el examen pero no sé si lo han terminado aún o no.
         */
        $alumnosACorregir = $this->Student->find('all', array(
            'conditions' => array('init' => '1'),
            'recursive' => -1
        ));

        //Le mando una variable booleana a la vista para mostrar que no hay alumnos a corregir en este
        //caso
        $hayAlumnos;
        
        if (empty($alumnosACorregir[0])) {//Si no existen alumnos para corregir
            //Habrá que mostrar un error de alguna manera
            $hayAlumnos = false;
        } else {
            //Hay alumnos para corregir
            $hayAlumnos = true;
            //Guarda los datos necesarios para enviarle a la vista
            $alumnos = array(array());
            //Emplea un contador
            $index = 0;


            foreach ($alumnosACorregir as $alumno) {

                //Obtengo los datos necesarios del alumno
                $studentID = $alumno['Student']['student'];
                $pseudonimo = $alumno['Student']['pseudonim'];

                //Me fijo si el alumno terminó de rendir o no
                $finalizado = $alumno['Student']['finish'];

                //Dependiendo del valor de finish sabe si terminó de rendir o no
                if ($finalizado == true) {
                    $estadoAlumno = "Finalizado";
                    //Me fijo si la pregunta está corregida para el alumno
                    $corregida = $this->_preguntaCorregidaParaAlumno($problemID, $questionID, $studentID);
                } else {
                    $estadoAlumno = "Rindiendo";
                    //No estará corregido aún
                    $corregida = false;
                }

                //Guardo los datos para la vista
                $alumnos[$index]['id'] = $studentID;
                $alumnos[$index]['pseudonimo'] = $pseudonimo;
                $alumnos[$index]['corregida'] = $corregida;
                $alumnos[$index]['estado'] = $estadoAlumno;

                //Incrementa el index
                $index++;
            }
        }
                
        //Ordena los alumnos alfabéticamente por pseudónimo
        if (!(empty($alumnos[0]))) {
            $alumnos = ordenar($alumnos, array('pseudonimo'), SORT_ASC);
        }
        
        //Envia la información a la vista encargada de mostrar los datos
        $this->set(compact('alumnos', 'questionID', 'problemID', 'tituloProblemaAsociado', 'tituloPreguntaAsociada', 'hayAlumnos'));
    }

    /*
     * Acción encargada de mostrar información relativa asociada a la respuesta de un alumno
     * a una pregunta de un determinado problema y también de guardar la corrección que el
     * evaluador lleva adelante de la misma.
     */

    public function assignedcorrection($studentID = null, $problemID = null, $questionID = null) {

        //Como se puede invocar desde afuera, debe verificar que los tres ids sean válidos
        //Cargado de modelos necesarios
        $this->loadModel('Question');
        $this->loadModel('Student');
        $this->loadModel('Problem');
        $this->loadModel('StudentsAnswer');
        $this->loadModel('Evaluator');

        //Verifica que el id enviado sea válido
        if ((!$this->Question->exists($questionID)) || (!$this->Student->exists($studentID)) || (!$this->Problem->exists($problemID))) {
            $this->Session->setFlash(__('Al menos uno de los IDS proporcionados en la URL (alumno, problema o pregunta) es inválido.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }
        
        //Me fijo que el problema asociado a la pregunta sea realmente uno que tengo asignado
        //Ojo, el problema podría existir pero haber forzado la URL y ser uno que NO le corresponde a ese evaluador
        $esProblemaAsignado = $this->_esProblemaAsignado($problemID);
        
        if (!$esProblemaAsignado) {
            $this->Session->setFlash(__('El problema especificado es inválido.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }
               
        //Verifica que el alumno asociado realmente haya terminado, ya que no se puede corregir a un alumno que no terminó
        //Recupero al alumno y me fijo su finish
        //Recupera el alumno empleando el ID para tener su pseudónimo
        $alumno = $this->Student->find('first', array(
            'conditions' => array('student' => $studentID),
            'recursive' => '-1'
        ));
        
        //Me fijo si terminó 
        $finishAlumno = $alumno['Student']['finish'];
        
        if($finishAlumno==0) {
            //El alumno no terminó de rendir, no le puedo mostrar la página para corregir entonces
            //Se lo informo y redirijo a 
            $this->Session->setFlash(__('No es posible corregir el alumno escrito en la URL porque no ha terminado de rendir.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
            
        }

        //En este punto los ids son válidos
        //Dado el alumno y la pregunta, recupera la respuesta del mismo a esa pregunta
        $respuestaAsociada = $this->StudentsAnswer->find('first', array(
            'conditions' => array('student_id' => $studentID, 'question_id' => $questionID),
            'recursive' => '-1'
        ));


        /* Podría darse el caso de que un alumno no llegue a terminar todo el examen y entonces hayan respuestas que no
         * figuran en esta tabla. Aquí hay dos opciones, o cuando el administrador pone finalizar se generan las faltantes
         * para cada alumno, o bien no se las guarda y en caso de faltar se asume que la respuesta fue vacía, es decir
         * que no llegó a responderla y por tanto el sistema no guardó nada para ese caso.
         * Las que efectivamente respondió vacias van a quedar vacias en el sistema. Esto es sólo para los casos en donde
         * no llega a terminar el examen.
         */

        //Por defecto asumo que el estudiante siempre responde algo
        $respuestaVacia = false;

        if (empty($respuestaAsociada)) {
            //En este caso no existe una respuesta grabada por el sistema para el alumno en cuestión y la
            //pregunta antes nombrada. El único caso donde puede pasar esto es que el alumno no termine su 
            //examen.
            //Al evaluador se le debe mostrar como vacia la respuesta en este caso.
            $respuestaVacia = true;
        } else {
            //Existe una entrada para la respuesta de este alumno a la pregunta
            $respuesta = $respuestaAsociada['StudentsAnswer']['answer'];

            if ($respuesta == '') {
                //No respondió nada para esta pregunta
                $respuestaVacia = true;
            }
        }


        //Recupera el pseudónimo del alumno
        $pseudonimo = $alumno['Student']['pseudonim'];


        //Arma el titulo del problema para pasarselo a la vista
        $tituloCaso = $this->_armarTituloProblema($problemID);


        //Arma el titulo de la pregunta
        $pregunta = $this->Question->find('first', array(
            'conditions' => array('id' => $questionID),
            'recursive' => -1
        ));
        
        $idProblemaAsociado = $pregunta['Question']['problem_id'];
        
        //Me debo fijar si se corresponden el problema con el id que efectivamente recibí
        if (!($problemID==$idProblemaAsociado)) {
            $this->Session->setFlash(__('La pregunta especificada es inválida.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }
        
        //Podría pasar que exista pero que sea null y en este caso también le muestro lo mismo
        $esNull = $pregunta['Question']['questions'] == null; 
        if ($esNull) {
            $this->Session->setFlash(__('La pregunta especificada es inválida.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }
        

        $tituloPregunta = 'Pregunta N° ' . $pregunta['Question']['relative_order'];
        //Emplea un arreglo asociativo para pasarle a la vista
        $titulos['caso'] = $tituloCaso;
        $titulos['pregunta'] = $tituloPregunta;


        //Obtiene un arreglo con información de las secciones y los items ya organizada
        $seccionesItems = $this->_seccionesItemsAsociados($problemID, $questionID, $studentID);
        
        //Para que se muestren de acuerdo al orden, las ordeno previamente
        if (!(empty($seccionesItems[0]))) {
            $seccionesItems = ordenar($seccionesItems, array('ordenRelativoSeccion'), SORT_ASC);
        }
        
       
        //Obtiene los estudiantes vecinos
        $estudiantesVecinos = $this->_obtenerVecinos($studentID);
        
               
        //Obtiene los ids para el vecino anterior y el siguiente
        //Vecino anterior
        if (empty($estudiantesVecinos['prev'])) {
            //Si  no existe un vecino anterior guardará null
            $vecinos['anterior'] = null;
        } else { //Si existe un vecino anterior guardará el id del vecino
            $vecinos['anterior'] = $estudiantesVecinos['prev']['student'];
        }


        //Vecino siguiente
        if (empty($estudiantesVecinos['next'])) {
            //Si  no existe un vecino siguiente guardará null
            $vecinos['siguiente'] = null;
        } else { //Si existe un vecino anterior guardará el id del vecino
            $vecinos['siguiente'] = $estudiantesVecinos['next']['student'];
        }

        //Obtiene el evaluador y le envía el valor de finish a la vista para que impida que los checks sean
        //editables en caso de ya haber finalizado la corrección
        $evaluador = $this->_obtenerEvaluador();
        
        //Obtiene el valor de finish del evaluador
        $finishedCorrection = $evaluador['Evaluator']['finish'];

        //Envia la información a la vista encargada de mostrar los datos
        $this->set(compact('respuestaVacia', 'respuesta', 'pseudonimo', 'titulos', 'seccionesItems', 'studentID', 'problemID', 'questionID', 'vecinos','finishedCorrection'));
    }

    /*
     * Esta acción que no posee vista alguna se encarga de, recibiendo los items que han sido tildados
     * así como el id del student y del problema:
     * 1) Dado que de manera inteligente se tiene un check tanto para SÍ como para NO, siempre me va a venir en el data
     *    request la información de cada item y su selección (si puso que SÍ o puso que NO), con lo cual, lo único que hay
     *    que hacer es verificar que siempre seleccione uno solo de los dos y que estén todos seleccionados.
     *    Esto podría hacerse mejor con javascript, por lo que todavia no estará implementado, se asumirá que siempre
     *    selecciona bien.
     * 2) Simplemente debe analizar para cada item si tocó que sí o que no, obtener la información en cada caso y proceder
     *    a crear la entrada en la tabla corrections.
     * 3) También debe rederigir a assignedcorrection con los valores adecuados según si hay siguiente o anterior y también
     *    dependiendo del botón elegido.
     * Cuando se dice crear, esto es subjetivo, habrá que hacerlo en caso de que ya no esté creado, o bien salvar el campo
     * yes_no_answer con el nuevo valor si la entrada ya existe en la tabla.
     */

    public function pubcorrections() {

        //Carga los modelos asociados
        $this->loadModel('Correction');

        //Recupera los ids enviados ocultos
        $studentID = $this->request->data['Student']['id'];
        $problemID = $this->request->data['Problem']['id'];
        $questionID = $this->request->data['Question']['id'];
        $anteriorID = $this->request->data['Anterior']['id'];
        $siguienteID = $this->request->data['Siguiente']['id'];

        //Por default no sucede ningún error
        $error = 0;
        $errorValidacion = 0;

        //Itera para cada elemento del request data, que te va a mostrar sólo los tickeados
        $cantidadIndices = $this->request->data['Items']['cantidad'];

        //Revisión de items
        //Hacer acá un método que le pasas el arreglo de los items y te chequea si cumple con las condiciones 
        //La única condición es que no haya dos items de la misma fila tildados
        $validaItems = $this->_validarItems($this->request->data);

        if ($validaItems == 0) {

            //Itera para cada item de la sección y se fija si coincide con alguno de los
            //tickeados
            for ($i = 0; $i < $cantidadIndices; $i++) {

                //Asumiendo que los items pasaron la validación me fijo cuál de los dos está seleccionado
                //Me fijo si existe o no el elemento, en cuyo caso si no existe no fue tildado y no hago nada
                $nombreItem = 'Item' . $i;

                if (array_key_exists('ItemsSi', $this->request->data)) {
                    if (array_key_exists($nombreItem, $this->request->data['ItemsSi'])) {
                        //Significa que ha tildado Sí para ese item
                        //Recupero los datos que necesito
                        $itemID = $this->request->data['ItemsSi'][$nombreItem];

                        //Tiene el id del item y el id del student, queda sólo verificar si ya hay una entrada para esa
                        //combinación creada o no. De haberla, sólo seteara el valor del yes_no_answer como 1 y de no
                        //haberla deberá crearla completamente con valor 1.
                        $correccionAsociada = $this->Correction->find('first', array(
                            'conditions' => array('item_id' => $itemID, 'student_id' => $studentID), 'recursive' => -1));

                        //Debe fijarse si es vacia o no, ya que en caso de ser vacia habrá que crearla y en caso contrario no

                        if (empty($correccionAsociada)) {
                            //Hay que crear la entrada
                            //La creo manualmente
                            $this->Correction->create();
                            $correccionGuardar = array(
                                'Correction' => array(
                                    'item_id' => $itemID,
                                    'student_id' => $studentID,
                                    'yes_no_answer' => 1,
                                )
                            );
                            if ($this->Correction->save($correccionGuardar)) {
                                //No hace nada
                            } else {
                                $error = 1;
                                break;
                            }
                        } else {
                            //Hay que editarla mediante un saveField
                            //Para esto hay que obtener el id de la misma
                            $correctionID = $correccionAsociada['Correction']['id'];

                            //Actualizo manualmente el campo
                            $this->Correction->id = $correctionID;
                            $actualizoBienCompleted = $this->Correction->saveField('yes_no_answer', 1);

                            if (!$actualizoBienCompleted) {
                                //Ocurrió un error
                                $error = 1;
                                break;
                            }
                        }
                    }
                }

                if (array_key_exists('ItemsNo', $this->request->data)) {
                    if (array_key_exists($nombreItem, $this->request->data['ItemsNo'])) {

                        //Significa que ha tildado NO para ese item
                        //Recupero los datos que necesito
                        $itemID = $this->request->data['ItemsNo'][$nombreItem];

                        $this->Session->write('EntreitemNo', 'true');


                        //Tiene el id del item y el id del student, queda sólo verificar si ya hay una entrada para esa
                        //combinación creada o no. De haberla, sólo seteara el valor del yes_no_answer como 0 y de no
                        //haberla deberá crearla completamente con valor 0.
                        $correccionAsociada = $this->Correction->find('first', array(
                            'conditions' => array('item_id' => $itemID, 'student_id' => $studentID), 'recursive' => -1));

                        //Debe fijarse si es vacia o no, ya que en caso de ser vacia habrá que crearla y en caso contrario no

                        if (empty($correccionAsociada)) {
                            //Hay que crear la entrada
                            //La creo manualmente
                            $this->Correction->create();
                            $correccionGuardar = array(
                                'Correction' => array(
                                    'item_id' => $itemID,
                                    'student_id' => $studentID,
                                    'yes_no_answer' => 0,
                                )
                            );
                            if ($this->Correction->save($correccionGuardar)) {
                                //No hace nada
                            } else {
                                $error = 1;
                                break;
                            }
                        } else {
                            //Hay que editarla mediante un saveField
                            //Para esto hay que obtener el id de la misma
                            $correctionID = $correccionAsociada['Correction']['id'];

                            //Actualizo manualmente el campo
                            $this->Correction->id = $correctionID;
                            $actualizoBienCompleted = $this->Correction->saveField('yes_no_answer', 0);

                            if (!$actualizoBienCompleted) {
                                //Ocurrió un error
                                $error = 1;
                                break;
                            }
                        }
                    }
                }
            }
        }

        //Analizo si ocurrió un error o no
        if ($validaItems == 1) {
            $this->Session->setFlash(__('Existe al menos una fila en donde las dos opciones han sido checkeadas.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedcorrection', $studentID, $problemID, $questionID));
        } else if ($validaItems == 2) {
            $this->Session->setFlash(__('Existe al menos un item no checkeado.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedcorrection', $studentID, $problemID, $questionID));
        } else if ($error) {
            $this->Session->setFlash(__('La corrección no pudo llevarse a cabo, inténtelo nuevamente.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedcorrection', $studentID, $problemID, $questionID));
        } else {
            $this->Session->setFlash(__('Los datos han sido guardados exitosamente.'), 'default', array('class' => 'alert alert-dismissable alert-success'));

            //Para saber a donde debo redireccionar, debo necesariamente saber el botón que se
            //toco
            if (isset($this->request->data['botonAnterior'])) {//Si tocaron el botón anterior
                return $this->redirect(array('action' => 'assignedcorrection', $anteriorID, $problemID, $questionID));
            } else if (isset($this->request->data['botonSiguiente'])) { //Si tocaron el botón siguiente
                if ($siguienteID == null) {//Se terminó la correción para esa pregunta
                    return $this->redirect(array('action' => 'assignedquestions', $problemID));
                } else { //No se terminó la correción
                    return $this->redirect(array('action' => 'assignedcorrection', $siguienteID, $problemID, $questionID));
                }
            }
        }
    }
    
    
    /*
     * Esta acción está desarrollada por simplicidad. Es muy similar a pubcorrections pero es simplemente para redirigir
     * a la corrección asociada sin publicar absolutamente nada en la base de datos ni hacer ningún control. Es decir
     * se llevará a cabo siempre que la corrección haya finalizado.
     */
    public function redirectcorrection() { 
        //Recupera los ids enviados ocultos
        $studentID = $this->request->data['Student']['id'];
        $problemID = $this->request->data['Problem']['id'];
        $questionID = $this->request->data['Question']['id'];
        $anteriorID = $this->request->data['Anterior']['id'];
        $siguienteID = $this->request->data['Siguiente']['id'];
        
        //Para saber a donde debo redireccionar, debo necesariamente saber el botón que se
        //toco
        if (isset($this->request->data['botonAnterior'])) {//Si tocaron el botón anterior
            return $this->redirect(array('action' => 'assignedcorrection', $anteriorID, $problemID, $questionID));
        } else if (isset($this->request->data['botonSiguiente'])) { //Si tocaron el botón siguiente
            if ($siguienteID == null) {//Se terminó la correción para esa pregunta
                return $this->redirect(array('action' => 'assignedquestions', $problemID));
            } else { //No se terminó la correción
                return $this->redirect(array('action' => 'assignedcorrection', $siguienteID, $problemID, $questionID));
            }
        }   
    }
    
    
    
    /*
     * Esta acción no tiene vista asociada. Lo que hace es, cuando se presiona el botón de terminar corrección
     * verifica que todos los problemas hayan sido corregidos (por si lo quiere bypassear) y en caso afirmativo
     * setea para el evaluador pasado por ID la variable finish en 1.
     */
    public function finishedcorrection () {
        
       //Cargado de modelos necesarios
        $this->loadModel('Evaluator');
        $this->loadModel('Problem');
        
                
        //Obtiene el evaluador
        $evaluador = $this->_obtenerEvaluador();
        
        //Si ya fue finalizada la corrección para este evaluador, no permito que la vuelva a finalizar
        if ($evaluador['Evaluator']['finish']) {
            //Ya terminó de corregir, no permito que pase de nuevo
            $this->Session->setFlash(__('No es posible finalizar una corrección que ya fue finalizada antes.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }
        
        

        //Obtiene los problemas asociados al evaluador encontrado
        $problemasAsociados = $evaluador['Problem'];


        //Emplea un contador
        $index = 0;
        
        //Variable para saber si todos los problemas fueron corregidos en la vista
        $todosCorregidos = true;

        foreach ($problemasAsociados as $problema) {

            //Obtengo el id del problema
            $idProblema = $problema['id'];
            
            //Determina si el problema ha sido corregido en su totalidad o no y lo envia
            //a la vista asociada
            $corregido = $this->_problemaCorregido($idProblema);

            if ($corregido==false) {
                //No están todos corregidos
                $todosCorregidos=false;
                break;
            }
            
            //Incrementa el contador de control
            $index++;
        }
        
               
        if (!$todosCorregidos) {
            //Me quisieron forzar con la url y ponerlo de prepo pero faltan corregir
            $this->Session->setFlash(__('Faltan problemas por corregir'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }
        
        else {
            //Fueron todos corregidos, entonces seteo en la base de datos en 1 el finish para ese evaluador y vuelvo a dirigir a la misma página
            //Hay que editarla mediante un saveField
            //Obtengo el id del evaluador
            $evaluatorID = $evaluador['Evaluator']['evaluator'];

            //Actualizo manualmente el campo
            $this->Evaluator->id = $evaluatorID;
            $actualizoBienCompleted = $this->Evaluator->saveField('finish', 1);

            if (!$actualizoBienCompleted) {
                //Ocurrió un error
                $this->Session->setFlash(__('Ha ocurrido un error al guardar la finalización de la corrección. Inténtelo nuevamente'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                return $this->redirect(array('action' => 'assignedproblems'));
            }
            
            else {
                //No hay problemas, muestra un mensaje
                $this->Session->setFlash(__('Su corrección ha finalizado exitosamente.'), 'default', array('class' => 'alert alert-dismissable alert-success'));
                return $this->redirect(array('action' => 'assignedproblems'));
            }
        }   
    }
    
    
    
    
    
    
    

    /*
     * Valida un conjunto de checks recibidos en el request data para determinar que no haya dos de la misma fila
     * seleccionados.
     * Devuelve un entero que identifica si hay un error o no.
     * - Sin errores, es decir, valida, 0
     * - Error de dos items de la misma fila tildados: 1
     * - Error algún item que falta tildar de todos: 2
     */

    protected function _validarItems($data) {

        $cantidadIndices = $data['Items']['cantidad'];

        //Si no tocó ninguno, no va a aparecer ni el si ni el no en el request->data
        if ((!array_key_exists('ItemsSi', $data)) && (!array_key_exists('ItemsNo', $data))) {
            return 2;
        }


        for ($i = 0; $i < $cantidadIndices; $i++) {

            $tickeadoSi = false;
            $tickeadoNo = false;


            //No debe darse el caso de que dos de la misma fila vengan tildados
            $nombreItem = 'Item' . $i;

            //Esta el ItemSi
            if (array_key_exists('ItemsSi', $data)) {
                //El nombre del item no figura
                if (!array_key_exists($nombreItem, $data['ItemsSi'])) {
                    //El nombre del item no está en si
                    $tickeadoSi = false;
                } else {
                    //Variable booleana para determinar que el si está
                    $tickeadoSi = true;
                }
            }

            //Esta el ItemNo
            if (array_key_exists('ItemsNo', $data)) {
                //El nombre del item no figura
                if (!array_key_exists($nombreItem, $data['ItemsNo'])) {
                    $tickeadoNo = false;
                } else {
                    $tickeadoNo = true;
                }
            }
            
            //Guarda en la session una variable
            $debug[$i]['nombreItem'] = $nombreItem;
            $debug[$i]['tickeadoSi'] = $tickeadoSi;
            $debug[$i]['tickeadoNo'] = $tickeadoNo;
            $this->Session->write('debug',$debug);
            
            

            //Hay de la misma fila dos checks seleccionados, no valida
            if ($tickeadoSi && $tickeadoNo) {
                return 1;
            }

            //Para este item no tildó ninguna de las dos
            else if ((!$tickeadoSi) && (!$tickeadoNo)) {
                return 2;
            }
        }


        //Llegado este punto es porque siempre hubo alguno de los dos seleccionados
        //o ninguno
        return 0;
    }

    /*
     * Obtiene el evaluador leyendo el id del usuario correspondiente con este
     * desde Session
     */

    protected function _obtenerEvaluador() {
        //Obtiene el id del usuario logueado
        $idUser = $this->Auth->user('id');

        //Obtiene el evaluador asociado al usuario
        $evaluador = $this->Evaluator->find('first', array(
            'conditions' => array('user_id' => $idUser)));

        return $evaluador;
    }

    /*
     * Dado un ID de un problema válido, conforma su titulo siguiendo
     * la estructura:
     * Caso <ordenRelativo> : titulo
     */

    protected function _armarTituloProblema($problemID) {
//Carga el modelo necesario  
        $this->loadModel('Problem');

        if (!$this->Problem->exists($problemID)) {
            $this->Session->setFlash(__('El problema especificado es inválido.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'assignedproblems'));
        }

//Obtiene el titulo del problema para mostrarlo en la vista
//Para eso busca el problema
        $problemaRelacionado = $this->Problem->find('first', array(
            'conditions' => array('id' => $problemID),
            'recursive' => -1
        ));

        $problema = $problemaRelacionado['Problem'];

        $ordenProblema = $problema['relative_order'];
        $tituloProblema = $problema['title'];

//Conforma el nombre del problema para que se muestre en la vista
        $tituloProblemaAsociado = 'Caso N°' . $ordenProblema . ': ' . $tituloProblema;

        return $tituloProblemaAsociado;
    }

    /*
     * Determina si un problema ha sido corregido completamente o no.
     * Notar que un problema será corregido para cada una de las preguntas asociadas
     * a éste, se cumple que estén corregidas.
     * Para esto debe internamente iterar sobre cada una de las preguntas.
     */

    protected function _problemaCorregido($problemID) {

        //Asume que el id enviado es válido, ya que es un método privado, invocado
        //únicamente desde aquí dentro
        //Cargado de modelos necesarios
        $this->loadModel('Question');

        //El ID del problema existe, obtiene las preguntas asociadas al mismo
        //Verifica que no sea null porque aquellas que tienen null son las últimas del
        //problema y no deben ser corregidas
        $preguntasAsociadasaProblema = $this->Question->find('all', array(
            'conditions' => array('problem_id' => $problemID, "not" => array("questions" => null)),
            'recursive' => -1
        ));


        foreach ($preguntasAsociadasaProblema as $pregunta) {

            //Obtengo el id y el orden de la pregunta
            $questionID = $pregunta['Question']['id'];

            //Invoco al método para saber si ha sido corregida
            if (($this->_preguntaCorregida($questionID, $problemID)) === false) {
                return false;
            }
        }

        //Todas las preguntas de este problema han sido corregidas para todos los alumnos
        //que fueron a rendir.
        return true;
    }

    /*
     * Determina si una pregunta de un determinado problema identificados mediante sus IDs 
     * ha sido corregida para todos los alumnos que comenzaron el examen.
     */

    protected function _preguntaCorregida($questionID, $problemID) {

        //Carga el modelo necesario  
        $this->loadModel('Student');

        //El id enviado existe
        /* Obtiene todos los estudiantes que tengan en 1 la variable init, es decir que hayan
         * comenzando el examen y además terminado.
         */
        $alumnosACorregir = $this->Student->find('all', array(
            'conditions' => array('init' => '1', 'finish' => '1'),
            'recursive' => -1
        ));


        if (empty($alumnosACorregir[0])) {//Si no existen alumnos para corregir la pregunta no está corregida
            return false;
        } else {

            foreach ($alumnosACorregir as $alumno) {

                //Para cada alumno existente debe darse que la pregunta del problema esté
                //corregida, en caso de que para al menos uno no lo esté entonces fallará
                //Obtengo los datos necesarios del alumno
                $studentID = $alumno['Student']['student'];


                //Me fijo si el alumno terminó de rendir o no
                $finalizado = $alumno['Student']['finish'];

                //Dependiendo del valor de finish sabe si terminó de rendir o no
                if ($finalizado == true) {
                    //Me fijo si la pregunta está corregida para el alumno
                    $corregida = $this->_preguntaCorregidaParaAlumno($problemID, $questionID, $studentID);

                    if ($corregida == false) {
                        //Hay al menos uno que no tiene la pregunta corregida
                        return false;
                    }
                } else {
                    //Hay al menos un alumno que no terminó de rendir aún, la pregunta no ha sido corregida
                    return false;
                }
            }

            //Llegado este punto, la pregunta fue corregida para todos
            return true;
        }
    }

    /*
     * Dado un problema, una pregunta y un alumno, devuelve true si la misma ha sido corregida
     * en su totalidad para éste o false caso contrario.
     */

    protected function _preguntaCorregidaParaAlumno($problemID, $questionID, $studentID) {


        //Obtiene, empleando un método auxiliar el conjunto de secciones e items asociados a 
        //el problema y la pregunta, junto con un valor en cada uno que indica si fue o no
        //corregido. Simplemente mirando que para el alumno en cuestión todos los valores sean
        //1 o 0 ya sé que han sido corregidos
        //Obtiene un arreglo con información de las secciones y los items ya organizada
        $seccionesItems = $this->_seccionesItemsAsociados($problemID, $questionID, $studentID);
        
        foreach ($seccionesItems as $seccion) {

            //Obtiene los items asociados
            $itemsSeccion = $seccion['items'];

            foreach ($itemsSeccion as $item) {
                //Si hay al menos un item que en valor de selección tiene -1, ya está, no está corregida
                $valorSeleccion = $item['seleccionar'];

                if ($valorSeleccion == -1) {
                    //Este alumno no tiene la pregunta corregida
                    return false;
                }
            }
        }

        //Llegado este punto, el alumno ya tiene corregida la pregunta
        return true;
    }

    /*
     * Determina, dado un problema y una pregunta (IDS) el conjunto de secciones
     * e items asociados a los mismos.
     * Para ver el formato de salida hacer un debug.
     * Notar que como este método se emplea desde el resto de las acciones y siempre le enviamos
     * un question_id, entonces nunca va a devolver los items de defensa,porque estos tienen NULL
     * como question_id y yo siempre le envio un id válido de pregunta al igual que el resto.
     */

    protected function _seccionesItemsAsociados($problemID, $questionID, $studentID) {

        //Cargado de modelos necesarios
        $this->loadModel('Question');
        $this->loadModel('Student');
        $this->loadModel('CollationTable');
        $this->loadModel('Section');
        $this->loadModel('SectionCollation');
        $this->loadModel('Item');
        $this->loadModel('Correction');


//NO chequeo que sean ids existentes porque estoy invocando desde un método interno
//el cual nunca va a obtener un id que no sea válido y este método es privado, no
//puede accederse desde afuera

        /*
         * Obtiene, dado el id del problema, la tabla de cotejo completa para obtener la información
         * que luego necesite
         */
        $tablaCotejo = $this->CollationTable->find('first', array(
            'conditions' => array('problem_id' => $problemID)
        ));

                
//Emplea una variable para contar secciones
        $contadorSeccion = 0;
        $contadorItems = 0;

        $salida = array(array());
        $itemsAsociados = array(array());

        if (empty($tablaCotejo['CollationTable']) || (empty($tablaCotejo['SectionCollation'][0]))) {
            //Si no existe una tabla de cotejo asociada a ese problema
            //O no existen secciones asociadas a la tabla de cotejo) 
            //Devolver que ha sucedido un error
        } else {
            //La lista de cotejo tiene elementos asociados y además hay secciones
            //De la tabla de cotejo, obtengo las secciones que tiene la misma
            $seccionesColaciones = $tablaCotejo['SectionCollation'];

            //Itera para cada una de las secciones encontrando los items asociados a éstas
            foreach ($seccionesColaciones as $seccionColacion) {

                //Obtiene el id de la sección actual y el nombre
                $idSeccionColacion = $seccionColacion['id'];
                
                //Para la obtención del nombre se necesita una redirección adicional, tengo que buscar
                //dado el section_id en Section a ver el nombre
                $idSeccion = $seccionColacion['section_id'];
                
                $seccionAsociada = $this->Section->find('first', array(
                            'conditions' => array('id' => $idSeccion), 'recursive' => -1));
                
                //Obtiene de la sección asociada el orden así como el nombre de la sección
                $tituloSeccion = $seccionAsociada['Section']['name'];
                $ordenRelativoSeccion = $seccionAsociada['Section']['relative_order'];
                
                
                //Dada una sección, obtiene los items de la misma definidos para la pregunta
                //que fue seleccionada
                $items = $this->Item->find('all', array(
                    'conditions' => array('section_collation_id' => $idSeccionColacion, 'question_id' => $questionID),
                    'recursive' => -1
                ));


                if (!empty($items[0])) { //Si la sección tiene items para esta pregunta
                    //Itera para cada uno de los items fijandose que ya haya una entrada
                    //en la tabla de correcciones
                    //Guarda el titulo de la sección
                    $salida[$contadorSeccion]['tituloSeccion'] = $tituloSeccion;
                    $salida[$contadorSeccion]['ordenRelativoSeccion'] = $ordenRelativoSeccion;

                    foreach ($items as $item) {

                        //Obtengo el id del item y su descripción
                        $idItem = $item['Item']['id'];
                        $descripciónItem = $item['Item']['description'];

                        //Obtiene la corrección, si es que existe para en una variable más indicar qué check
                        //habrá que poner seleccionado (quizás ninguno si no existe).
                        $correccionAsociada = $this->Correction->find('first', array(
                            'conditions' => array('item_id' => $idItem, 'student_id' => $studentID), 'recursive' => -1));

                        //Debe fijarse si es vacia o no, ya que en caso de ser vacia habrá que crearla y en caso contrario no
                        if (empty($correccionAsociada)) {
                            //No hay ninguna corrección, no debe estar seleccionado ni el Sí ni el No
                            //Lo represento con un -1
                            $valorSeleccionar = -1;
                        } else {
                            //Existe una corrección, debo fijarme el yes_no_answer
                            $yes_no_answer = $correccionAsociada['Correction']['yes_no_answer'];
                            //Será 0 para que haya que ponerle el check al No y 1 para el Sí
                            if ($yes_no_answer) {
                                //Es verdadero, le pongo un 1
                                $valorSeleccionar = 1;
                            } else {
                                //Es falso, le pongo un 0
                                $valorSeleccionar = 0;
                            }
                        }

                        //Guarda los datos para la salida
                        $itemsAsociados[$contadorItems]['id'] = $idItem;
                        $itemsAsociados[$contadorItems]['descripcion'] = $descripciónItem;
                        $itemsAsociados[$contadorItems]['seleccionar'] = $valorSeleccionar;

                        //Incrementa el contador de items
                        $contadorItems++;
                    }

                    //Guarda los items para la sección
                    $salida[$contadorSeccion]['items'] = $itemsAsociados;

                    //Incrementa el contador de la sección
                    $contadorSeccion++;

                    //Reinicia el contador para los items
                    $contadorItems = 0;

                    //Limpia el arreglo de items asociados para evitar conflictos
                    $itemsAsociados = array(array());
                } else {
                    //No hay items definidos para la pregunta
                    //Habra un error
                }
            }


            //Devuelve la variable de salida
            return $salida;
        }
    }

    /*
     * Dado un id de un estudiante obtiene sus vecinos, entiéndase por esto y de existir, quién es
     * el próximo y quién el anterior.
     */

    protected function _obtenerVecinos($studentID) {
        $neighbors = $this->Student->find('neighbors', array('field' => 'student',
            'value' => $studentID,
            'order' => 'pseudonim DESC',
            'recursive' => '-1',
            'conditions' => array('init' => '1', 'finish' => '1')));
        
        //Lo voy a hacer a mano para tener homogeneidad con el ordenar
        //Recupero todos los alumnos que efectivamente rindieron
        $alumnos = $this->Student->find('all', array(
            'conditions' => array('init' => '1', 'finish' => '1'),
            'recursive' => -1
        ));
        
        //Proceso el arreglo para que me lo deje con el nivel necesario como para ordenarlo
        $alumnosEfectivos = $this->_procesarAlumnos($alumnos);
        
        
        //Los ordeno por pseudónimo alfabéticamente
        if (!(empty($alumnosEfectivos[0]))) {
            $alumnosOrdenado = ordenar($alumnosEfectivos, array('pseudonimo'), SORT_ASC);
        }
                
        //Obtengo los vecinos manualmente. La idea es simple. Si el id que tengo que buscar es el primero
        //entonces solo tiene próximo. Si es el último, sólo tiene anterior. Caso contrario tiene siguiente
        //y anterior. Esto siempre que no sea el único
        $contador=0;
        $cantidad=count($alumnosOrdenado)-1;
        
                
        if ($cantidad==0) {
            $salida['prev'] = array();
            $salida['next'] = array();
        }
        
        else {
        //Seguro que hay al menos dos alumnos            
        foreach($alumnosOrdenado as $alumno) {
            
            //Obtengo el id 
            $idAlumno = $alumno['student'];
            
            //Comparo con el que busco
            if ($idAlumno == $studentID) {
                //Encontré el adecuado, me tengo que fijar si es el último o el primero
                if (($contador==0) && ($contador!=$cantidad)) {
                    //Es el primero y hay más de uno, devuelve el siguiente.
                    $salida['next']['student'] = $alumnosOrdenado[$contador+1]['student'];
                    $salida['prev'] = array();
                    return $salida;
                }
                
                else if (($contador==$cantidad) && ($contador!=0)) {
                    //Es el último y hay más de uno, devuelve el anterior
                    $salida['prev']['student'] = $alumnosOrdenado[$contador-1]['student'];
                    $salida['next'] = array();
                    return $salida;
                }
                
                else {
                    //Es uno del medio, tiene siguiente y anterior
                    $salida['next']['student'] = $alumnosOrdenado[$contador+1]['student'];
                    $salida['prev']['student'] = $alumnosOrdenado[$contador-1]['student'];
                    return $salida;
                }   
            }
            
            //Incremento el contador
            $contador++;  
        }
    }
    }
    
    
    
    /*
     * Como la función ordenar no está implementada para un nivel genérico de indirecciones, entonces
     * le proceso el arreglo obtenido para que la pueda ordenar
     */
    protected function _procesarAlumnos($alumnos) {
        
        $contador=0;
        $salida=array(array());
        
        foreach ($alumnos as $alumno) {
            
            $salida[$contador]['student'] = $alumno['Student']['student'];
            $salida[$contador]['pseudonimo'] = $alumno['Student']['pseudonim'];
            
            $contador++;
            
        }
        
       return $salida; 
        
    }
    

    /*
     * Redefine el método isAuthorized para este controlador. 
     * Se verifica el rol del usuario que esta logueado y dependiendo del mismo se establecen
     * los permisos correspondientes.
     */
    
    public function isAuthorized($user) {
        
        $esAdmin = false;
        $esStudent = false;
        $esEvaluator = false;
        if (AuthComponent::user()) {

            $nombreRol = $this->Session->read('Auth.User.role');

            if (strcmp("Administrador", $nombreRol) === 0) {
                //Es administrador
                $esAdmin = true;
            } else if (strcmp("Evaluador", $nombreRol) === 0) {
                //Es evaluador
                $esEvaluator = true;
            } else if (strcmp("Alumno", $nombreRol) === 0) {
                //Es alumno
                $esStudent = true;
            }
        }
        
        if ($esAdmin && (!$esEvaluator) && (!$esStudent)) {
            $this->Session->setFlash(__($this->Auth->authError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect('/users/administrador');
        } 
        else if ((!$esAdmin) && $esEvaluator && (!$esStudent)) {
            return true;
        } 
        else if ((!$esAdmin) && (!$esEvaluator) && $esStudent) {
            $this->Session->setFlash(__($this->Auth->authError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect('/StudentsAnswers/index');
        }
        //Llegado este punto, el isAuthorized por la configuración de AppController devuelve false
        return parent::isAuthorized($user);
    }
}
