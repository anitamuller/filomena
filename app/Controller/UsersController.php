<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }

       
    /*
     * Crea un administrador en el sistema
     */

    public function addadmin() {
        if ($this->request->is('post')) {
            $this->User->create();
            //Establece el rol internamente
            $this->request->data['User']['role'] = 'Administrador';
            if ($this->User->saveAll($this->request->data)) {
                $this->Session->setFlash(__('The Administrator has been saved'), 'default', array('class' => 'alert alert-dismissable alert-success'));

                //Para saber a donde debo redireccionar, debo necesariamente saber el botón que se
                //toco
                if (isset($this->request->data['guardarNuevo'])) {
                    return $this->redirect(array('action' => 'addadmin'));
                } else if (isset($this->request->data['guardar'])) {
                    return $this->redirect(array('action' => 'index'));
                }
            }
            $this->Session->setFlash(
                    __('The administrator could not be saved. Please, try again.')
            );
        }
    }

    /*
     * Crea un evaluador en el sistema
     */

    public function addevaluator() {
        if ($this->request->is('post')) {
            $this->User->create();
            //Establece el rol internamente
            $this->request->data['User']['role'] = 'Evaluador';
            if ($this->User->saveAll($this->request->data)) {
                $this->Session->setFlash(__('The Evaluator has been saved'), 'default', array('class' => 'alert alert-dismissable alert-success'));

                //Para saber a donde debo redireccionar, debo necesariamente saber el botón que se
                //toco
                if (isset($this->request->data['guardarNuevo'])) {
                    return $this->redirect(array('action' => 'addevaluator'));
                } else if (isset($this->request->data['guardar'])) {
                    return $this->redirect(array('action' => 'index'));
                }
            }
            $this->Session->setFlash(
                    __('The evaluator could not be saved. Please, try again.')
            );
        }
    }

    /*
     * Crea un alumno en el sistema. Al momento de la creación
     * genera el pseudónimo por debajo para que se lo almacene en la base de datos.
     * Notar que la forma en que se genera el pseudónimo es obteniendo el último id de
     * la tabla student empleado y sumándole 1. Así se obtiene un número que se
     * concatena a la palabra Alumno.
     */

    public function addstudent() {
        if ($this->request->is('post')) {
            
            //Me fijo que realmente haya un pseudónimo para asignarle al usuario, si no no lo creo
            $pseudonimo = $this->_obtenerPseudonimoTabla();
                
                if ($pseudonimo==false) {
                    //Ocurrió un error, probablemente no hay más pseudónimos para asignar o bien un error durante
                    //la actualización del pseudónimo
                    $this->Session->setFlash(__('No hay más pseudónimos para asignar, agregue nuevos.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                    
                    if (isset($this->request->data['guardarNuevo'])) {
                    return $this->redirect(array('action' => 'addstudent'));
                    } else if (isset($this->request->data['guardar'])) {
                    return $this->redirect(array('action' => 'index'));
                    }  
                }           
            
            //En este caso hay un pseudónimo disponible, entonces lo uso            
            $this->User->create();
            //Establece el rol internamente
            $this->request->data['User']['role'] = 'Alumno';
            if ($this->User->saveAll($this->request->data)) {
                //Para saber a donde debo redireccionar, debo necesariamente saber el botón que se
                //toco      
                //Actualiza el pseudónimo del alumno
                $actualizoBienPseudonimo = $this->User->Student->saveField('pseudonim', $pseudonimo);
                
                if (!$actualizoBienPseudonimo) {
                    $this->Session->setFlash(__('Error al actualizar el pseudónimo del alumno. Inténtelo nuevamente.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                    
                    if (isset($this->request->data['guardarNuevo'])) {
                    return $this->redirect(array('action' => 'addstudent'));
                    } else if (isset($this->request->data['guardar'])) {
                    return $this->redirect(array('action' => 'index'));
                    } 
                }  
                
                else {
                    $this->Session->setFlash(__('El alumno se ha creado con éxito.'), 'default', array('class' => 'alert alert-dismissable alert-success'));
                    //No hubo ningún error
                    if (isset($this->request->data['guardarNuevo'])) {
                        return $this->redirect(array('action' => 'addstudent'));
                    } else if (isset($this->request->data['guardar'])) {
                        return $this->redirect(array('action' => 'index'));
                    }
                }
            }
            
            $this->Session->setFlash(
                    __('The user could not be saved. Please, try again.')
            );
        }
    }

    /*
     * Genera un string aleatoriamente de longitud especificada
     */

    protected function _generarPseudonimoRandom($longitud) {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $longitud);
    }
    
    
    /*
     * Empleando la tabla de pseudónimos, busca, en caso de haber, el pseudónimo que le corresponde al alumno
     * y lo establece.
     * Devuelve falso si no hay más pseudónimos.
     */
    protected function _obtenerPseudonimoTabla() {
        
        //Cargado de modelos auxiliares
        $this->loadModel('Pseudonim');
        
        //Busco de la tabla el primero que tenga el campo 'used' en 0.
        $pseudonimoTabla = $this->Pseudonim->find('first', array(
            'conditions' => array('used' => 0),
            'recursive' => -1
        ));
        
        //Podría no haber ninguno si es que no hay más lugar
        if(empty($pseudonimoTabla['Pseudonim'])) {
            return false;
        }        
        
        else {
            //Recupero el pseudónimo en cuestión
            $pseudonimo = $pseudonimoTabla['Pseudonim']['pseudonim'];
            
            //Actualizo el campo 'used' del mismo para reflejar que alguien lo está usando
            //Hay que editarla mediante un saveField
            //Para esto hay que obtener el id de la misma
            $pseudonimoID = $pseudonimoTabla['Pseudonim']['id'];

            //Actualizo manualmente el campo
            $this->Pseudonim->id = $pseudonimoID;
            $actualizoBienUsed = $this->Pseudonim->saveField('used', 1);

            if (!$actualizoBienUsed) {
                //Ocurrió un error
                return false;
            }          
            
            //Llegado este punto no hay errores, devuelvo el pseudónimo
            return $pseudonimo;
            
        }
    }
    
    

    /*
     * Genera un pseudónimo contemplando el id de inserción del alumno y la palabra básica
     * 'Alumno'
     */

    protected function _generarPseudonimoUltimoID() {
        $idCorrespondiente = $this->User->Student->getLastInsertID();
        $pseudonimo = 'Alumno' . $idCorrespondiente;
        return $pseudonimo;
    }

    /*
     * Función para editar un administrador antes creado en el sistema.
     */

    public function editadministrator($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The administrator has been saved.'), 'default', array('class' => 'alert alert-dismissable alert-success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The administrator could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    /*
     * Función para editar un alumno antes creado en el sistema.
     * Notar que para editar el estudiante en la vista estoy colocando tanto el id del user
     * como el id que tiene el estudiante asociado al mismo para que fácilmente lo pueda
     * encontrar en la bd.
     */

    public function editstudent($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid student'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //Guardo lo que me viene en el formulario en el modelo del usuario y además en el modelo del estudiante
            //asociado al usuario en cuestion. Si ni pude guardar alguno de los dos muestro un error.
            if (($this->User->save($this->request->data['User'])) && ($this->User->Student->save($this->request->data['Student']))) {
                $this->Session->setFlash(__('The student has been updated.'), 'default', array('class' => 'alert alert-dismissable alert-success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The student could not be updated. Please, try again.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    /*
     * Función para editar un evaluador antes creado en el sistema.
     * Notar que para editar el evaluador en la vista estoy colocando tanto el id del user
     * como el id que tiene el evaluador asociado al mismo para que fácilmente lo pueda
     * encontrar en la bd.
     */

    public function editevaluator($id = null) {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid student'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //Guardo lo que me viene en el formulario en el modelo del usuario y además en el modelo del evaluador
            //asociado al usuario en cuestion. Si no pude guardar alguno de los dos muestro un error.
            if (($this->User->save($this->request->data['User'])) && ($this->User->Evaluator->save($this->request->data['Evaluator']))) {
                $this->Session->setFlash(__('The evaluator has been updated.'), 'default', array('class' => 'alert alert-dismissable alert-success'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The evaluator could not be updated. Please, try again.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            }
        } else {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }
    }

    /*
     * Función empleada para eliminar un usuario.
     * Por ahora elimina ciegamente al usuario, conforme avanzado el sistema habrá que ver
     * cuándo es posible eliminar un alumno, evaluador o administrador.
     */

    public function delete($id = null) {
        //Cargado de modelos auxiliares
        $this->loadModel('Problem');

        //Obtiene el id del usuario
        $this->User->id = $id;

        //Si no existe arroja una excepción
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->request->allowMethod('post', 'delete');

        //Encuentra el rol asociado al usuario que se desea eliminar
        $usuarioAsociado = $this->User->find('first', array(
            'conditions' => array('id' => $id)));

        $rol = $usuarioAsociado['User']['role'];

        if ($rol == 'Evaluador') {
            //Obtiene el evaluador asociado
            $evaluatorID = $usuarioAsociado['Evaluator']['evaluator'];

            //Un evaluador podrá borrarse siempre que no tenga un problema asociado al mismo
            $problemaAsociado = $this->Problem->find('first', array(
                'conditions' => array('evaluator_id' => $evaluatorID),
                'recursive' => -1));


            if (empty($problemaAsociado['Problem'])) {
                //Lo borra
                $salidaExito = 'El evaluador ha sido eliminado con éxito.';
                if ($this->User->delete()) {
                    $this->Session->setFlash(__($salidaExito), 'default', array('class' => 'alert alert-dismissable alert-success'));
                }
            } else {
                //El evaluador no puede eliminarse porque tiene problemas asociados
                $salidaError = 'El evaluador no ha podido eliminarse porque tiene al menos un problema asignado.';
                $this->Session->setFlash(__($salidaError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            }
        } else if ($rol == 'Administrador') {
            $salidaExito = 'El administrador ha sido eliminado con éxito.';
            $salidaError = 'El administrador no ha podido eliminarse. Inténtelo nuevamente.';
        } else if ($rol == 'Alumno') {
            $salidaExito = 'El alumno ha sido eliminado con éxito.';
            $salidaError = 'El alumno no ha podido eliminarse. Inténtelo nuevamente.';
        }

        /* if ($this->User->delete()) {
          $this->Session->setFlash(__($salidaExito), 'default', array('class' => 'alert alert-dismissable alert-success'));
          }
          else {
          $this->Session->setFlash(__($salidaError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
          } */

        return $this->redirect(array('action' => 'index'));
    }

    /*
     * Esta función está únicamente para tener una vista asociada. Es decir
     * por una formalidad de cómo es el patrón MVC.
     */

    public function administrador() {
        //Tiene una vista asociada que representa el menú inicial del administrador
        $this->loadModel('Exam');
        //Busco el primer examen que haya, que en realidad va a ser el único (Filomena) y redirijo al
        //examstate del mismo.
        $primerExamen = $this->Exam->find('first', array('recursive' => -1));
        
        //Obtengo el id y redirijo
        $idFilomena = $primerExamen['Exam']['id'];
        $this->redirect(array('controller' => 'ManagerAdmin','action' => 'examstate',$idFilomena));
        
    }
    

    /*
     * Función empleada para la autenticación de usuarios al sistema.
     */

    public function login() {
        //Quito siempre lo que haya en Auth redirect para que no moleste y me redirija a donde quiera por
        //intentos de accesos no válidos.
        $this->Session->delete('Auth.redirect');
        
	//Si el usuario ya está logueado, redireccionar
        if ($this->Session->check('Auth.User')) {
            //El usuario se logueo exitosamente
            $esAdmin = false;
            $esStudent = false;
            $esEvaluator = false;
	    
            //Obtiene el rol asociado al usuario          
            $nombreRol = $this->Auth->user('role');

            if (strcmp("Administrador", $nombreRol) === 0) {
                //Es administrador
                $esAdmin = true;
            } else if (strcmp("Evaluador", $nombreRol) === 0) {
                //Es evaluador
                $esEvaluator = true;
            } else if (strcmp("Alumno", $nombreRol) === 0) {
                //Es alumno
                $esStudent = true;
            }


            if ($esAdmin && (!$esEvaluator) && (!$esStudent)) {
                //El usuario es solo administrador
                $this->redirect(array('action'=>'administrador'));
            } else if ((!$esAdmin) && $esEvaluator && (!$esStudent)) {
                //El usuario es solo evaluador
                $this->redirect(array('controller' => 'managerEvaluator', 'action' => 'assignedproblems'));
            } else if ((!$esAdmin) && (!$esEvaluator) && $esStudent) {
                
                $this->loadModel('Student');
                
                //El usuario es solo estudiante
                //Debo pasarle al controller que haga todo el manejo asociado, el user_id
                //del estudiante logueado
                $id = $this->Auth->user('id');
                                
                $alumno = $this->Student->find('first', array(
                    'conditions' => array('user_id' => $id), 'recursive' => -1));
                
                //Obtiene el ip almacenado y el ip del cliente
                $idAlumno = $alumno['Student']['student'];
                $ipAlmacenado = $alumno['Student']['ip'];
                $ipCliente = $this->request->clientIp();
                              
                if($ipAlmacenado==null) {
                    //Significa que es la primera vez que ingresa o luego de haberse deslogueado
                    //Tengo que capturar el ip del cliente y guardarlo
                    
                    //Actualizo el ip de ese alumno
                    //Actualizo manualmente el campo
                    $this->Student->id = $idAlumno;
                    $actualizoBienIP = $this->Student->saveField('ip', $ipCliente);

                    if (!$actualizoBienIP) {
                        //Ocurrió un error
                        $this->Session->setFlash(__('Error al actualizar la base de datos. Inténtelo nuevamente'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
			$this->redirect(array('action' => 'logout'));
                    }
                    
                    else {
                    //No hubo errores al actualizar
                    //Lo redirijo a donde corresponda
                     $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'index'));
                    }
                }
                
                else {
                    
                    //Tengo que ver el valor del ip y compararlo con el mio
                    //Si coinciden, no hay problema. Caso contrario no le permito ingresar.
					
                    $ipAlmacenado2="".$ipAlmacenado."";
                    $ipCliente2 = "".$ipCliente."";
 
                    $sonIguales = strcmp($ipAlmacenado2,$ipCliente2) === 0;
                    if ($sonIguales) {
                        //No hay ningún problema
			//Este caso en teoria jamás va a suceder por cómo es cake
                        $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'index'));
                    }
                    
                    else {
                        //Me quiere entrar desde otro lado, no lo permito
			//Llamo a un logout especial el cual NO actualiza la base de datos, solo redirige
                        $this->Session->setFlash(__('Ya existe una cuenta logueada desde otro IP para este usuario. Desloguee dicha cuenta.'), 'errorLogueo');
			$this->redirect(array('action' => 'logoutintruso'));
                    }  
                }  
            }
        }

        // Si se tiene información del post, intenta autenticar
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                //Redirige al mismo metod login
                $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Session->setFlash(__('Usuario o contraseña incorrecto/s.'), 'errorLogueo');
            }
        }      
    }
	
	
	/*
     * Función empleada para desloguear un usuario intruso del sistema
     */
	public function logoutintruso() {
		//En cualquier otro caso deslogueo sin más
        return $this->redirect($this->Auth->logout());
	}

    /*
     * Función empleada para desloguear un usuario logueado al sistema.
     */

    public function logout() {
        
        //Antes de desloguearme pongo en null el campo ip para el student en cuestión
        //Siempre que sea un student obviamente
        $nombreRol = $this->Session->read('Auth.User.role');
        if (strcmp("Alumno", $nombreRol) === 0) {
            
            $this->loadModel('Student');

            //Tengo un alumno logueado
            $idUsuario = $this->Auth->user('id');
            
            //Busco el alumno y actualizo
            $alumno = $this->Student->find('first', array(
                    'conditions' => array('user_id' => $idUsuario), 'recursive' => -1));
                
            //Obtiene el id del alumno
            $idAlumno = $alumno['Student']['student'];
            
            
            //Actualizo el ip de ese alumno
            //Actualizo manualmente el campo
            $this->Student->id = $idAlumno;
            $actualizoBienIP = $this->Student->saveField('ip', null);

            if (!$actualizoBienIP) {
                //Ocurrió un error
                $this->Session->setFlash(__('Error al actualizar la base de datos. Inténtelo nuevamente'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'index'));
             }
                    
            else {
            //No hubo errores al actualizar
            //Lo redirijo a donde corresponda y remuevo tanto la cookie como la información de Session para este 
            //usuario
                $this->Session->destroy();
                return $this->redirect($this->Auth->logout());
            } 
        }
        
        else {
            //En cualquier otro caso deslogueo sin más, recordando destruir session y cookie
            $this->Session->destroy();
            return $this->redirect($this->Auth->logout());
        }
    }

    /*
     * Redefine el método isAuthorized para este controlador. 
     * Se verifica el rol del usuario que esta logueado y dependiendo del mismo se establecen
     * los permisos correspondientes.
     */

    public function isAuthorized($user) {
        $esAdmin = false;
        $esStudent = false;
        $esEvaluator = false;
        if (AuthComponent::user()) {

            $nombreRol = $this->Session->read('Auth.User.role');

            if (strcmp("Administrador", $nombreRol) === 0) {
                //Es administrador
                $esAdmin = true;
            } else if (strcmp("Evaluador", $nombreRol) === 0) {
                //Es evaluador
                $esEvaluator = true;
            } else if (strcmp("Alumno", $nombreRol) === 0) {
                //Es alumno
                $esStudent = true;
            }
        }

        if ($esAdmin && (!$esEvaluator) && (!$esStudent)) {
            return true;
        } else if ((!$esAdmin) && $esEvaluator && (!$esStudent)) {
            $this->Session->setFlash(__($this->Auth->authError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect('/managerEvaluator/assignedproblems');
            
        } else if ((!$esAdmin) && (!$esEvaluator) && $esStudent) {
            $this->Session->setFlash(__($this->Auth->authError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect('/StudentsAnswers/index');
            
        }
        
        //Llegado este punto, el isAuthorized por la configuración de AppController devuelve false
        return parent::isAuthorized($user);
    }

}
