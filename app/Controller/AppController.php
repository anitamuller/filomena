<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Session',
        'DebugKit.Toolbar',
        'Auth' => array(
            'flash' => array(
                'element' => 'alert',
                'key' => 'auth',
                'params' => array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-error'
                )
            )
        )
    );
    public $helpers = array(
        'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form' => array('className' => 'BoostCake.BoostCakeForm'),
        'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
    );

    public function beforeFilter() {
        $this->inicializarAuth();
    }

    function inicializarAuth() {
        $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
        $this->Auth->loginError = array('controller' => 'users', 'action' => 'login');
        $this->Auth->authError = 'No tiene autorización para ver la página solicitada.';
        //Necesario para el isAuthorized.
        $this->Auth->authorize = array('Controller');

        //Para todos los usuarios permito acceder a login/logout y a display sin importar nada
        //Recordar que lo definido en el allow no se mira en el isAuthorized, directamente se
        //Respeta.
        $this->Auth->allow('login');
        $this->Auth->allow('logout');
        $this->Auth->allow('logoutintruso');
    }

    /*
     * Por default, deniego todo sin importarme el rol que tiene el usuario, permito nada más
     * que tenga autorización para lo definido en el allow.
     */

    public function isAuthorized($user) {
        //Default deny
        return false;
    }

}
