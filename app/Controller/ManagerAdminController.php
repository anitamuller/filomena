<?php

include '../webroot/utilitarios.php';
App::uses('AppController', 'Controller');

class ManagerAdminController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');
    //No buscan ningun modelo asociado con la linea que viene abajo
    var $uses = array();

        
    /*
     * Está únicamente para que podamos acceder al abm
     */
    public function abm() {
        
    }

    
    /*
     * Incluye la información necesaria para la descripción de un examen
     */
    public function examstate($id = null) {
        //Se muestran las acciones (comenzar, finalizar, defensa, exportar) relacionadas a un determinado examen
        $this->loadModel('Exam');
        
        //Debo verificar si el id pasado del examen realmente existe y además si la defensa puede o no comenzar
        //asi se lo manda a la vista para que establezca el botón como corresponda
        $texto = 'El examen especificado no existe.';
        $this->_validarIDExamen($id,$texto);
        
        $resultado = $this->_verificarInicioDefensa($id);
        
        //Me fijo si ya está defendido el examen o no, ya que en este caso el botón de exportación
        //de datos se debe habilitar, si no no
        //Busca el examen en la base de datos
        $examen = $this->Exam->find('first', array('conditions' => array('id' => $id), 'recursive' => -1));
        
        $defendido = $examen['Exam']['defend'];      
        
        //Envia información del examen solicitado a la vista                    
        $this->set('exam', $examen);
        $this->set('puedeDefender',$resultado);
        $this->set('defendido',$defendido);
           
    }

    
    
    /*
     * Funcionalidad asociada a dar inicio a un examen del cual se brinda el id
     */
    public function initexam($id = null) {
        
        
        //Se dará por comenzado un examen, cuando éste se encuentre publicado.
        //El comienzo de un examen, habilita a que los alumnos puedan empezar a desarrollar el examen.
        $this->loadModel('Exam');
        
        //Debo verificar si el id pasado del examen realmente existe
        $texto = 'El examen que desea iniciar no existe.';
        $this->_validarIDExamen($id,$texto);
        
        
        //Recupero el examen y me fijo si ya está iniciado
        $examen = $this->Exam->find('first', array('fields' => 'published',
            'conditions' => array('id' => $id), 'recursive' => -1
        ));
        
        $yaPublicado = $examen['Exam']['published'];
        
        if ($yaPublicado) {
            //Muestro un error y redirijo
            $this->Session->setFlash(__('El examen ya está publicado, no es posible volver a publicarlo.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'examstate',$id)); 
        }
        
        else {
        
        //El examen que se quiere iniciar existe y no está publicado
        //Actualizo manualmente el campo published
        $this->Exam->id = $id;
        $actualizoBienCompleted = $this->Exam->saveField('published', 1);

        if (!$actualizoBienCompleted) {
            //Ocurrió un error
            $this->Session->setFlash(__('Ha ocurrido un error al guardar la publicación del examen. Inténtelo nuevamente'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'examstate',$id));
        }
        else {
            //No hay problemas, muestra un mensaje
            $this->Session->setFlash(__('El examen se ha publicado exitosamente.'), 'default', array('class' => 'alert alert-dismissable alert-success'));
            return $this->redirect(array('action' => 'examstate', $id));
        }  
        }
    }
    
    
    /*
     * Funcionalidad asociada a la finalización de un examen
     * El examen debe existir, y además, no se puede finalizar un examen que no fue publicado.
     */
    public function endexam($id = null) {
        //La finalizacion de un examen, deshabilita la posibilidad que los alumnos continuen con el desarrollo del examen.
        $this->loadModel('Exam');
        $this->loadModel('Student');

        //Debo verificar si el id pasado del examen realmente existe
        $texto = 'El examen que desea finalizar no existe.';
        $this->_validarIDExamen($id,$texto);
        
        //Se trata de un id de examen válido

        //Lo primero es fijarme si el examen realmente ha sido publicado o si ya está finalizado. 
        //No tiene sentido finalizar un examen que no fue publicado aún así como permitir que se publique más de
        //una vez.
        $examenAsociado = $this->Exam->find('first', array(
        'conditions' => array('id' => $id), 'recursive'=> -1));

        $finalizado = $examenAsociado['Exam']['finish'];
        
                
        if ($finalizado) {
            //Muestro un error y redirijo
            $this->Session->setFlash(__('El examen ya ha finalizado, no es posible volver a finalizarlo.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'examstate',$id)); 
        }
        
        else {
        
        $publicado = $examenAsociado['Exam']['published'];

        if ($publicado==0) {
            //No puedo finalizar un examen no publicado
            $this->Session->setFlash(__('No es posible finalizar un examen que nunca comenzó.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'examstate',$id));

        }


        //Obtengo los estudiantes sin terminar. No importa si empezaron o no porque hay que ponerles el finish
        //de cualquier manera
        $estudiantesSinTerminar = $this->Student->find('all', array('fields' => 'student',
            'conditions' => array('finish' => 0), 'recursive' => -1
        ));

        foreach ($estudiantesSinTerminar as $estudiante) {
            //Obtiene el id del alumno
            $idEstudiante = $estudiante['Student']['student'];

            //Actualiza el campo finish de ese alumno
            $this->Student->id = $idEstudiante;
            $actualizoBienAlumno = $this->Student->saveField('finish', 1);

            if (!$actualizoBienAlumno) {
                //Ocurrió un error
                $this->Session->setFlash(__('Ha ocurrido un error al finalizar el examen. Inténtelo nuevamente'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                return $this->redirect(array('action' => 'examstate',$id));
            }   
        }

        //Pone en 1 la variable finish del examen en cuestión
        $this->Exam->id = $id;
        $actualizoBienExam = $this->Exam->saveField('finish', 1);

        if (!$actualizoBienExam) {
            //Ocurrió un error
            $this->Session->setFlash(__('Ha ocurrido un error al finalizar el examen. Inténtelo nuevamente'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'examstate',$id));
        } 

        //Llegado este punto, todos los alumnos fueron actualizados exitosamente y el finish de exam también
        $this->Session->setFlash(__('El examen ha finalizado exitosamente. Los alumnos no podrán continuar con el desarrollo del mismo.'), 'default', array('class' => 'alert alert-dismissable alert-success'));
        return $this->redirect(array('action' => 'examstate', $id));
            
    }
    }

    

    /*
     * Se encarga de, recibido un examen, obtener los evaluadores junto con si terminaron o no de corregir
     * para luego mostrarlo en pantalla
     */
    public function corrections($id=null) {
                       
        $this->loadModel('Evaluator');
        $this->loadModel('Problem');

        //Debo verificar si el id pasado del examen realmente existe
        $texto = 'El examen del cual desea analizar el proceso de corrección no existe.';
        $this->_validarIDExamen($id,$texto);
             
        //Debo primero recuperar los problemas que hay para el examen
        $problemas = $this->Problem->find('all', array('conditions' => array('exam_id' => $id),
            'recursive' => -1));
        
        $evaluadores = array(array());
        $contador=0;
        
                
        foreach($problemas as $problema) {
            //Para cada problema me fijo quien lo corrige
            $idEvaluador = $problema['Problem']['evaluator_id'];
                        
            if (!$this->aparece($idEvaluador,$evaluadores)) {
                //Lo agrego como uno nuevo
                $evaluadores[$contador]['idEvaluador'] = $idEvaluador;
            }
            //Caso contrario ya aparece y solo incremento el contador
            $contador++;   
        }
             
        //Ya tengo una estructura con todos los evaluadores sin repetir que me interesan     
        $listaEvaluadores = array(array());
        $index = 0;

        //Para cada evaluador, analizo si ha termiando o no de corregir
        foreach ($evaluadores as $evaluador):

            //Obtiene el id del evaluador
            $idEvaluador = $evaluador['idEvaluador'];

            //Recupera al evaluador que tiene ese id
            $evaluadorAsignado = $this->Evaluator->find('first', array('fields' => array('finish', 'surnames', 'names', 'email'),
                'conditions' => array('evaluator' => $idEvaluador),
                'recursive' => -1
            ));
            
                        
            //Obtengo los problemas asociados a ese evaluador para el examen así se los paso a la vista
            $problemasAsociados = $this->Problem->find('all', array(
                'conditions' => array('evaluator_id' => $idEvaluador, 'exam_id'=>$id),
                'recursive' => -1
            ));
            
            //Para cada uno de esos, voy contatenando una variable
            $casosAsociados = "";
            //Cantidad de problemas asociados
            $contadorAux = 1;
            
            foreach($problemasAsociados as $problema) {
                $ordenRelativo = $problema['Problem']['relative_order'];
                
                if ($contadorAux===1) {
                    $casosAsociados = $ordenRelativo;
                }
                else {
                $casosAsociados = $casosAsociados . '/' . $ordenRelativo;
                }
                
                $contadorAux++;
            }
            
            //Obtengo el porcentaje de corrección de ese evaluador
            $porcentajeCorreccion = $this->_calcularPorcentajeCorreccion($idEvaluador,$id);
                                                     
            //Recupero los datos que necesito del evaluador
            $estado = $evaluadorAsignado['Evaluator']['finish'];
            $nombreEvaluador = $evaluadorAsignado['Evaluator']['names'];
            $apellidoEvaluador = $evaluadorAsignado['Evaluator']['surnames'];
            $emailEvaluador = $evaluadorAsignado['Evaluator']['email'];

            //Guarda en un arreglo la información de los evaluadores
            $listaEvaluadores[$index]['id'] = $idEvaluador;
            $listaEvaluadores[$index]['nombre'] = $nombreEvaluador;
            $listaEvaluadores[$index]['apellido'] = $apellidoEvaluador;
            $listaEvaluadores[$index]['email'] = $emailEvaluador;
            $listaEvaluadores[$index]['terminoCorreccion'] = $estado;
            $listaEvaluadores[$index]['casos'] = $casosAsociados;
            $listaEvaluadores[$index]['porcentajeCorreccion'] = $porcentajeCorreccion;
            
            $index++;
        endforeach;
        
        //Guarda información en la vista
        $this->set('idExam', $id);
        $this->set(compact('listaEvaluadores'));
        
        
    }
    
    
    
    /*
     * Dado el id de un evaluador, calcula el porcentaje de preguntas corregidas. 
     * La forma más precisa de estimar la corrección es la siguiente:
     * Obtener los casos asociados. Obtener las preguntas de cada uno de esos casos.
     * Calcular el total de preguntas que hay para corregir sumando los conjuntos obtenidos
     * en los casos anteriores.
     * Multiplicar por la cantidad de alumnos a corregir el total de preguntas obtenido.
     * Empleando el método desarrollado en ManagerEvaluatorController, ir determinando para cada
     * alumno si la pregunta fue o no corregida
     */
    protected function _calcularPorcentajeCorreccion ($idEvaluador,$idExam) {
        
        //Cargado de modelos necesarios
        $this->loadModel('Evaluator');
        $this->loadModel('Problem');
        $this->loadModel('Question');
        $this->loadModel('Student');
        
        //Obtengo los alumnos a corregir
        $alumnosACorregir = $this->Student->find('all', array(
        'conditions' => array('init' => '1', 'finish' => '1'),
        'recursive' => -1
        )); 
        
        $cantidadAlumnos = count($alumnosACorregir);
        
        //Busco todos los problemas que tengan asignado el evaluador que me pasaron.
        //No chequeo nada de este, porque es un método privado que no puede invocarse desde afuera
        $problemasAsignados = $this->Problem->find('all', array(
                'conditions' => array('evaluator_id' => $idEvaluador, 'exam_id'=>$idExam),
                'recursive' => -1
        ));
        
        //Variable inicializada en 0       
        $totalPreguntas = 0;
        $preguntasCorregidas = 0;
                
        foreach ($problemasAsignados as $problema) {
            //Recupero las preguntas asociadas a este problema
            $idProblema = $problema['Problem']['id'];
            
            //Verifica que no sea null porque aquellas que tienen null son las últimas del
            //problema y no deben ser corregidas
            $preguntasAsociadasaProblema = $this->Question->find('all', array(
                'conditions' => array('problem_id' => $idProblema, "not" => array("questions" => null)),
                'recursive' => -1
            ));
            
            
            //Itero para cada una de las preguntas 
            foreach ($preguntasAsociadasaProblema as $pregunta) {
                
                //Recupero el id de la pregunta               
                $idPregunta = $pregunta['Question']['id'];
                                
                foreach($alumnosACorregir as $alumno) {
                    //Itero para cada uno de los alumnos y actualizo un contador
                    //Recupero el id
                    $idStudent = $alumno['Student']['student'];
                    
                    if ($this->_preguntaCorregidaParaAlumno($idProblema, $idPregunta, $idStudent)) {
                        $preguntasCorregidas++;
                    }    
                }   
            }
            
            
            //Actualizo el total de preguntas
            $totalPreguntas += count($preguntasAsociadasaProblema);
        }
        
        
        //Realizo una división y redondeo hacia arriba
        $totalEfectivoPreguntas = $totalPreguntas*$cantidadAlumnos;
        
        if ($totalEfectivoPreguntas!=0) {
            $porcentajeReal = 100*($preguntasCorregidas/$totalEfectivoPreguntas);
            //Redondeo al par más cercano
            $porcentajeEntero = round($porcentajeReal);
            
            return $porcentajeEntero;
        }
        
        else {
            return 0;
        }
        }



    /*
     * Método auxiliar que se encarga de verificar si el proceso de defensa puede comenzar o no
     * Devuelve true si es posible o false caso contrario
     */
    protected function _verificarInicioDefensa($id = null) {
        
        $this->loadModel('Evaluator');
        $this->loadModel('Problem');
        
        //Para iniciar el proceso de defensa, es necesario conocer previamente
        //si todos los evaluadores han corregido los problemas (del examen en cuestión) que tenian asignados.
        //Recupero los evaluadores de aquellos problemas asociados al examen en cuestion. NO puedo resolverlo
        //en una sola consulta porque puede ser que un mismo evaluador tenga más de un problema asociado
        
        //Debo primero recuperar los problemas que hay para el examen
        $problemas = $this->Problem->find('all', array('conditions' => array('exam_id' => $id),
            'recursive' => -1));
        
        $evaluadores = array(array());
        $contador=0;
        
        foreach($problemas as $problema) {
            //Para cada problema me fijo quien lo corrige
            $idEvaluador = $problema['Problem']['evaluator_id'];
            
            if (!$this->aparece($idEvaluador,$evaluadores)) {
                //Lo agrego como uno nuevo
                $evaluadores[$contador]['idEvaluador'] = $idEvaluador;
            }
            
            //Caso contrario ya aparece y solo incremento el contador
            $contador++;   
        }
             
        //Ya tengo una estructura con todos los evaluadores sin repetir que me interesan     
       
        //Para cada evaluador, analizo si ha termiando o no de corregir
        foreach ($evaluadores as $evaluador) {

            //Obtiene el id del evaluador
            $idEvaluador = $evaluador['idEvaluador'];

            //Recupera al evaluador que tiene ese id
            $evaluadorAsignado = $this->Evaluator->find('first', array('fields' => array('finish', 'surnames', 'names', 'email'),
                'conditions' => array('evaluator' => $idEvaluador),
                'recursive' => -1
            ));

                        
            //Recupero los datos que necesito del evaluador
            $estado = $evaluadorAsignado['Evaluator']['finish'];

            if (!$estado) {
                //Al menos un evaluador no corrigió.
                //El admin aún no puede realizar la defensa.
                return false;
            }
        }

        //Llegado este punto es porque todos los evaluadores terminaron de corregir, le permito hacer la defensa
        return true;
        }
    
    
    /*
     * Se fija si el $idEvaluador aparece en el arreglo $evaluadores el cual tiene formato
     * $evaluadores[indice]['idEvaluador']
     */
    protected function aparece($idEvaluador,$evaluadores) {
              
        //Debo verificar que no esté vacío para iterar
        if (!empty($evaluadores[0])) {
        
        foreach($evaluadores as $evaluador) {
            
            if($evaluador['idEvaluador']==$idEvaluador) {
                return true;
            }
            
        }
        }
        //Si está vacio o bien ya itero, seguro que no aparece
        return false;
    }
    
    
    
    /*
     * Función auxiliar para validar el id del examen y mostrar un mensaje en caso de que no aparezca
     */
    protected function _validarIDExamen($id,$texto) {
        
        $this->loadModel('Exam');
        //Debo verificar si el id pasado del examen realmente existe
        if (!$this->Exam->exists($id)) {
            
            //Recupero a filomena y redirijo allí
            $primerExamen = $this->Exam->find('first', array('recursive' => -1));
        
            //Obtengo el id y redirijo
            $idFilomena = $primerExamen['Exam']['id'];
            
            $this->Session->setFlash(__($texto), 'default', array('class' => 'alert alert-danger'));
            return $this->redirect(array('action' => 'examstate',$idFilomena));
        }
    }
    
    
    
    /*
     * Función auxiliar para validar el id del estudiante y redirigir a donde corresponda si falla
     */
    protected function _validarIDEstudiante($idStu,$idExam,$texto) {
        $this->loadModel('Student');
        //Debo verificar si el id pasado del examen realmente existe
        if (!$this->Student->exists($idStu)) {                      
            $this->Session->setFlash(__($texto), 'default', array('class' => 'alert alert-danger'));
            return $this->redirect(array('action' => 'examstate',$idExam));
        }
    }
    
    
    /*
     * Función auxiliar para validar el id del problema y redirigir a donde corresponda si falla
     */
    protected function _validarIDProblema($idProblema,$idExam,$texto) {
        $this->loadModel('Problem');
        //Debo verificar si el id pasado del examen realmente existe
        if (!$this->Problem->exists($idProblema)) {                      
            $this->Session->setFlash(__($texto), 'default', array('class' => 'alert alert-danger'));
            return $this->redirect(array('action' => 'examstate',$idExam));
        }
    }
    
    
    //Función encargada de listar todos los alumnos registrados a rendir el examen
    public function studentsdefense($id = null) {
        
        //Valida el id del examen
        $texto = 'El examen especificado no existe.';
        $this->_validarIDExamen($id,$texto);
        
        //Valida que efectivamente la defensa pueda hacerse
        $comienzoDefensa = $this->_verificarInicioDefensa($id);
        
        if ($comienzoDefensa==false) {
        $this->Session->setFlash(__('El proceso de defensa no puede realizarse, hay al menos un evaluador que no terminó de corregir. Revise "Proceso de corrección" para más información'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
        return $this->redirect(array('controller' => 'ManagerAdmin', 'action' => 'examstate', $id));
        }
        
        //Si puede comenzar hace el resto       
        //Cargado de modelos necesarios
        $this->loadModel('Student');
        $this->loadModel('Exam');
        
        /* Obtiene los estudiantes que comenzaron y terminaron efectivamente el examen. Los que no lo empezaron
         * no interesan porque no hay nada para defender en este caso
         */
        $alumnosACorregir = $this->Student->find('all', array(
            'order' => 'pseudonim ASC',
            'conditions' => array('init' => '1', 'finish'=>'1'),
            'recursive' => -1
        ));


        if (empty($alumnosACorregir[0])) {//Si no existen alumnos para corregir
            //Habrá que mostrar un error de alguna manera
            $this->Session->setFlash(__('No existen alumnos para corregir.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('controller' => 'ManagerAdmin', 'action' => 'examstate', $id));
        } else {
            //Guarda los datos necesarios para enviarle a la vista
            $alumnos = array(array());
            //Emplea un contador
            $index = 0;

            foreach ($alumnosACorregir as $alumno) {

                //Obtengo los datos necesarios del alumno
                $studentID = $alumno['Student']['student'];
                $pseudonimo = $alumno['Student']['pseudonim'];
                $defendido = $alumno['Student']['defended'];

                //Guardo los datos para la vista
                $alumnos[$index]['id'] = $studentID;
                $alumnos[$index]['pseudonimo'] = $pseudonimo;
                $alumnos[$index]['defendido'] = $defendido;

                //Incrementa el index
                $index++;
            }
        }
        
        //Me fijo si el examen ya fue defendido o no para deshabilitar o no el botón de finalizar defensa
        $examen = $this->Exam->find('first', array('conditions' => array('id' => $id), 'recursive' => -1));
        
        //Recupero el defend
        $examenDefendido = $examen['Exam']['defend'];
                
        //Envia la información a la vista encargada de mostrar los datos
        $this->set('idExam', $id);
        $this->set('examenDefendido',$examenDefendido);
        $this->set(compact('alumnos'));
    }
    
    
    
    //Funcion encargada de listar los problemas evaluados para un dado alumno, con el fin de que cada
    //alumno pueda defender cada problema en particular
    public function studentdefense($idExam = null, $idAlumno = null) {
        //Se cargan los modelos necesarios para la accion 
        $this->loadModel('Problem');
        $this->loadModel('Student');
        $this->loadModel('Section');
        $this->loadModel('SectionCollation');
        $this->loadModel('CollationTable');
        $this->loadModel('Exam');

               
        //Debe verificar que la defensa pueda efectivamente hacerse (por si te escribe en la URL) y además
        //que sean válidos el id del examen y el del alumno
        
        if (!$this->request->is('post')) {
        
            $texto = 'El examen especificado no existe.';
            $this->_validarIDExamen($idExam,$texto);

            //Valida que efectivamente la defensa pueda hacerse
            $comienzoDefensa = $this->_verificarInicioDefensa($idExam);

            if ($comienzoDefensa==false) {
            $this->Session->setFlash(__('No es posible comenzar la defensa para al alumno especificado en la URL porque hay al menos un evaluador que no terminó de corregir.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('controller' => 'ManagerAdmin', 'action' => 'examstate', $idExam));
            }

            //Valida el id del estudiante
            $texto = 'El alumno especificado no existe.';
            $this->_validarIDEstudiante($idAlumno,$idExam,$texto);
        
        }   
        
        //Si es un post es porque el administrador decidio finalizar la defensa para un alumno en particular. 
        //Recordar que se trata de un problema en particular, por lo que se determinarán las secciones defendidas
        //asociadas a este último
        
        
        //Recupera el alumno
        $alumno = $this->Student->find('first', array('conditions' => array('student' => $idAlumno)));
        $pseudonimo = $alumno['Student']['pseudonim'];

        $problemas = $this->Problem->query("SELECT * FROM problems WHERE exam_id='$idExam' ORDER BY problems.relative_order ASC;");
        $this->set('problemas', $problemas);

        //Guarda los datos necesarios para enviarle a la vista
        $problemasADefender = array(array());
        //Emplea un contador
        $index = 0;

        foreach ($problemas as $problema) {

            //Verifico si el problema ya ha sido defendido para el alumno. En primer lugar obtengo el id y el título asociado al problema que 
            //necesitaré luego para la vista

            $idProblema = $problema['problems']['id'];
            $ordenRelativo = $problema['problems']['relative_order'];
            $tituloAux = $problema['problems']['title'];
            $titulo = 'Caso N°' . $ordenRelativo . ': ' . $tituloAux;
            $problemaDefendido = false;

            //Verifico si alguna de las secciones del problema ha sido defendida, lo que implica que la defensa ha sido hecha para tal problema
            //Para esto debo recuperar las secciones asociadas a dicho problema. 
            //Para recuperar las secciones correspondientes al problema primero debo obtener la lista de cotejo asociada al mismo, sobre la que se 
            //definirán las secciones respectivas

            $tablaCotejo = $this->CollationTable->find('first', array('conditions' => array('problem_id' => $idProblema)));
            $idTablaCotejo = $tablaCotejo['CollationTable']['id'];

            $seccionesAsociadasaProblema = $this->SectionCollation->find('all', array(
                'conditions' => array('collation_table_id' => $idTablaCotejo),
                'recursive' => -1));


            foreach ($seccionesAsociadasaProblema as $seccion) {
                $idSeccion = $seccion['SectionCollation']['id'];

                //Se fija si el problema de ese alumno y ese collation table está defendido o no
                $defendido = $this->Problem->query("SELECT defended_section FROM students_notes WHERE problem_id='$idProblema' and section_collation_id='$idSeccion' and student_id='$idAlumno';");
                
                if (!empty($defendido)) {
                    $seccionDefendida = $defendido[0]['students_notes']['defended_section'];

                    //Como la defensa se realiza por problema, que se haya defendido alguna de las secciones implica que el problema
                    //ha sido defendido en su totalidad tambien. Se remarca que un alumno no necesariamente defenderá todas las secciones
                    //respectivas a un problema, por lo que alcanzará con que una de ellas sea defendida para que el problema
                    //sea considerado como defendido.

                    if ($seccionDefendida) {
                        $problemaDefendido = true;
                        //No tengo que iterar para todas las secciones, con que haya una que ya está defendida
                        //de ese problema, listo!
                        break;
                    }
                }
            }

            //Guardo los datos para la vista
            $problemasADefender[$index]['id'] = $idProblema;
            $problemasADefender[$index]['titulo'] = $titulo;
            $problemasADefender[$index]['defendido'] = $problemaDefendido;


            //Incrementa el index
            $index++;
        }

        //Guarda los datos para enviarle a la vista
        $this->set('pseudonimo',$pseudonimo);
        $this->set('idExam', $idExam);
        $this->set('idAlumno', $idAlumno);
        $this->set(compact('problemasADefender'));
    }
    
    
    //Accion encargada de finalizar la defensa. Esto implica que el proceso de defensa no pueda continuar para ningun alumno
    //de forma que para cada uno de ellos se actualiza el campo 'defended' asignándole el valor 1
    public function enddefense($id = null) {

        //La finalizacion del proceso de defensa, deshabilita la posibilidad que los alumnos continuen con la defensa del examen.
        $this->loadModel('Exam');
        $this->loadModel('Student');
        
        $texto = 'El examen especificado no existe.';
        $this->_validarIDExamen($id,$texto);
        
        //Debe verificar que el examen exista y que la defensa haya podido iniciar
        $resultado = $this->_verificarInicioDefensa($id);
     
        if ($resultado==false) {
            //No puede finalizar la defensa porque ni siquiera está en condiciones de comenzarla
            //Esto solo puede pasar si escribe en la URL la finalización, porque por contról nunca va a poder
            //tocar el botón si no entra a defensa, la cual mira estas cuestiones
            $this->Session->setFlash(__('No es posible finalizar la defensa, porque hay al menos un evaluador que no terminó de corregir. Verifique "Proceso de corrección".'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('controller' => 'ManagerAdmin', 'action' => 'examstate', $id));
        }
        
        
        //Recupero el examen y me fijo si ya está defendido
        $examen = $this->Exam->find('first', array('fields' => 'defend',
            'conditions' => array('id' => $id), 'recursive' => -1
        ));
        
        $yaDefendido = $examen['Exam']['defend'];
        
        if ($yaDefendido) {
            //Muestro un error y redirijo
            $this->Session->setFlash(__('El examen ya está defendido, no es posible volver a finalizar la defensa.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('action' => 'examstate',$id)); 
        }
        
        else {
        //Actualiza el defend del examen como para ya tener ahí que fue defendido y hacer controles más fácilmente
        //Pongo el id para actualizar
        $this->Exam->id = $id;
        $actualizoBienDefend = $this->Exam->saveField('defend', 1);

        if (!$actualizoBienDefend) {
                //Ocurrió un error
                $this->Session->setFlash(__('Error en la actualización de la base de datos.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                return $this->redirect(array('action' => 'examstate',$id));                            
        } 
        
        
        
        
        /*
         * Notar que se le pone a todos los que no están defendidos, sin importar si empezó o no. Esto es correcto
         * que así sea por cuestiones de exportación.
         */
        $estudiantesSinTerminar = $this->Student->find('all', array('fields' => 'student',
            'conditions' => array('defended' => 0), 'recursive' => -1
        ));

        foreach ($estudiantesSinTerminar as $estudiante) {
            //Para cada alumno, obtiene su id y le pone el defended en 1 si es que lo tiene en 0
            $idEstudiante = $estudiante['Student']['student'];
            
            //Pongo el id para actualizar
            $this->Student->id = $idEstudiante;
            $actualizoBienDefended = $this->Student->saveField('defended', 1);

            if (!$actualizoBienDefended) {
                    //Ocurrió un error
                    $this->Session->setFlash(__('Error en la actualización de la base de datos.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                    return $this->redirect(array('action' => 'examstate',$id));                            
            } 
        }
               
                
        //LLegado este punto todo tuvo éxito.
        //Muestra un mensaje y redirige
        $this->Session->setFlash(__('El proceso de defensa ha finalizado exitosamente.'), 'default', array('class' => 'alert alert-dismissable alert-success'));
        return $this->redirect(array('action' => 'studentsdefense', $id));
    }
    }

    //Funcion encargada de mostrar la lista de cotejo asociada a un alumno a un determinado problema, 
    // con el objetivo de que el alumno pueda defender cada competencia de dicha lista
    public function collationtableproblem($idExam = null, $idAlumno = null, $idProblema = null) {

        //Cargado de modelos necesarios
        $this->loadModel('CollationTable');
        $this->loadModel('Section');
        $this->loadModel('SectionCollation');
        $this->loadModel('Item');
        $this->loadModel('Correction');
        $this->loadModel('Student');
        $this->loadModel('Problem');

        //Si se trata de un post es porque el administrador ya evaluó los items de defensa para el alumno,
        //luego se redirecciona al mismo a los problemas a evaluar para el alumno en cuestion
        if ($this->request->is('post')) {
            //Variable auxiliar
            $alMenosUna = false;
            
            //Variables booleanas para saber qué mensaje mostrar en cada caso
            $erroresNumericos = false;
            $erroresRango = false;
            
            //Recupera los datos del request data            
            $defensa = $this->request->data;
            
            $idAlumno = $defensa['CollationTable']['idAlumno'];
            $idProblema = $defensa['CollationTable']['idProblema'];
            $idExam = $defensa['CollationTable']['idExam'];
            $idSecciones = unserialize($defensa['CollationTable']['idSecciones']);
            
            //Se obtiene la tabla de cotejo asociada al problema para luego determinar las secciones de la misma
            $TablaCotejo = $this->CollationTable->find('all', array(
            'conditions' => array('problem_id' => $idProblema),
             ));
            
            //Se obtiene el id de la tabla de cotejo
            $idTablaCotejo=$TablaCotejo[0]['CollationTable']['id'];
            

            //Calculo de notas parcial por seccion
            //A este punto solo existe nota_parcial, nota defensa y nota final no se encuentran actualizadas 
            //ya que todavia no se termino la defensa del alumno
            $this->_notesbysections($idProblema, $idTablaCotejo, $idAlumno);
            
            $errores = array();
            $valoresDefault = array();
            $noNumericos = array();

            $flagExistenErrores = false;
            
            $seccionesADefender = $TablaCotejo[0]['SectionCollation'];
            
             $i = 0;
            
            foreach ($seccionesADefender as $seccionDef){
                
                $idSeccion = $seccionDef['id'];
                
                //Recupero nota parcial (resultando de sumar los yes_no de la tabla corrections de los evaluadores)
                $notaParcial = $this->Item->query("SELECT parcial_note FROM students_notes WHERE student_id='$idAlumno' AND problem_id='$idProblema' AND section_collation_id='$idSeccion';");
                $numNotaParcial = $notaParcial[0]['students_notes']['parcial_note'];
                
                //Recupero ponderacion de la seccion 
                $porcentajeSeccion = $this->Item->query("SELECT percentage FROM section_collations WHERE id='$idSeccion';");
                $numPorcentajeSeccion = $porcentajeSeccion[0]['section_collations']['percentage'];


                //Se realizan las comparaciones pertinentes para establecer el rango de valores que el admin puede sumar en la defensa
                // por seccion de un problema/tabla de cotejo y un estudiante en particular

                if (!empty($defensa['defensa' . $i]) || ($defensa['defensa' . $i]== '0')) {
                    //Recupera la nota de defensa
                    $notaDefensa = $defensa['defensa' . $i];
                    
                                        
                    if (!(is_numeric($notaDefensa))) {
                        //Hay un valor ingresado en alguna de las secciones que NO es numérico
                        $erroresNumericos = true;
                        $flagExistenErrores = true;
                        $noNumericos[$i] = true;
                        $valoresDefault[$i] = $notaDefensa;
                    } 
                    
                    else {
                        //El valor ingresado es numérico para esta sección
                        $noNumericos[$i] = false;
                        $notaFinal = $numNotaParcial + $notaDefensa;

                        //Verifica si el valor ingresado por el usuario supera o  no lo explicitado en el rango
                        //La política empleada en este caso es no guardar en la BD salvo que los tres valores
                        //sean correctos y cumplan con las condiciones explicitadas (numérico y dentro del rango).
                        if ($notaFinal <= $numPorcentajeSeccion) {
                            
                            //Hay al menos una actualización
                            $alMenosUna = true;
                            
                            //Actulizo la información que pueda en las bases de datos. Notar que si hay algunas secciones
                            //que NO se pueden actualizar, pero otras que sí, entonces actualizará sólo aquellas que pueda
                            $this->Item->query("UPDATE students_notes SET defense_note='$notaDefensa' WHERE student_id='$idAlumno' AND problem_id='$idProblema' AND section_collation_id='$idSeccion';");
                            $this->Item->query("UPDATE students_notes SET final_note='$notaFinal' WHERE student_id='$idAlumno' AND problem_id='$idProblema' AND section_collation_id='$idSeccion';");
                            $this->Item->query("UPDATE students_notes SET defended_section='1' WHERE student_id='$idAlumno' AND problem_id='$idProblema' AND section_collation_id='$idSeccion';");  
                            
                            //No hay errores
                            $errores[$i] = false;
                            $valoresDefault[$i] = $notaDefensa;
                            
 
                        } else {                           
                            //El valor ingresado SUPERA al permitido
                            $erroresRango=true;
                            
                            $seccion_tabla_sections =  $this->SectionCollation->find('first', array('conditions' => array('SectionCollation.id' => $idSeccion)));
                            $id_seccion_tabla_sections = $seccion_tabla_sections['SectionCollation']['id'];
                            
                            $nombreSeccion =  $this->Section->find('first', array('conditions' => array('id' => $id_seccion_tabla_sections)));
                            $seccion = $seccion_tabla_sections['Section']['name'];
               
                            $flagExistenErrores = true;
                            $valoresDefault[$i] = $notaDefensa;
                            $errores[$i] = true;
                        } 
                    }
                }
                //Si no fue evaluado el item de defensa implica que tampoco hay errores asociados, dado que no hay una entrada en el input
                else {
                    $errores[$i] = false;
                    $noNumericos[$i] = false;
                }
                     $i++;
            }
            
            
            //Si hay al menos una actualización, entonces actualizo el defended
            if ($alMenosUna==true) {
                //Acá debo fijarme si todo salió bien o no para grabar y actualizar el defended
                //Actualizo el valor de defended del alumno, con que defienda una sección ya asumo que defendió
                //Esto debe hacerse siempre que no haya errores
                //Actualizo manualmente el campo
                $this->Student->id = $idAlumno;
                $actualizoBienDefended = $this->Student->saveField('defended', 1);

                if (!$actualizoBienDefended) {
                    //Ocurrió un error
                    $this->Session->setFlash(__('Error en la actualización de la base de datos.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                    return $this->redirect(array('action' => 'collationtableproblem', $idExam, $idAlumno, $idProblema));                            
                }
            }
            
            
            //Si no existen errores, se redirige a la vista del alumno listando los posibles problemas a defender
            if (!$flagExistenErrores) {            
                $this->Session->setFlash(__('Datos guardados exitosamente.'), 'default', array('class' => 'alert alert-dismissable alert-success'));
                return $this->redirect(array('controller' => 'ManagerAdmin', 'action' => 'studentdefense', $idExam, $idAlumno));
            }
            
            else {
                //Hay errores, imprimo un mensaje según el caso
                if ($erroresNumericos && !$erroresRango) {
                    //Solo errores numéricos
                    $this->Session->setFlash(__('Sólo se permiten valores numéricos para el Item de defensa.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                }
                
                else if ($erroresRango && !$erroresNumericos) {
                    //Solo errores rango
                    $this->Session->setFlash(__('El valor ingresado para un item de defensa supera el rango especificado.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                }
                
                else {
                    //Son los dos
                    $this->Session->setFlash(__('Sólo se permiten valores numéricos dentro del rango especificado para cada Item de defensa.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
                }
                
                
                
            }
            
        } 
        
        else {
            //En caso de que no se trate de un post será porque es la primera vez que se accede a esta vista y
            //es porque el administrador desea comenzar la defensa para el alumno, por lo que es necesario 
            //determinar que no existen errores al cargar esta vista.
            
            //Primero hay que validar que el proceso de defensa realmente se pueda hacer, por si te escriben esta
            //URL de una
            
            $textoAux = 'El examen especificado no existe.';
            $this->_validarIDExamen($idExam,$textoAux);
            
            //Valida que efectivamente la defensa pueda hacerse (esto ya valida el id del examen)
            $comienzoDefensa = $this->_verificarInicioDefensa($idExam);

            if ($comienzoDefensa==false) {
            $this->Session->setFlash(__('No es posible comenzar la defensa para al alumno especificado en la URL porque hay al menos un evaluador que no terminó de corregir.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('controller' => 'ManagerAdmin', 'action' => 'examstate', $idExam));
            }
            
            //Valida el id del estudiante
            $texto = 'El alumno especificado en la URL no existe.';
            $this->_validarIDEstudiante($idAlumno,$idExam,$texto);

            //Valida el id del problema
            $texto2 = 'El problema especificado en la URL no existe.';
            $this->_validarIDProblema($idProblema,$idExam,$texto2);
                      
            //Se obtiene la tabla de cotejo asociada al problema para luego determinar las secciones de la misma
            $TablaCotejo = $this->CollationTable->find('all', array(
            'conditions' => array('problem_id' => $idProblema),
        ));
            
            //Se obtiene el id de la tabla de cotejo
            $idTablaCotejo=$TablaCotejo[0]['CollationTable']['id'];
            
            
            //Secciones defendidas
            $seccionesDefendidas = $TablaCotejo[0]['SectionCollation'];
            
            
            $i = 0;
            
            
            //Se cargan valores previos en caso de que existan. Si no existen, se pone como valor de defensa 0.
            foreach ($seccionesDefendidas as $seccionDef){
                //Se debe verificar si existen valores asignados a una defensa previa,
                //de ser así se cargan dichos valores
                $idSeccionDefendida = $seccionDef['id'];
                
                $existeDefensaEstudiante = $this->Item->query("SELECT * FROM students_notes WHERE student_id='$idAlumno' AND problem_id='$idProblema' AND section_collation_id='$idSeccionDefendida';");
                if (!empty($existeDefensaEstudiante)) {
                    //Existen valores asignados de una defensa previa
                    $valoresDefault[$i] = $existeDefensaEstudiante[0]['students_notes']['defense_note'];
                    
                } else {
                    //El estudiante no ha hecho ninguna defensa aún
                    $valoresDefault[$i] = "0";
                }

                //No existen errores si ya hay una defensa hecha previamente y se comienza la defensa otra vez
                $errores[$i] = false;
                $noNumericos[$i] = false;
                $i++;
            }
            
        }
        
        //En la siguiente consulta se obtienen las secciones con toda su informacion respectiva (porcentaje, nombre)
        //de la lista de cotejo asociada al problema a defender
        //Para lograr obtener las secciones en el orden correcto, se hace un join con la tabla sections que es la que determina
        //tal orden 
        $seccionesTabla = $this->SectionCollation->find("all", array(
            "joins" => array(
                array(
                    "table" => "sections",
                    "alias" => "Sections",
                    "type" => "LEFT",
                    "conditions" => array(
                        "SectionCollation.section_id = Sections.id"
                    )
                )),
            'order' => array('Sections.relative_order ASC'),
            'fields' => array(
                'SectionCollation.*',
                'Sections.*'
            ),
            'conditions' => array('SectionCollation.collation_table_id' => $idTablaCotejo),
           
        ));
        
        //Guarda los datos necesarios para enviarle a la vista, entre ellos los items con su respectiva correccion (si o no)
        //dada por el evaluador
        $itemsADefender = array(array());
        $secciones = array();
        $idSecciones = array();

        //Empleamos contadores para las iteraciones de secciones y de items por seccion
        
        $indexSeccion = 0;
        
        //Se itera entre las secciones que componen la lista de cotejo
        foreach ($seccionesTabla as $seccion){
            //Se almacena el id de la seccion
            $idSeccion = $seccion['Sections']['id'];
            $idSecciones[$indexSeccion] = $idSeccion;
            
            //Se almacena el nombre de la seccion
            $nombreSeccion = $seccion['Sections']['name'];
            $secciones[$indexSeccion] = $nombreSeccion;
            
            //Items asociados a esa sección
            $items= $seccion['Item'];
            
            $indexItem = 0;
            //Se itera sobre los items que componen una seccion dada de la lista de cotejo
            foreach ($items as $item){
                //Se obtiene el id
                $idItem = $item['id'];
                //Se obtiene la descripción
                $nombreItem = $item['description'];
                //Se obtiene el porcentaje
                $porcentajeItem = $item['percentage'];
                //Se obtiene si el valor de la correción
                $correccionItem = $this->Correction->query("SELECT yes_no_answer FROM corrections WHERE item_id='$idItem' AND student_id='$idAlumno';");
                if (!empty($correccionItem)) {
                    $evalItem = $correccionItem[0]['corrections']['yes_no_answer'];
                }

                //Guardo los datos para la vista asociados a cada item de la lista de cotejo ya evaluada
                $itemsADefender[$indexSeccion][$indexItem]['id'] = $idItem;
                $itemsADefender[$indexSeccion][$indexItem]['nombre'] = $nombreItem;
                $itemsADefender[$indexSeccion][$indexItem]['porcentaje'] = $porcentajeItem;
                $itemsADefender[$indexSeccion][$indexItem]['correcion'] = $evalItem;
                $itemsADefender[$indexSeccion][$indexItem]['seccion'] = $nombreSeccion;
                $itemsADefender[$indexSeccion][$indexItem]['idSeccion'] = $idSeccion;


                //Incrementa el index de los items
                $indexItem++;
            }
            //Incrementa el index de las secciones
            $indexSeccion++;
        }


        //Se setean a la vista algunas variables necesarias para la próxima vista.
        //Será necesario conocer tanto el id del problema, como del alumno, así como los ids de las competencias
        //secciones evaluadas según la lista de cotejo asociada al problema a defender
        
        $this->loadModel('Exam');
        
        //Sea o no un post me busco el examen para saber si está defendido o no y se lo mando a la vista
        //Recupero el examen y me fijo si ya está defendido
        $examenAsociado = $this->Exam->find('first', array('fields' => 'defend',
            'conditions' => array('id' => $idExam), 'recursive' => -1
        ));

        $defendido = $examenAsociado['Exam']['defend'];
        //Lo guardo en este caso a la vista
        $this->set('defendido', $defendido);
        
        //Se manda a la vista los nombres de las secciones 
        $this->set('secciones', $secciones);
        //Se manda a la vista los ids de las secciones 
        $this->set('idSecciones', $idSecciones);
        
        //Se obtiene información correspondiente al problema
        $problema = $TablaCotejo[0]['Problem'];
        
        $ordenRelativo = $problema['relative_order'];
        $tituloAux = $problema['title'];
        $titulo = 'Caso N°' . $ordenRelativo . ': ' . $tituloAux;
        
        //Se envía a la vista información del problema
        $this->set('problema', $ordenRelativo);
        $this->set('titulo',$titulo);
        $this->set('idProblema', $idProblema);

        $pseudonimo = $this->Student->query("SELECT pseudonim FROM students WHERE student='$idAlumno';");
        $this->set('alumno', $pseudonimo[0]['students']['pseudonim']);
        $this->set(compact('itemsADefender'));
        $this->set('idAlumno', $idAlumno);

        $this->set('idExam', $idExam);

        $this->set('errores', $errores);
        $this->set('noNumericos', $noNumericos);
        $this->set('valoresDefault', $valoresDefault);
    }

    protected function _notesbystudent() {

        $this->loadModel('Student');

        $notasPorEstudiante = array();

        $students = $this->Student->find('all', array('fields' => array('student', 'pseudonim'), 'order' => array('Student.pseudonim ASC'), 'recursive' => -1
        ));

        foreach ($students as $student):
            $student_id = $student['Student']['student'];
            $student_pseudonim = $student['Student']['pseudonim'];

            $notas = $this->_notesbyproblem($student_id);
            $notasEstudiante = array($student_pseudonim => $notas);
            array_push($notasPorEstudiante, $notasEstudiante);
        endforeach;

        return $notasPorEstudiante;
    }

    protected function _notesbyproblem($student_id = null) {

        //Para cada problema/lista de cotejo

        $this->loadModel('CollationTable');

        $suma = 0;

        $notasPorProblema = array();

        $collation_tables = $this->CollationTable->find('all', array('fields' => array('id', 'problem_id'), 'recursive' => -1
        ));
        foreach ($collation_tables as $collation_table):
            $collation_table_id = $collation_table['CollationTable']['id'];
            $problem_id = $collation_table['CollationTable']['problem_id'];



            $notesByProblem = $this->_notesbysections($problem_id, $collation_table_id, $student_id);
            //array (array[array(section_id,sumaNota,name),array(section_id,sumaNota,name),array(section_id,sumaNota,name))],..)
            // $notaProblema=array($problem_id=>$notesByProblem);
            array_push($notasPorProblema, $notesByProblem);
        endforeach;


        return $notasPorProblema;
    }

    protected function _notesbysections($problem_id = null, $collation_table_id = null, $student_id = null) {

        $this->loadModel('Section');
        $this->loadModel('SectionCollation');

        $suma = 0;

        $notasPorSeccion = array();

        $secciones = $this->SectionCollation->query("SELECT section_collations.id, sections.name , sections.relative_order
                                                          FROM section_collations
                                                          LEFT JOIN sections
                                                          ON section_collations.section_id=sections.id
                                                          WHERE section_collations.collation_table_id='$collation_table_id'
                                                          ORDER BY sections.relative_order ASC;");



        //Obetern suma : Notas SO, Notas E, Notas P

        foreach ($secciones as $seccion):

            $section_collation_id = $seccion['section_collations']['id'];
            $section_name = $seccion['sections']['name'];
            $sumaNotas = $this->_addnotes($problem_id, $collation_table_id, $student_id, $section_collation_id);

            $notaSeccion = array($section_name => $sumaNotas);
            array_push($notasPorSeccion, $notaSeccion);
            //Tendria que hacer un push, y obtener un arreglo con la estructura:
            // array(array(section_id,sumaNota,name),array(section_id,sumaNota,name),array(section_id,sumaNota,name))

        endforeach;

        //debug($notasPorSeccion);
        return $notasPorSeccion;
    }

    protected function _addnotes($problem_id = null, $collation_table_id = null, $student_id = null, $section_collation_id = null) {
        //AQUI SE DEBE DISTINGUIR EL CONJUNTO DE ITEMS QUE NO ESTAN PRESENTE EN LA TABLA CORRECCIONES,
        //ESTOS SERAN LOS ITEMS DE DEFENSA.

        $this->loadModel('Item');
        $this->loadModel('Correction');


        $suma = 0;


        $items = $this->Item->find('all', array('fields' => array('id', 'percentage'), 'conditions' => array('section_collation_id' => $section_collation_id, 'problem_id' => $problem_id), 'recursive' => -1
        ));

        foreach ($items as $item):

            $item_id = $item['Item']['id'];
            $item_value = $item['Item']['percentage'];
            $correcciones = $this->Correction->find('all', array('fields' => array('yes_no_answer', 'item_id'), 'conditions' => array('item_id' => $item_id, 'student_id' => $student_id), 'recursive' => -1
            ));



            //Solo tengo una correccion asociada al item y al estudiante, pero hago foreach para
            //no tener que utilizar indice [0]
            foreach ($correcciones as $correccion):

                if ($correccion['Correction']['yes_no_answer']) {

                    //suma valor del item
                    $suma = $suma + $item_value;
                }
            endforeach;


        endforeach;

        //guardar nota parcial ($suma) en parcial_note
        // DEBERIA INSERTAR UNA UNICA VEZ Y DESPUES HACER UPDATES DE LOS CAMPOS CORRESPONDIENTES.

        $existeDefensaEstudiante = $this->Item->query("SELECT * FROM students_notes WHERE student_id='$student_id' AND problem_id='$problem_id' AND section_collation_id='$section_collation_id';");
        if (empty($existeDefensaEstudiante)) {
            $notaparcial = $this->Item->query("INSERT INTO students_notes ( `student_id`, `section_collation_id`, `problem_id`,`defense_note`,`final_note`, `parcial_note`)  VALUES ('$student_id','$section_collation_id','$problem_id',0,0,'$suma');");
        } else {
            //ya existe la entrada asociada al estudiante
        }

        //Nota + nota defensa (si la defensa aun no esta hecha suma cero por defecto)
        //defense_note DEFAULT 0
        $notaDefensa = $this->Item->query("SELECT defense_note FROM students_notes WHERE student_id='$student_id' AND problem_id='$problem_id' AND section_collation_id='$section_collation_id';");
        $notaEstudianteDefensa = $notaDefensa[0]['students_notes']['defense_note'];
        $suma = $suma + $notaEstudianteDefensa;


        return $suma;
    }

    protected function _pseudonims() {

        $this->loadModel('Student');

        //ORDER BY en esta consulta y en la de _notesbystudents asi quedan iguales y los puedo acceder siempre bien
        $pseudonimos = array();
        $students = $this->Student->find('all', array('fields' => array('student', 'pseudonim'), 'order' => array('Student.pseudonim ASC'), 'recursive' => -1
        ));

        foreach ($students as $student):

            $student_id = $student['Student']['student'];
            $student_pseudonim = $student['Student']['pseudonim'];
            array_push($pseudonimos, $student_pseudonim);

        endforeach;

        return $pseudonimos;
    }

     protected function _studentsnames() {

        $this->loadModel('Student');

        //ORDER BY en esta consulta y en la de _notesbystudents asi quedan iguales y los puedo acceder siempre bien
        $nombres_estudiantes = array();
      
        
           $students = $this->Student->find('all', array('fields' => array('names', 'surnames'), 'order' => array('Student.pseudonim ASC'), 'recursive' => -1
        ));
        
        foreach ($students as $student):

            $student_name = $student['Student']['names'].' '.$student['Student']['surnames'];
            array_push($nombres_estudiantes,  $student_name);

        endforeach;

        return $nombres_estudiantes;
    }

    protected function _sections() {




        $this->loadModel('Section');


        $sections = $this->Section->find('all', array('fields' => 'name', 'order' => array('Section.relative_order ASC'), 'recursive' => -1
        ));



        $secciones = Set::classicExtract($sections, '{n}.Section.name');

        //ORDER BY en esta consulta y en la de _notesbystudents asi quedan iguales y los puedo acceder siempre bien





        return $secciones;
    }

    //Verifica si la defensa ha finalizado o no
    private function _finishdefense($idExam) {


        $this->loadModel('Exam');

        $examen = $this->Exam->find('first', array('conditions' => array('id' => $idExam), 'recursive' => -1));

        $defendido = $examen['Exam']['defend'];
        
        return $defendido;
    }

    public function export($id = null) {


        $this->layout = 'ajax';

        $defensaCompleta = $this->_finishdefense($id);


        if ($defensaCompleta) {

            $this->loadModel('Problem');

            $cantProblems = $this->Problem->find('count', array('recursive' => -1
            ));

            //Ordenarlos de alguna forma asi siemp regresan en el mismo orden
            $titulosProblemas = $this->Problem->find('all', array('fields' => 'title', 'recursive' => -1
            ));


            $notasPorEstudiante = $this->_notesbystudent();
            $pseudonims = $this->_pseudonims();
            $sections = $this->_sections();
            $studentsnames=$this->_studentsnames();

            $this->set('sumaNotas', $notasPorEstudiante);
            $this->set('pseudonimos', $pseudonims);
            $this->set('secciones', $sections);
            $this->set('cantProblemas', $cantProblems);
            $this->set('nombresProblemas', $titulosProblemas);
            $this->set('defensaCompleta', true);
            $this->set('nombresEstudiantes', $studentsnames);
            
        } else {
            $this->set('defensaCompleta', false);
            $this->Session->setFlash(__('La exportacion de datos no se puede realizar dado que el proceso de defensa no ha sido finalizado.'), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect(array('controller' => 'ManagerAdmin', 'action' => 'examstate', $id));
        }
    }
    
    
    
    
    
    /*
     * Dado un problema, una pregunta y un alumno, devuelve true si la misma ha sido corregida
     * en su totalidad para éste o false caso contrario.
     */

    protected function _preguntaCorregidaParaAlumno($problemID, $questionID, $studentID) {


        //Obtiene, empleando un método auxiliar el conjunto de secciones e items asociados a 
        //el problema y la pregunta, junto con un valor en cada uno que indica si fue o no
        //corregido. Simplemente mirando que para el alumno en cuestión todos los valores sean
        //1 o 0 ya sé que han sido corregidos
        //Obtiene un arreglo con información de las secciones y los items ya organizada
        $seccionesItems = $this->_seccionesItemsAsociados($problemID, $questionID, $studentID);
        
        foreach ($seccionesItems as $seccion) {

            //Obtiene los items asociados
            $itemsSeccion = $seccion['items'];

            foreach ($itemsSeccion as $item) {
                //Si hay al menos un item que en valor de selección tiene -1, ya está, no está corregida
                $valorSeleccion = $item['seleccionar'];

                if ($valorSeleccion == -1) {
                    //Este alumno no tiene la pregunta corregida
                    return false;
                }
            }
        }

        //Llegado este punto, el alumno ya tiene corregida la pregunta
        return true;
    }

    /*
     * Determina, dado un problema y una pregunta (IDS) el conjunto de secciones
     * e items asociados a los mismos.
     * Para ver el formato de salida hacer un debug.
     * Notar que como este método se emplea desde el resto de las acciones y siempre le enviamos
     * un question_id, entonces nunca va a devolver los items de defensa,porque estos tienen NULL
     * como question_id y yo siempre le envio un id válido de pregunta al igual que el resto.
     */

    protected function _seccionesItemsAsociados($problemID, $questionID, $studentID) {

        //Cargado de modelos necesarios
        $this->loadModel('Question');
        $this->loadModel('Student');
        $this->loadModel('CollationTable');
        $this->loadModel('Section');
        $this->loadModel('SectionCollation');
        $this->loadModel('Item');
        $this->loadModel('Correction');


//NO chequeo que sean ids existentes porque estoy invocando desde un método interno
//el cual nunca va a obtener un id que no sea válido y este método es privado, no
//puede accederse desde afuera

        /*
         * Obtiene, dado el id del problema, la tabla de cotejo completa para obtener la información
         * que luego necesite
         */
        $tablaCotejo = $this->CollationTable->find('first', array(
            'conditions' => array('problem_id' => $problemID)
        ));

                
//Emplea una variable para contar secciones
        $contadorSeccion = 0;
        $contadorItems = 0;

        $salida = array(array());
        $itemsAsociados = array(array());

        if (empty($tablaCotejo['CollationTable']) || (empty($tablaCotejo['SectionCollation'][0]))) {
            //Si no existe una tabla de cotejo asociada a ese problema
            //O no existen secciones asociadas a la tabla de cotejo) 
            //Devolver que ha sucedido un error
        } else {
            //La lista de cotejo tiene elementos asociados y además hay secciones
            //De la tabla de cotejo, obtengo las secciones que tiene la misma
            $seccionesColaciones = $tablaCotejo['SectionCollation'];

            //Itera para cada una de las secciones encontrando los items asociados a éstas
            foreach ($seccionesColaciones as $seccionColacion) {

                //Obtiene el id de la sección actual y el nombre
                $idSeccionColacion = $seccionColacion['id'];
                
                //Para la obtención del nombre se necesita una redirección adicional, tengo que buscar
                //dado el section_id en Section a ver el nombre
                $idSeccion = $seccionColacion['section_id'];
                
                $seccionAsociada = $this->Section->find('first', array(
                            'conditions' => array('id' => $idSeccion), 'recursive' => -1));
                
                //Obtiene de la sección asociada el orden así como el nombre de la sección
                $tituloSeccion = $seccionAsociada['Section']['name'];
                $ordenRelativoSeccion = $seccionAsociada['Section']['relative_order'];
                
                
                //Dada una sección, obtiene los items de la misma definidos para la pregunta
                //que fue seleccionada
                $items = $this->Item->find('all', array(
                    'conditions' => array('section_collation_id' => $idSeccionColacion, 'question_id' => $questionID),
                    'recursive' => -1
                ));


                if (!empty($items[0])) { //Si la sección tiene items para esta pregunta
                    //Itera para cada uno de los items fijandose que ya haya una entrada
                    //en la tabla de correcciones
                    //Guarda el titulo de la sección
                    $salida[$contadorSeccion]['tituloSeccion'] = $tituloSeccion;
                    $salida[$contadorSeccion]['ordenRelativoSeccion'] = $ordenRelativoSeccion;

                    foreach ($items as $item) {

                        //Obtengo el id del item y su descripción
                        $idItem = $item['Item']['id'];
                        $descripciónItem = $item['Item']['description'];

                        //Obtiene la corrección, si es que existe para en una variable más indicar qué check
                        //habrá que poner seleccionado (quizás ninguno si no existe).
                        $correccionAsociada = $this->Correction->find('first', array(
                            'conditions' => array('item_id' => $idItem, 'student_id' => $studentID), 'recursive' => -1));

                        //Debe fijarse si es vacia o no, ya que en caso de ser vacia habrá que crearla y en caso contrario no
                        if (empty($correccionAsociada)) {
                            //No hay ninguna corrección, no debe estar seleccionado ni el Sí ni el No
                            //Lo represento con un -1
                            $valorSeleccionar = -1;
                        } else {
                            //Existe una corrección, debo fijarme el yes_no_answer
                            $yes_no_answer = $correccionAsociada['Correction']['yes_no_answer'];
                            //Será 0 para que haya que ponerle el check al No y 1 para el Sí
                            if ($yes_no_answer) {
                                //Es verdadero, le pongo un 1
                                $valorSeleccionar = 1;
                            } else {
                                //Es falso, le pongo un 0
                                $valorSeleccionar = 0;
                            }
                        }

                        //Guarda los datos para la salida
                        $itemsAsociados[$contadorItems]['id'] = $idItem;
                        $itemsAsociados[$contadorItems]['descripcion'] = $descripciónItem;
                        $itemsAsociados[$contadorItems]['seleccionar'] = $valorSeleccionar;

                        //Incrementa el contador de items
                        $contadorItems++;
                    }

                    //Guarda los items para la sección
                    $salida[$contadorSeccion]['items'] = $itemsAsociados;

                    //Incrementa el contador de la sección
                    $contadorSeccion++;

                    //Reinicia el contador para los items
                    $contadorItems = 0;

                    //Limpia el arreglo de items asociados para evitar conflictos
                    $itemsAsociados = array(array());
                } else {
                    //No hay items definidos para la pregunta
                    //Habra un error
                }
            }


            //Devuelve la variable de salida
            return $salida;
        }
    }


    /*
     * Redefine el método isAuthorized para este controlador. 
     * Se verifica el rol del usuario que esta logueado y dependiendo del mismo se establecen
     * los permisos correspondientes.
     */

    public function isAuthorized($user) {
        $esAdmin = false;
        $esStudent = false;
        $esEvaluator = false;
        if (AuthComponent::user()) {

            $nombreRol = $this->Session->read('Auth.User.role');

            if (strcmp("Administrador", $nombreRol) === 0) {
                //Es administrador
                $esAdmin = true;
            } else if (strcmp("Evaluador", $nombreRol) === 0) {
                //Es evaluador
                $esEvaluator = true;
            } else if (strcmp("Alumno", $nombreRol) === 0) {
                //Es alumno
                $esStudent = true;
            }
        }

        if ($esAdmin && (!$esEvaluator) && (!$esStudent)) {
            return true;
        } else if ((!$esAdmin) && $esEvaluator && (!$esStudent)) {
            $this->Session->setFlash(__($this->Auth->authError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect('/managerEvaluator/assignedproblems');
        } else if ((!$esAdmin) && (!$esEvaluator) && $esStudent) {
            $this->Session->setFlash(__($this->Auth->authError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect('/StudentsAnswers/index');
        }
        
        //Llegado este punto, el isAuthorized por la configuración de AppController devuelve false
        return parent::isAuthorized($user);
    }

}
