<?php

App::uses('AppController', 'Controller');


//App::import('Vendor', 'PHPExcel/Classes/PHPExcel');

/**
 * StudentsAnswers Controller
 *
 * @property StudentsAnswer $StudentsAnswer
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class StudentsAnswersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * index method
     *
     * @return void
     */
    public function index() {

        $problemas = $this->StudentsAnswer->Question->query("SELECT problems.id FROM problems ORDER BY problems.relative_order ASC;");
        $cantProblemas = count($problemas);
        $resultado = Set::classicExtract($problemas, '{n}.problems.id');
        $this->Session->write('cantProblemas', $cantProblemas);
        $numProblema = 1;

        for ($i = 0; $i < $cantProblemas; $i++) {
            $cantPreguntas = $this->StudentsAnswer->Question->query("SELECT * FROM questions where problem_id = '$resultado[$i]';");
            $nombreProblema = 'problema' . $numProblema;
            $numProblema = $numProblema + 1;
            $this->Session->write($nombreProblema, count($cantPreguntas));
        }

        $idUsuarioAlumno = $this->Auth->user('id');
                
        $alumno = $this->StudentsAnswer->query("SELECT * FROM students WHERE user_id='$idUsuarioAlumno';");
        $idAlumno = $alumno[0]['students']['student'];

        $initExamen = $this->StudentsAnswer->query("SELECT init FROM students WHERE student='$idAlumno';");
        $init = $initExamen[0]['students']['init'];
        $finishExamen = $this->StudentsAnswer->query("SELECT finish FROM students WHERE student='$idAlumno';");
        $finish = $finishExamen[0]['students']['finish'];

        $estadoExamen = $this->StudentsAnswer->query("SELECT published FROM exams WHERE id='1';");
        $examenPublicado = $estadoExamen[0]['exams']['published'];

        $this->set('init', $init);
        $this->set('finish', $finish);
        $this->set('published', $examenPublicado);
    }

    public function answer($problema = null, $pregunta = null) {

        //Obtiene el id del usuario leyendo desde Session
        $idUsuarioAlumno = $this->Auth->user('id');
        
        $semaforo = $this->Session->read('Semaforo');
        
        //Recupera el alumno
        $alumno = $this->StudentsAnswer->query("SELECT * FROM students WHERE user_id='$idUsuarioAlumno';");
        //Obtiene el id del alumno
        $idAlumno = $alumno[0]['students']['student'];
        $finalizoAlumno = $alumno[0]['students']['finish'];
        
        
        if ( ($semaforo==1) && (($problema>1) && ($pregunta==1)) ) {
            //Esto modela la presión del botón para avanzar de problema, donde antes no se actualizaba
            //el estado actual y es necesario hacerlo para una correcta lógica           
                $this->Session->write('Semaforo',0);
                
                //Estoy en un pasaje a siguiente problema
                $estadoActual = $this->_obtenerEstadoActual($idAlumno);

                //Actualizo el nuevo estado de manera acorde
                //Se hace de los campos del estado actual
                $problemaAux = $estadoActual['problemID'];
                $preguntaAux = $estadoActual['questionID'];

                $this->_actualizarActualState($problemaAux,$preguntaAux,$idAlumno);  
        }
                
        //Obtiene (si es que existe) el estado actual correspondiente al alumno
        //Por estado actual me refiero a la pregunta correspondiente al problema que el alumno debería responder,
        //NO al par conformados por la ya respondida
        $estadoActual = $this->_obtenerEstadoActual($idAlumno);

        //Se hace de los campos del estado actual
        $problemaEstadoActual = $estadoActual['problemID'];
        $preguntaEstadoActual = $estadoActual['questionID'];
        $finalizadoExamenActual = $estadoActual['finalizado'];
        
                 
        //Se fija si los valores recibidos en la url coinciden con el estado actual o no
        if (($problema == $problemaEstadoActual) && ($pregunta == $preguntaEstadoActual)) {

            //Está en el estado en donde debería estar
            //Ya se está seguro de que tanto la pregunta como el problema son válidos, si no no pasarían la verificación anterior
            
            //Recupera la pregunta y el problema asociados a los ordenes relativos recibidos en la url
            $idProblemaActual = $this->StudentsAnswer->Question->query("SELECT id FROM problems where relative_order = '$problema';");
            $resultado = Set::classicExtract($idProblemaActual, '{n}.problems.id');

            $preguntaActualAux = $this->StudentsAnswer->Question->query("SELECT * FROM questions where relative_order = '$pregunta' and problem_id='$resultado[0]';");
            $preguntaActual = Set::classicExtract($preguntaActualAux, '{n}.questions');
            $idPreguntaActual = $preguntaActual[0]['id'];

            $preguntasAnterioresAux = $this->StudentsAnswer->Question->query("SELECT * FROM questions where relative_order < '$pregunta' and problem_id='$resultado[0]';");
            $preguntasAnteriores = Set::classicExtract($preguntasAnterioresAux, '{n}.questions');


            //Me fijo si se trata de un post para guardar nueva información
            if ($this->request->is('post')) {
                
                //Si terminé el examen por mi propio mérito, no hay más nada que hacer                
                if ($finalizadoExamenActual==true) {
                    $this->StudentsAnswer->query("UPDATE students SET finish='1' WHERE student='$idAlumno';");
                    return $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'finishexam'));
                }
                
                //Si no, si me terminaron el examen a la fuerza
                else if ($finalizoAlumno) {
                    //Si me terminaron el examen a la fuerza, redirecciono a una página que indica esto
                    return $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'finishexambyforce'));   
                }
                
                else {
                    
                //Si es la primera pregunta del primer problema, entonces actualiza el init de ese alumno
                if ($problema == 1 && $pregunta == 1) {
                        $this->StudentsAnswer->query("UPDATE students SET init='1' WHERE student='$idAlumno';");
                }
                
                //Recupera la cantidad de problemas y de preguntas asociadas al problema
                $nombreProblema = 'problema' . $problema;
                $cantPreguntas = $this->Session->read($nombreProblema);
                $cantProblemas = $this->Session->read('cantProblemas');

                //Si no se corresponde con la ultima pregunta
                if ($cantPreguntas != $pregunta) {

                    //Obtiene la respuesta escrita por el alumno
                    $respuestaAlumno = $this->request->data['StudentsAnswer']['rtaAlumno'];
                    $entradasTablaStudentsAnswers = $this->StudentsAnswer->query("SELECT * FROM students_answers WHERE student_id='$idAlumno' AND question_id='$idPreguntaActual';");

                    if (empty($entradasTablaStudentsAnswers)) {

                        //Si no hay una entrada en la tabla, la inserta
                        $this->StudentsAnswer->query("INSERT INTO students_answers (student_id,question_id,answer) VALUES ('$idAlumno','$idPreguntaActual','$respuestaAlumno');");

                        //Obtiene la información del estado actual de la tabla
                        $estadoActualAlumno = $this->StudentsAnswer->query("SELECT * FROM actual_states WHERE student_id='$idAlumno';");
                        //Si no hay entrada, inserta
                        if (empty($estadoActualAlumno)) {
                            $this->StudentsAnswer->query("INSERT INTO actual_states (student_id,problem_id,question_id) VALUES ('$idAlumno','$resultado[0]','$idPreguntaActual');");
                        } else {
                            //Si ya hay una entrada, simplemente la actualiza
                            $this->StudentsAnswer->query("UPDATE actual_states SET problem_id='$resultado[0]',question_id='$idPreguntaActual' WHERE student_id='$idAlumno';");
                        }

                        //Una vez actualizado el estado actual, obtiene el nuevo estado actual para redirigir al mismo
                        $nuevoEstadoActual = $this->_obtenerEstadoActual($idAlumno);

                        //Se hace de los campos del estado actual
                        $problemaNuevoEstadoActual = $nuevoEstadoActual['problemID'];
                        $preguntaNuevoEstadoActual = $nuevoEstadoActual['questionID'];
                        
                        return $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'answer', $problemaNuevoEstadoActual, $preguntaNuevoEstadoActual));
                       
                    } else {
                        //No debería ocurrir jamás este caso, muestro un mensaje de error por las dudas y redirijo al estado actual
                        $this->Session->setFlash(__('Ya está cargada la respuesta para la pregunta de este alumno!'), 'default', array('class' => 'alert alert-info'));
                        return $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'index'));
                    }      
                } 
                
                else {
                    //Llegué a la última pregunta del problema, la cual no tiene respuesta por lo que manualmente
                    //redirijo como corresponda
                    if ($problema < $cantProblemas) {
                        $pregunta = 1;
                        $problema = $problema + 1;
                        
                        //Uso una variable que escribo en session como semáforo
                        $this->Session->write('Semaforo',1);                        
                        return $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'answer', $problema, $pregunta));
                    } else {
                        //No hay más problemas, queda redirigir a una página que muestre que el examen terminó
                        $this->StudentsAnswer->query("UPDATE students SET finish='1' WHERE student='$idAlumno';");
                        return $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'finishexam'));
                    }
                }
            }
            }
            
            else {
                //Es un get, me la están escribiendo. Si terminó lo redirijo a finishexams
                if ($finalizoAlumno) {
                    //Si me terminaron el examen a la fuerza, redirecciono a una página que indica esto
                    return $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'finishexam'));   
                }
                
                //Si el examen no está publicado también redirijo al index
                //Obtengo el examen de esta forma (a lo negro y redirijo al index)
                $estadoExamen = $this->StudentsAnswer->query("SELECT published FROM exams WHERE id='1';");
                $examenPublicado = $estadoExamen[0]['exams']['published'];
                
                if (!$examenPublicado) {
                    return $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'index'));   
                }
                
            }

            //Post o no, carga la información de la vista
            //Guarda las cosas en la vista
            $this->set('problema', $problema);
            $this->set('pregunta', $pregunta);
            $this->set('preguntaEvaluada', $preguntaActual);
            $this->set('preguntasPrevias', $preguntasAnteriores);

            $imgPregunta = $this->StudentsAnswer->query("SELECT * FROM images WHERE question_id='$idPreguntaActual';");
            $listaImagenes = array();
            for ($i = 0; $i < count($imgPregunta); $i++) {
                $listaImagenes[$i] = $imgPregunta[$i]['images']['relative_path'];
            }
            $this->set('imagenes', $listaImagenes);
            
        }
        
        
        else {
            //Hay un problema, está queriendo acceder a un estado que no es el que le corresponde
            //Redirijo directamente al estado actual, no hay más nada que verificar
            return $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'answer', $problemaEstadoActual, $preguntaEstadoActual));
        }    
    }
    
    /*
     * Acción que tiene una vista muy sencilla asociada
     */
    public function finishexam() {
        
    }
    
    /*
     * Acción que tiene una vista muy sencilla asociada
     */
    public function finishexambyforce() {
        
    }

    /*
     * En función del id de un alumno, busca el estado actual del mismo.
     * Arreglo de devolución de datos:
     * data['result']=false/true en función de si hay o no entrada.
     * data['problemID'] = ID del problema actual del alumno.
     * data['questionID'] = ID de la pregunta actual del alumno.
     * data['finalizado'] = indica si el alumnno llegó al final del examen, sirve para redirigir a donde corresponda 
     * Posibles casos:
     * 
     * No hay ninguna entrada en la base de datos para ese alumno, lo cual sucede la primera vez
     * que se ingresa al examen. En este caso devuelve:
     *      data['result']=false, data['problemID']=1, data['questionID']=1, data['finalizado']=false.
     * 
     * Hay una entrada en la base de datos para ese alumno. En este caso devuelve:
     *      data['result']=true, data['problemID']=ID correspondiente, data['questionID']=ID correspondiente,data['finalizado']=true.  
     * 
     * Como es un método privado, asume que se invoca con un ID de student válido, nadie lo puede invocar
     * desde afuera.
     * 
     * Detalle importante: el estado actual hace referencia al par pregunta-problema que tiene que responder
     * el alumno y NO al último que del cual se tiene guardado.
     */

    protected function _obtenerEstadoActual($studentID) {

        //Cargado de modelos necesarios
        $this->loadModel('ActualState');

        /*
         * Obtiene, dado el id del estudiante, el estado actual correspondiente al mismo
         */
        $estadoActual = $this->ActualState->find('first', array(
            'conditions' => array('student_id' => $studentID),
            'recursive' => -1
        ));

        //Variable de salida
        $salida = array();

        //Se fija si el estado actual está vacio o no para la respuesta
        if (empty($estadoActual['ActualState'])) {
            $salida['result'] = false;
            $salida['problemID'] = 1;
            $salida['questionID'] = 1;
            $salida['finalizado'] = false;
        } else {
            //Hay un estado actual para ese alumno, lo recupero
            //Calculo en función de esto el estado actual efectivo
            $problemIDActual = $estadoActual['ActualState']['problem_id'];
            $questionIDActual = $estadoActual['ActualState']['question_id'];

            //Invoco un método que se encarga de hacerlo
            $estadoActualReal = $this->_calcularActualEfectivo($problemIDActual, $questionIDActual);

            $salida['result'] = true;
            $salida['problemID'] = $estadoActualReal['problema'];
            $salida['questionID'] = $estadoActualReal['pregunta'];
            $salida['finalizado'] = $estadoActualReal['terminado'];
        }

        //Retorna el resultado
        return $salida;
    }

    /*
     * Dado el id del problema y de la pregunta, obtiene el estado actual real, esto es, el par
     * pregunta, problema que efectivamente le toca responder a un alumno dado. Nosotros en la base
     * de datos guardamos el estado actual como el par pregunta problema que hasta el momento
     * el alumno ha respondido, por eso es necesario un cálculo extra para obtener el estado actual
     * efectivo.
     * 
     * Notar que es un submétodo del método privado _obtenerEstadoActual que sólo hace calculos para un
     * caso
     * 
     * Estructura de salida:
     * data['terminado']
     * data['problema']
     * data['pregunta']
     */

    protected function _calcularActualEfectivo($idProblema, $idPregunta) {

        //Recupera el orden relativo del problema y de la pregunta, que es lo que usamos para ordenar todo
        $ordenRelativoProblema = $this->StudentsAnswer->query("SELECT relative_order FROM problems WHERE id='$idProblema';");
        $ordenRelativoPregunta = $this->StudentsAnswer->query("SELECT relative_order FROM questions WHERE id='$idPregunta';");

        $problema = $ordenRelativoProblema[0]['problems']['relative_order'];
        $pregunta = $ordenRelativoPregunta[0]['questions']['relative_order'];

        $nombreProblema = 'problema' . $problema;

        //Variable de salida
        $salida = array();
        //Asumo inicialmente que el examen no termino
        $salida['terminado'] = false;

        //En función del problema, recupera la cantidad de preguntas que tiene
        //Recupera a su vez la cantidad de problemas que hay
        $cantPreguntas = $this->Session->read($nombreProblema);
        $cantProblemas = $this->Session->read('cantProblemas');

        if ($pregunta < $cantPreguntas) {
            //Esta dentro de los límites válidos, simplemente le suma 1 al número de la pregunta    
            $pregunta = $pregunta + 1;
        } else {
            //La pregunta se pasó del límite, tengo que pasar al siguiente problema
            if ($problema < $cantProblemas) {
                //El problema está dentro del rango válido, simplemente le sumo 1
                $pregunta = 1;
                $problema = $problema + 1;
            } else {
                //La pregunta se pasó del límite y el problema también, se ha terminado el examen
                $salida['terminado'] = true;
            }
        }

        //Devuelvo los valores correspondientes en el arreglo
        $salida['problema'] = $problema;
        $salida['pregunta'] = $pregunta;

        return $salida;
    }
    
    
    /*
     * Cuando se presiona el botón para pasar al siguiente problema, nunca se actualizaba el estado actual
     * antes de efectivamente pasar. Esto debe hacerse para correctitud por cómo está implementado ahora.
     */
    protected function _actualizarActualState($problema,$pregunta,$idAlumno) {
        $this->StudentsAnswer->query("UPDATE actual_states SET problem_id='$problema',question_id='$pregunta' WHERE student_id='$idAlumno';");
    }
       
    /*
     * Se usa para el botón retomar, simplemente obtiene el estado actual y llama a answer con esa
     * información
     */
    public function actualstate() {
        $idUsuarioAlumno = $this->Auth->user('id');
        $alumno = $this->StudentsAnswer->query("SELECT * FROM students WHERE user_id='$idUsuarioAlumno';");

        $idAlumno = $alumno[0]['students']['student'];
        
        //Obtiene (si es que existe) el estado actual correspondiente al alumno
        //Por estado actual me refiero a la pregunta correspondiente al problema que el alumno debería responder,
        //NO al par conformados por la ya respondida
        $estadoActual = $this->_obtenerEstadoActual($idAlumno);

        //Se hace de los campos del estado actual
        $problemaEstadoActual = $estadoActual['problemID'];
        $preguntaEstadoActual = $estadoActual['questionID'];
        
        //Redirige a answer con el estado actual correspondiente
        return $this->redirect(array('controller' => 'StudentsAnswers', 'action' => 'answer', $problemaEstadoActual, $preguntaEstadoActual));
    }  
    
    

    /*
     * Redefine el método isAuthorized para este controlador. 
     * Se verifica el rol del usuario que esta logueado y dependiendo del mismo se establecen
     * los permisos correspondientes.
     */

    public function isAuthorized($user) {
        $esAdmin = false;
        $esStudent = false;
        $esEvaluator = false;
        if (AuthComponent::user()) {

            $nombreRol = $this->Session->read('Auth.User.role');

            if (strcmp("Administrador", $nombreRol) === 0) {
                //Es administrador
                $esAdmin = true;
            } else if (strcmp("Evaluador", $nombreRol) === 0) {
                //Es evaluador
                $esEvaluator = true;
            } else if (strcmp("Alumno", $nombreRol) === 0) {
                //Es alumno
                $esStudent = true;
            }
        }

        if ($esAdmin && (!$esEvaluator) && (!$esStudent)) {
            $this->Session->setFlash(__($this->Auth->authError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect('/users/administrador');
            
        } else if ((!$esAdmin) && $esEvaluator && (!$esStudent)) {
            $this->Session->setFlash(__($this->Auth->authError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect('/managerEvaluator/assignedproblems');
            
        } else if ((!$esAdmin) && (!$esEvaluator) && $esStudent) {
            return true;
        }
        
        //Llegado este punto, el isAuthorized por la configuración de AppController devuelve false
        return parent::isAuthorized($user);
    }
    
    
}
