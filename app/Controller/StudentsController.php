<?php

App::uses('AppController', 'Controller');

/**
 * Students Controller
 *
 * @property Student $Student
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class StudentsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    public function calculator() {
        
    }

    public function datepicker() {
        
    }

    public function gestograma() {
        
    }

    /*
     * Redefine el método isAuthorized para este controlador. 
     * Se verifica el rol del usuario que esta logueado y dependiendo del mismo se establecen
     * los permisos correspondientes.
     */
    public function isAuthorized($user) {
        $esAdmin = false;
        $esStudent = false;
        $esEvaluator = false;
        if (AuthComponent::user()) {

            $nombreRol = $this->Session->read('Auth.User.role');

            if (strcmp("Administrador", $nombreRol) === 0) {
                //Es administrador
                $esAdmin = true;
            } else if (strcmp("Evaluador", $nombreRol) === 0) {
                //Es evaluador
                $esEvaluator = true;
            } else if (strcmp("Alumno", $nombreRol) === 0) {
                //Es alumno
                $esStudent = true;
            }
        }

        if ($esAdmin && (!$esEvaluator) && (!$esStudent)) {
            $this->Session->setFlash(__($this->Auth->authError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect('/users/administrador');
             }   
        else if ((!$esAdmin) && $esEvaluator && (!$esStudent)) {
            $this->Session->setFlash(__($this->Auth->authError), 'default', array('class' => 'alert alert-dismissable alert-danger'));
            return $this->redirect('/managerEvaluator/assignedproblems');
            
        } else if ((!$esAdmin) && (!$esEvaluator) && $esStudent) {
            return true;  
        }
        
        //Llegado este punto, el isAuthorized por la configuración de AppController devuelve false
        return parent::isAuthorized($user);
    }

}
